﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Sistema.Entidades.Usuarios
{
    public class RolNet
    {
        public int id { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 3,
            ErrorMessage = "El nombre no debe de tener más de 50 caracteres, ni menos de 3 caracteres.")]

        public string role_name { get; set; }
        
        public bool activo { get; set; }
        public bool eliminado { get; set; }

        //public ICollection<UsuarioNet> UsuarioNet { get; set; }
        public ICollection<usuario_rol> detalle_usuario { get; set; }

        //public ICollection<usuario_rol> detalle_rol { get; set; }

        //public ICollection<UsuarioNet> usuarios { get; set; }
    }
}
