﻿using Sistema.Entidades.Wcm._1_N;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Sistema.Entidades.Usuarios
{
   public  class UsuarioNet
    {
        public int id { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Se require un nombre de usuario")]
        public string username { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Se requiero un password.")]
        public string passwd { get; set; }

        public string fname { get; set; } = null;
        public string lname { get; set; } = null;
        public string notes { get; set; } = null;
        public string telefono { get; set; } = null;
        
        public string email { get; set; } = null;
        [Required]
        public byte[] password_hash { get; set; }
        [Required]
        public byte[] password_salt { get; set; }
        public byte[] huella { get; set; }
        public bool activo { get; set; } = true;
        public bool eliminado { get; set; } = false;

        public string avatar { get; set; } = null;
        // public int idrol { get; set; }
        //public RolNet rolnet { get; set; }
        public ICollection<usuario_rol> detalle_rol { get; set; }
        public ICollection<usuario_area> detalle_area { get; set; }
        public ICollection<responsable_area> responsable_area { get; set; }

        public ICollection<Registrosanomalias> registroAnomalia { get; set; }
       public ICollection<Registrosanomalias> registroAnomaliaTecnico { get; set; }
       public ICollection<Registrosanomalias> registroAnomaliaSupervisor { get; set; }
       public ICollection<Registrosanomalias> registroAnomaliaLider { get; set; }

        public ICollection<RegistroShe> registroAnomaliaShe { get; set; }
        public ICollection<RegistroShe> registroAnomaliaSheTecnico { get; set; }
        public ICollection<RegistroShe> registroAnomaliaSheSupervisor { get; set; }
        public ICollection<RegistroShe> registroAnomaliaSheLider { get; set; }


    }
}
