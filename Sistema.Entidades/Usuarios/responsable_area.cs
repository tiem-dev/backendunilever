﻿using Sistema.Entidades.Wcm;

namespace Sistema.Entidades.Usuarios
{
    public class responsable_area
    {
        public int idrelacion { get; set; }
        public int idusuario { get; set; }
        public int idarea { get; set; }
        public UsuarioNet UsuarioNet { get; set; }
        public Area areas { get; set; }
    }
}
