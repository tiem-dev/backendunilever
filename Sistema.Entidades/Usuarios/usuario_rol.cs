﻿namespace Sistema.Entidades.Usuarios
{
    public class usuario_rol
    {
        public int id{ get; set; }
        public int user_id { get; set; }
        public int role_id { get; set; }
        public UsuarioNet UsuarioNet { get; set; }
        public RolNet RolNet { get; set; }
    }
}
