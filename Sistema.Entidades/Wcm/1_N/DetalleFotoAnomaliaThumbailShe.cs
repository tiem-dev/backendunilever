﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sistema.Entidades.Wcm._1_N
{
    public class DetalleFotoAnomaliaThumbailShe
    {
        public int idfoto { get; set; }
        public int idshe { get; set; }
        public string foto_anomalia { get; set; }
        public string tipo_foto { get; set; }
        public bool eliminado { get; set; } = false;

        public RegistroShe anomalia { get; set; }
    }
}
