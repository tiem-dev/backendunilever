﻿using Sistema.Entidades.Proyecto;
using Sistema.Entidades.Usuarios;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sistema.Entidades.Wcm._1_N
{
    public class Registrosanomalias
    {
        public int idregistroanomalia { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string codigo { get; set; }
        public DateTime emision_ts { get; set; } = DateTime.Now;
        public int idusuario { get; set; }
        public int paso_ma { get; set; }
        public string criticidad { get; set; }
        public string turno { get; set; }
        public int idarea { get; set; }
        public int idsector { get; set; } = 1;
        public int? idmaquina { get; set; }
        public int idanomalia { get; set; }
        public int idsuceso { get; set; }
        public int idtarjeta { get; set; }
        public Nullable<int> idproyecto { get; set; } = 1;

        public string descripcion { get; set; }

        public string sol_implementada { get; set; }
        public DateTime programado_ts { get; set; }

        public DateTime ejecucion_ts { get; set; } = Convert.ToDateTime("1990-01-01 00:00:00");
        public DateTime recepcion_ts { get; set; } = Convert.ToDateTime("1990-01-01 00:00:00");
        public DateTime cierre_ts { get; set; } = Convert.ToDateTime("1990-01-01 00:00:00");

        public Nullable<int> idtecnico { get; set; }


        public bool confirmacion_tec { get; set; } = false;


        public Nullable<int> idsupervisor { get; set; }
        public bool confirmacion_super { get; set; } = false;
        public Nullable<int> idlider { get; set; }
        public bool confirmacion_lider { get; set; } = false;
        public bool prog { get; set; } = false;
        public bool eliminado { get; set; } = false;
        //public string avatar { get; set; }
        public ICollection<DetalleFotoAnomalia> detalles { get; set; }
        public DetalleFotoAnomaliaThumbnail foto_icon { get; set; }

        public UsuarioNet usuario { get; set; }
        public UsuarioNet usuariotecnico { get; set; }
        public UsuarioNet usuariosupervisor { get; set; }
        public UsuarioNet usuariolider { get; set; }
        public Area area { get; set; }
        public Sector sector { get; set; }

        public virtual Maquina maquina { get; set; }

        public Anomalia anomalia { get; set; }
        public Suceso suceso { get; set; }
        public Tarjeta tarjeta { get; set; }
        public Proyectos proyecto { get; set; }






    }
}
