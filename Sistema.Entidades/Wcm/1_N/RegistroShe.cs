﻿using Sistema.Entidades.Usuarios;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Sistema.Entidades.Wcm._1_N
{
    public class RegistroShe
    {
        public int idshe { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string codigo { get; set; }
        public DateTime emision_ts { get; set; }
        public int idusuario { get; set; }
        public string prioridad { get; set; }
        public string turno { get; set; }
        public int idarea { get; set; }
        public int idsector { get; set; }
        public int idmaquina { get; set; }
        public string she_ma { get; set; }
        public bool paro_equipo { get; set; }
        public int idfalla { get; set; }
        public int idcondicion { get; set; }
        public int idtarjeta { get; set; }

        public string descripcion { get; set; }
        public string ope_mtto { get; set; }
        public string sol_implementada { get; set; }
        public DateTime programado_ts { get; set; }
        public DateTime ejecucion_ts { get; set; } = Convert.ToDateTime("1990-01-01 00:00:00");
        public DateTime recepcion_ts { get; set; } = Convert.ToDateTime("1990-01-01 00:00:00");
        public DateTime cierre_ts { get; set; } = Convert.ToDateTime("1990-01-01 00:00:00");
        public Nullable<int> idtecnico { get; set; }
        public bool confirmacion_tec { get; set; } = false;
        public Nullable<int> idsupervisor { get; set; }
        public bool confirmacion_super { get; set; } = false;
        public Nullable<int> idlider { get; set; }
        public bool confirmacion_lider { get; set; } = false;
        public bool prog { get; set; } = false;

        public bool eliminado { get; set; } = false;

        public Maquina maquina { get; set; }
        public Area area { get; set; }
        public Falla Falla { get; set; }
        public CondicionInsegura condicionInsegura { get; set; }
        public UsuarioNet usuario { get; set; }
        public UsuarioNet usuariotecnico { get; set; }
        public UsuarioNet usuariosupervisor { get; set; }
        public UsuarioNet usuariolider { get; set; }
        public Sector sector { get; set; }
        public Tarjeta tarjeta { get; set; }

        public ICollection<DetalleFotoAnomaliaShe> detalles { get; set; }
        public DetalleFotoAnomaliaThumbailShe foto_icon { get; set; }



    }
}
