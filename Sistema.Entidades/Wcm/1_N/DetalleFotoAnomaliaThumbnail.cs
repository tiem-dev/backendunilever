﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sistema.Entidades.Wcm._1_N
{
    public class DetalleFotoAnomaliaThumbnail
    {
        public int idfoto { get; set; }
        public int idregistroanomalia { get; set; }
        public string foto_anomalia { get; set; }
        public string tipo_foto { get; set; }
        public bool eliminado { get; set; } = false;

        public Registrosanomalias anomalia { get; set; }
    }
}
