﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sistema.Datos;
using Sistema.Entidades.Usuarios;
using Sistema.Web.Models.Usuarios.UsuarioNet;

namespace Sistema.Web.Controllers.UsuariosNet
{
    [Route("api/[controller]")]
    [ApiController]
    public class usuario_areaController : ControllerBase
    {
        private readonly DbContextSistema _context;

        public usuario_areaController(DbContextSistema context)
        {
            _context = context;
        }

        // GET: api/usuario_area
        [HttpGet]
        public async Task<ActionResult<IEnumerable<usuario_area>>> Getusuario_area()
        {
            return await _context.usuario_area.ToListAsync();
        }

        // GET: api/usuario_area/5
        [HttpGet("{id}")]
        public async Task<ActionResult<usuario_area>> Getusuario_area(int id)
        {
            var usuario_area = await _context.usuario_area.FindAsync(id);

            if (usuario_area == null)
            {
                return NotFound();
            }

            return usuario_area;
        }

        // GET: api/usuario_area/SelectEspecifico/1
        [HttpGet("[action]/{id}")]
        public async Task<IEnumerable<Usuario_areaViewModel>> SelectEspecifico(int id)
        {
            var categoriaPerdida = await _context.usuario_area.Where(c => c.idusuario == id).ToListAsync();

            return categoriaPerdida.Select(c => new Usuario_areaViewModel
            {
                idrelacionarea = c.idrelacionarea,
                idusuario = c.idusuario,
                idarea = c.idarea
            });

        }

        // GET: api/usuario_area/SelectEspecifico/1
        [HttpGet("[action]/{id_user}/{id_area}")]
        public async Task<IEnumerable<Usuario_areaViewModel>> SelectUserArea(int id_user, int id_area)
        {
            var categoriaPerdida = await _context.usuario_area.Where(c => c.idusuario == id_user).Where(c => c.idarea == id_area).ToListAsync();

            return categoriaPerdida.Select(c => new Usuario_areaViewModel
            {
                idrelacionarea = c.idrelacionarea,
                idusuario = c.idusuario,
                idarea = c.idarea
            });

        }

        // DELETE: api/usuario_area/Deleteusuario_area/33/1
        [HttpDelete("[action]/{id_user}/{id_area}")]
        public async Task<ActionResult<usuario_area>> Deleteusuario_area(int id_user, int id_area)
        {
            var usuario_area = await _context.usuario_area.Where(c => c.idusuario == id_user).Where(c => c.idarea == id_area).FirstOrDefaultAsync();
            if (usuario_area == null)
            {
                return NotFound();
            }

            _context.usuario_area.Remove(usuario_area);
            await _context.SaveChangesAsync();

            return usuario_area;
        }

        // POST: api/usuario_area/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> CrearArea([FromBody] CrearUsuarioAreaViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            usuario_area nuevo_area = new usuario_area
            {
                idusuario = model.idusuario,
                idarea = model.idarea,
            };

            _context.usuario_area.Add(nuevo_area);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

            return Ok();
        }

        // PUT: api/usuario_area/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Putusuario_area(int id, usuario_area usuario_area)
        {
            if (id != usuario_area.idrelacionarea)
            {
                return BadRequest();
            }

            _context.Entry(usuario_area).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!usuario_areaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/usuario_area
        [HttpPost]
        public async Task<ActionResult<usuario_area>> Postusuario_area(usuario_area usuario_area)
        {
            _context.usuario_area.Add(usuario_area);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Getusuario_area", new { id = usuario_area.idrelacionarea }, usuario_area);
        }

        // DELETE: api/usuario_area/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<usuario_area>> Deleteusuario_area(int id)
        {
            var usuario_area = await _context.usuario_area.FindAsync(id);
            if (usuario_area == null)
            {
                return NotFound();
            }

            _context.usuario_area.Remove(usuario_area);
            await _context.SaveChangesAsync();

            return usuario_area;
        }

        private bool usuario_areaExists(int id)
        {
            return _context.usuario_area.Any(e => e.idrelacionarea == id);
        }
    }
}
