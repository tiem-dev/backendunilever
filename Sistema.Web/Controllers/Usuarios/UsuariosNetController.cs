﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Sistema.Datos;
using Sistema.Entidades.Usuarios;
using Sistema.Web.Controllers.CalculosAuxliares;
using Sistema.Web.Models.Usuarios.UsuarioNet;

namespace Sistema.Web.Controllers.UsuariosNet
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosNetController : ControllerBase
    {
        private readonly DbContextSistema _context;
        private readonly IConfiguration _config;

        public UsuariosNetController(DbContextSistema context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }

        // GET:api/UsuariosNet/Listarbusqueda
        ///[Authorize(Roles = "Administrador")]
        [HttpGet("[action]")]
        public async Task<IEnumerable<UsuarioListarViewModel>> Listarbusqueda()
        {
            var usuario = await _context.UsuariosNet
               // .Include(u => u.rolnet)
                .ToListAsync();

            return usuario.Select(u => new UsuarioListarViewModel
            {
                idusuario = u.id,
                username = u.username,
                activo = u.activo,
            });

        }
        
        // GET: api/UsuariosNet/Listaravatar
        //[Authorize(Roles = "Administrador")]
        [HttpGet("[action]")]
        public async Task<IEnumerable<UsuarioViewModel>> Listaravatar()
        {
            var usuario = await _context.UsuariosNet.ToListAsync();

            return usuario.Select(u => new UsuarioViewModel
            {
                idusuario = u.id,
                username = u.username,
                fname = u.fname,
                lname = u.lname,
                telefono = u.telefono,
                email = u.email,
                password_hash = u.password_hash,
                activo = u.activo,
                avatar = u.avatar
            });
        }

        // POST: api/UsuariosNet/Crear
        //[Authorize(Roles = "ADMINISTRADOR, ADMINISTRADOR WCM")]
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] CrearViewModel model)
        {
            Console.WriteLine("hasta aqui si llego 1 ///////////////////////////////////////////////");
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            CrearPasswordHash(model.password, out byte[] passwordHash, out byte[] passwordSalt);
            Console.WriteLine("hasta aqui si llego 2////////////////////////////////////////////////");
            var nombre_completo = model.nombre + "." + model.apellido;
            EncriptacionPassword ignition_Password = new EncriptacionPassword();
            UsuarioNet user_nue = new UsuarioNet
            {
                username = nombre_completo,
                passwd = ignition_Password.GetHashCodeInt64(model.password),
                telefono = model.telefono,
                fname = model.nombre,
                lname = model.apellido,
                password_hash = passwordHash,
                password_salt = passwordSalt,
                activo = true,
                eliminado = false,
                avatar = model.avatar
            };
            try
            {
                _context.UsuariosNet.Add(user_nue);
                await _context.SaveChangesAsync();

                var idusuariorol = user_nue.id;
                //////////////////////////////////////////////////////////////////////// llenado de tabla relacion usuario area///////////////////
                foreach (var col in model.UsuarioAreaRelacion)
                {
                    usuario_area detalle_usuario_area = new usuario_area
                    {
                        idusuario = idusuariorol,
                        idarea = col.id,
                    };
                    _context.usuario_area.Add(detalle_usuario_area);
                }
                //////////////////////////////////////////////////////////////////////// llenado de tabla relacion usuario rol///////////////////
                foreach (var col in model.UsuariorolRelacion)
                {
                    usuario_rol detalle_usuario = new usuario_rol
                    {
                        user_id = idusuariorol,
                        role_id = col.idrol,
                    };
                    _context.usuario_rol.Add(detalle_usuario);
                }

                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

            return Ok();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UsuarioNet>> GetUsuarioNet(int id)
        {
            var usuarioNet = await _context.UsuariosNet.FindAsync(id);

            if (usuarioNet == null)
            {
                return NotFound();
            }

            return usuarioNet;
        }

        // PUT: api/UsuariosNet/Actualizar
        [HttpPut("[action]")]
        public async Task<IActionResult> Actualizar([FromBody] ActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idusuario <= 0)
            {
                return BadRequest();
            }

            var usuario = await _context.UsuariosNet.FirstOrDefaultAsync(u => u.id == model.idusuario);
            var nombre_completo = model.nombre + "." + model.apellido;
            EncriptacionPassword ignition_Password = new EncriptacionPassword();
            if (usuario == null)
            {
                return NotFound();
            }

            usuario.username = nombre_completo;
            usuario.telefono = model.telefono;
            usuario.avatar = model.avatar;
            usuario.passwd = ignition_Password.GetHashCodeInt64(model.password);
            usuario.fname = model.nombre;
            usuario.lname = model.apellido;

            if (model.act_password == true)
            {
                CrearPasswordHash(model.password, out byte[] passwordHash, out byte[] passwordSalt);
                usuario.password_hash = passwordHash;
                usuario.password_salt = passwordSalt;
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }

        // PUT: api/UsuariosNet/ActualizarFoto
        [HttpPut("[action]")]
        public async Task<IActionResult> ActualizarFoto([FromBody] ActualizarFotoViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idusuario <= 0)
            {
                return BadRequest();
            }

            var usuario = await _context.UsuariosNet.FirstOrDefaultAsync(u => u.id == model.idusuario);
            if (usuario == null)
            {
                return NotFound();
            }
            usuario.avatar = model.avatar;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }
        // PUT: api/UsuariosNet/ActualizarPas
        [HttpPut("[action]")]
        public async Task<IActionResult> ActualizarPas([FromBody] ActualizarPasViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idusuario <= 0)
            {
                return BadRequest();
            }

            var usuario = await _context.UsuariosNet.FirstOrDefaultAsync(u => u.id == model.idusuario);
            EncriptacionPassword ignition_Password = new EncriptacionPassword();
            if (usuario == null)
            {
                return NotFound();
            }

            usuario.passwd = ignition_Password.GetHashCodeInt64(model.password);
            CrearPasswordHash(model.password, out byte[] passwordHash, out byte[] passwordSalt);
            usuario.password_hash = passwordHash;
            usuario.password_salt = passwordSalt;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }

        

        // PUT: api/UsuariosNet/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUsuarioNet(int id, UsuarioNet usuarioNet)
        {
            if (id != usuarioNet.id)
            {
                return BadRequest();
            }

            _context.Entry(usuarioNet).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsuarioNetExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // PUT: api/UsuariosNet/Desactivar/1
        //[Authorize(Roles = "Administrador")]
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Desactivar([FromRoute] int id)
        {

            if (id <= 0)
            {
                return BadRequest();
            }

            var usuario = await _context.UsuariosNet.FirstOrDefaultAsync(a => a.id == id);

            if (usuario == null)
            {
                return NotFound();
            }

            usuario.activo = false;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }

        // PUT: api/UsuariosNet/Activar/1
        [Authorize(Roles = "ADMINISTRADOR")]
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Activar([FromRoute] int id)
        {

            if (id <= 0)
            {
                return BadRequest();
            }

            var usuario = await _context.UsuariosNet.FirstOrDefaultAsync(a => a.id == id);

            if (usuario == null)
            {
                return NotFound();
            }

            usuario.activo = true;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }

        // PUT: api/UsuariosNet/UsuarioRolesAsignados/1
        //[Authorize(Roles = "Administrador")]
        [HttpGet("[action]/{id}")]
        public async Task<IEnumerable<UsuarioListaRolesViewModel>> UsuarioRolesAsignados([FromRoute] int id)
        {
            var usuario_roles = await _context.usuario_rol.Where(r => r.user_id == id).Include(r => r.RolNet).ToListAsync();
            
            return usuario_roles.Select(u => new UsuarioListaRolesViewModel
            {
                idusuario = u.user_id,
                id_rol = u.role_id,
                nombre_rol = u.RolNet.role_name
            });
        }

        // PUT: api/UsuariosNet/UsuarioRolesAsignados/1
        //[Authorize(Roles = "Administrador")]
        [HttpGet("[action]/{id}")]
        public async Task<IEnumerable<UsuarioListaAreasViewModel>> UsuarioAreasAsignados([FromRoute] int id)
        {
            var usuario_areas = await _context.usuario_area.Where(r => r.idusuario == id).Include(r => r.areas).ToListAsync();

            return usuario_areas.Select(u => new UsuarioListaAreasViewModel
            {
                idusuario = u.idusuario,
                id_area = u.idarea,
                nombre_area = u.areas.nombre
            });
        }

        //api/UsuariosNet/Login/
        [HttpPost("[action]")]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            //var email = model.email.ToLower();
            var username = model.username.ToLower();
            var cont = 0;
            var contA = 0;
            var prueba1 = "nada";
            var prueba2 = "nada";
            var prueba3 = "nada";
            var prueba4 = "nada";
            var prueba5 = "nada";
            var arrayArea = "";
            var AreaNombreArray = "";

            var usuario = await _context.UsuariosNet.Where(u => u.activo == true).FirstOrDefaultAsync(u => u.username == username);
            //var subRoles = await _context.usuario_rol.Where(r => r.idusuario == usuario.idusuario).ToListAsync();
            var subRoles = await _context.usuario_rol.Where(r => r.user_id == usuario.id).Include(r => r.RolNet).ToListAsync();
            var subAreas = await _context.usuario_area.Where(r => r.idusuario == usuario.id).Include(r => r.areas).ToListAsync();
            contA = subAreas.Count();
            cont = subRoles.Count();

            for (int i = 0; i < subRoles.Count(); i++)
            {
                if (i == 0)
                {
                    prueba1 = subRoles[0].RolNet.role_name;
                }
                else
                {
                    if (i == 1)
                    {
                        prueba2 = subRoles[1].RolNet.role_name;
                    }
                    else
                    {
                        if (i == 2)
                        {
                            prueba3 = subRoles[2].RolNet.role_name;
                        }
                        else
                        {
                            if (i == 3)
                            {
                                prueba4 = subRoles[3].RolNet.role_name;
                            }
                            else
                            {
                                if (i == 4)
                                {
                                    prueba5 = subRoles[4].RolNet.role_name;
                                }
                            }
                        }
                    }
                }
            }

            if (contA > 0)
            {
                for (int i = 0; i < subAreas.Count(); i++)
                {
                    if (arrayArea == "")
                    {
                        arrayArea = subAreas[i].idarea.ToString();
                        AreaNombreArray = subAreas[i].areas.nombre.ToString();
                    }
                    else
                    {
                        arrayArea = arrayArea + "," + subAreas[i].idarea.ToString();
                        AreaNombreArray = AreaNombreArray + ", " + subAreas[i].areas.nombre.ToString();
                    }
                }
            }

            if (usuario == null)
            {
                return NotFound();
            }

            if (!VerificarPasswordHash(model.password, usuario.password_hash, usuario.password_salt))
            {
                return NotFound();
            }

            if (usuario == null)
            {
                return NotFound();
            }

            if (!VerificarPasswordHash(model.password, usuario.password_hash, usuario.password_salt))
            {
                return NotFound();
            }

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, usuario.id.ToString()),
                new Claim(ClaimTypes.Role, prueba1 ),
                new Claim(ClaimTypes.Role, prueba2 ),
                new Claim(ClaimTypes.Role, prueba3 ),
                new Claim(ClaimTypes.Role, prueba4 ),
                new Claim(ClaimTypes.Role, prueba5 ),
                new Claim("idusuario", usuario.id.ToString()),
                new Claim("nombre", usuario.username),
                new Claim("fname", usuario.fname),
                new Claim("lname", usuario.lname),
                new Claim("rol", prueba1),
                new Claim("rol", prueba2),
                new Claim("rol", prueba3),
                new Claim("rol", prueba4),
                new Claim("rol", prueba5),
                new Claim("arrayArea", arrayArea ),
                new Claim("AreaNombreArray", AreaNombreArray ),
                new Claim("imagen", usuario.avatar)
            };

            return Ok(
                    new { token = GenerarToken(claims) }
                );

        }
        private string GenerarToken(List<Claim> claims)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
              _config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              expires: DateTime.Now.AddMinutes(30),
              signingCredentials: creds,
              claims: claims);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private bool VerificarPasswordHash(string password, byte[] passwordHashAlmacenado, byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {
                var passwordHashNuevo = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                return new ReadOnlySpan<byte>(passwordHashAlmacenado).SequenceEqual(new ReadOnlySpan<byte>(passwordHashNuevo));
            }
        }

        //METODO DE ENCRIPTACION DEL PASSWORD
        private void CrearPasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }

        }

        private bool UsuarioNetExists(int id)
        {
            return _context.UsuariosNet.Any(e => e.id == id);
        }
    }
}
