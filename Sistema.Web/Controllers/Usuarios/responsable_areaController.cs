﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sistema.Datos;
using Sistema.Entidades.Usuarios;
using Sistema.Web.Models.Usuarios.UsuarioNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Controllers.UsuariosNet
{
    [Route("api/[controller]")]
    [ApiController]
    public class responsable_areaController : ControllerBase
    {
        private readonly DbContextSistema _context;

        public responsable_areaController(DbContextSistema context)
        {
            _context = context;
        }

        // GET: api/responsable_area
        [HttpGet]
        public async Task<ActionResult<IEnumerable<responsable_area>>> Getresponsable_area()
        {
            return await _context.responsable_area.ToListAsync();
        }

        // GET: api/responsable_area/5
        [HttpGet("{id}")]
        public async Task<ActionResult<responsable_area>> Getresponsable_area(int id)
        {
            var responsable_area = await _context.responsable_area.FindAsync(id);

            if (responsable_area == null)
            {
                return NotFound();
            }

            return responsable_area;
        }

        // GET: api/responsable_area/SelectEspecifico/1
        [HttpGet("[action]/{id}")]
        public async Task<IEnumerable<SelectResposableViewModel>> SelectEspecifico(int id)
        {
            var categoriaPerdida = await _context.responsable_area
                .Where(c => c.idusuario == id)
                .Include(r => r.areas)
                .Include(r => r.UsuarioNet)
                .ToListAsync();
            return categoriaPerdida.Select(c => new SelectResposableViewModel
            {
                idrelacion = c.idrelacion,
                idusuario = c.idusuario,
                idarea = c.idarea,
                nombreArea = c.areas.nombre,
                nombre = c.UsuarioNet.fname,
                apellido = c.UsuarioNet.lname
            });
        }

        // GET: api/responsable_area/listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<SelectPrueba>> listar()
        {
            var categoriaPerdida = await _context.responsable_area.Include(r => r.areas).Include(r => r.UsuarioNet).ToListAsync();
            return categoriaPerdida.Select(c => new SelectPrueba
            {
                idusuario = c.idusuario,
                nombre = c.UsuarioNet.fname,
                apellido = c.UsuarioNet.lname
            });
        }

        // GET: api/responsable_area/SelectUserArea/1/1
        [HttpGet("[action]/{id_user}/{id_area}")]
        public async Task<IEnumerable<Responsable_areaViewModel>> SelectUserArea(int id_user, int id_area)
        {
            var categoriaPerdida = await _context.responsable_area.Where(c => c.idusuario == id_user).Where(c => c.idarea == id_area).ToListAsync();

            return categoriaPerdida.Select(c => new Responsable_areaViewModel
            {
                idrelacion = c.idrelacion,
                idusuario = c.idusuario,
                idarea = c.idarea
            });

        }

        // DELETE: api/responsable_area/Delete_responsable_area/33/1
        [HttpDelete("[action]/{id_user}/{id_area}")]
        public async Task<ActionResult<responsable_area>> Delete_responsable_area(int id_user, int id_area)
        {
            var responsable_area = await _context.responsable_area.Where(c => c.idusuario == id_user).Where(c => c.idarea == id_area).FirstOrDefaultAsync();
            if (responsable_area == null)
            {
                return NotFound();
            }

            _context.responsable_area.Remove(responsable_area);
            await _context.SaveChangesAsync();

            return responsable_area;
        }

        // POST: api/responsable_area/CrearResponsable
        [HttpPost("[action]")]
        public async Task<IActionResult> CrearResponsable([FromBody] CrearResponsableAreaViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            responsable_area nuevo_area = new responsable_area
            {
                idusuario = model.idusuario,
                idarea = model.idarea,
            };

            _context.responsable_area.Add(nuevo_area);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

            return Ok();
        }

        // PUT: api/responsable_area/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Putresponsable_area(int id, responsable_area responsable_area)
        {
            if (id != responsable_area.idrelacion)
            {
                return BadRequest();
            }

            _context.Entry(responsable_area).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!responsable_areaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/responsable_area
        [HttpPost]
        public async Task<ActionResult<responsable_area>> Postresponsable_area(responsable_area responsable_area)
        {
            _context.responsable_area.Add(responsable_area);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Getresponsable_area", new { id = responsable_area.idrelacion }, responsable_area);
        }

        // DELETE: api/responsable_area/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<responsable_area>> Deleteresponsable_area(int id)
        {
            var responsable_area = await _context.responsable_area.FindAsync(id);
            if (responsable_area == null)
            {
                return NotFound();
            }

            _context.responsable_area.Remove(responsable_area);
            await _context.SaveChangesAsync();

            return responsable_area;
        }

        private bool responsable_areaExists(int id)
        {
            return _context.responsable_area.Any(e => e.idrelacion == id);
        }
    }
}
