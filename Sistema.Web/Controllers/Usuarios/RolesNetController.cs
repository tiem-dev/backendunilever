﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sistema.Datos;
using Sistema.Entidades.Usuarios;
using Sistema.Web.Models.Usuarios.Role;

namespace Sistema.Web.Controllers.UsuariosNet
{
    [Route("api/[controller]")]
    [ApiController]
    public class RolesNetController : ControllerBase
    {
        private readonly DbContextSistema _context;

        public RolesNetController(DbContextSistema context)
        {
            _context = context;
        }

        //GET: api/RolesNet/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<RolViewModel>> Listar()
        {
            var rol = await _context.RolesNet.ToListAsync();

            return rol.Select(c => new RolViewModel
            {
                idrol = c.id,
                nombre = c.role_name,
                condicion = c.activo
            });

        }

        //GET: api/RolesNet/ListarAgregar
        [HttpGet("[action]")]
        public async Task<IEnumerable<RolViewModel>> ListarAgregar()
        {
            var rol = await _context.RolesNet.Where(a => a.activo == true).ToListAsync();

            return rol.Select(c => new RolViewModel
            {
                idrol = c.id,
                nombre = c.role_name,
                condicion = c.activo
            });

        }

        // GET: api/Roles/ListarRoles/texto
        [HttpGet("[action]/{texto}")]
        public async Task<IEnumerable<RolViewModel>> ListarRoles([FromRoute] string texto)
        {
            var rol = await _context.RolesNet.
                Where(r => r.role_name.Contains(texto)).
                Where(r => r.activo == true).
                ToListAsync();

            return rol.Select(c => new RolViewModel
            {
                idrol = c.id,
                nombre = c.role_name,
                condicion = c.activo
            });

        }

        // GET: api/Roles/Select
        [HttpGet("[action]")]
        public async Task<IEnumerable<SelectViewModel>> Select()
        {
            var rol = await _context.RolesNet.Where(r => r.activo == true).ToListAsync();

            return rol.Select(r => new SelectViewModel
            {
                idrol = r.id,
                nombre = r.role_name
            });

        }

        // GET: api/Roles/buscarRol
        [HttpGet("[action]/{busquedaRol}")]
        public async Task<IActionResult> buscarRol([FromRoute] string busquedaRol)
        {
            var rol = await _context.RolesNet
                .Where(r => r.activo == true).
                SingleOrDefaultAsync(r => r.role_name == busquedaRol);
            if (rol == null)
            {
                return NotFound();
            }
            return Ok(new RolViewModel
            {
                idrol = rol.id,
                nombre = rol.role_name
            });

        }


        private bool RolNetExists(int id)
        {
            return _context.RolesNet.Any(e => e.id == id);
        }
    }
}
