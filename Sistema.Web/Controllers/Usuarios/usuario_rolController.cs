﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sistema.Datos;
using Sistema.Entidades.Usuarios;
using Sistema.Web.Models.Usuarios.UsuarioNet;

namespace Sistema.Web.Controllers.UsuariosNet
{
    [Route("api/[controller]")]
    [ApiController]
    public class usuario_rolController : ControllerBase
    {
        private readonly DbContextSistema _context;

        public usuario_rolController(DbContextSistema context)
        {
            _context = context;
        }

        // GET: api/usuario_rol
        [HttpGet]
        public async Task<ActionResult<IEnumerable<usuario_rol>>> Getusuario_rol()
        {
            return await _context.usuario_rol.ToListAsync();
        }

        // GET: api/usuario_rol/SelectEspecifico/1
        [HttpGet("[action]/{id}")]
        public async Task<IEnumerable<Usuario_rolViewModel>> SelectEspecifico(int id)
        {
            var categoriaPerdida = await _context.usuario_rol.Where(c => c.user_id == id).ToListAsync();

            return categoriaPerdida.Select(c => new Usuario_rolViewModel
            {
                id = c.id,
                user_id = c.user_id,
                role_id = c.role_id
            });

        }

        // GET: api/usuario_rol/SelectEspecifico/1
        [HttpGet("[action]/{id_user}/{id_rol}")]
        public async Task<IEnumerable<Usuario_rolViewModel>> SelectUserRol(int id_user, int id_rol)
        {
            var categoriaPerdida = await _context.usuario_rol.Where(c => c.user_id == id_user).Where(c => c.role_id == id_rol).ToListAsync();

            return categoriaPerdida.Select(c => new Usuario_rolViewModel
            {
                id = c.id,
                user_id = c.user_id,
                role_id = c.role_id
            });

        }

        // DELETE: api/usuario_rol/Deleteusuario_rol/33/1
        [HttpDelete("[action]/{id_user}/{id_rol}")]
        public async Task<ActionResult<usuario_rol>> Deleteusuario_rol(int id_user, int id_rol)
        {
            var usuario_rol = await _context.usuario_rol.Where(c => c.user_id == id_user).Where(c => c.role_id == id_rol).FirstOrDefaultAsync();
            if (usuario_rol == null)
            {
                return NotFound();
            }

            _context.usuario_rol.Remove(usuario_rol);
            await _context.SaveChangesAsync();

            return usuario_rol;
        }

        // POST: api/usuario_rol/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> CrearRol([FromBody] CrearUsuarioRolViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            usuario_rol nuevo_role = new usuario_rol
            {
                user_id = model.user_id,
                role_id = model.role_id,
            };

            _context.usuario_rol.Add(nuevo_role);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

            return Ok();
        }


        // GET: api/usuario_rol/5
        [HttpGet("{id}")]
        public async Task<ActionResult<usuario_rol>> Getusuario_rol(int id)
        {
            var usuario_rol = await _context.usuario_rol.FindAsync(id);

            if (usuario_rol == null)
            {
                return NotFound();
            }

            return usuario_rol;
        }

        // PUT: api/usuario_rol/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Putusuario_rol(int id, usuario_rol usuario_rol)
        {
            if (id != usuario_rol.id)
            {
                return BadRequest();
            }

            _context.Entry(usuario_rol).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!usuario_rolExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        
        
        private bool usuario_rolExists(int id)
        {
            return _context.usuario_rol.Any(e => e.id == id);
        }
    }
}
