﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sistema.Datos;
using Sistema.Entidades.Usuarios;
using Sistema.Entidades.Wcm;
using Sistema.Entidades.Wcm._1_N;
using Sistema.Web.Controllers.CalculosAuxliares;
using Sistema.Web.Models.Wcm._1_N.RegistroAnomalia;
using Sistema.Web.Models.Wcm.Matriz;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Controllers.Wcm.Matriz
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegistrosanomaliasMatrizController : ControllerBase
    {
        private readonly DbContextSistema _context;

        public RegistrosanomaliasMatrizController(DbContextSistema context)
        {
            _context = context;
        }

        //Controlador carga de usuarios reponsables por area
        // GET: api/RegistrosanomaliasMatriz/MatrizDatosResponsable
        [HttpGet("[action]")]
        public async Task<IEnumerable<ResponsablesViewModel>> MatrizDatosResponsable()
        {
            List<UsuarioNet> usuarios = await _context.UsuariosNet.ToListAsync();

            var trendData =
             (from u in usuarios
              join r in _context.responsable_area
               on u.id equals r.idusuario
              join a in _context.Areas
              on r.idarea equals a.idarea
              group u by new
              {
                  Usuario = u.username

              } into g
              select new
              {
                  responsable = g.Key.Usuario,
              }
        ).AsEnumerable()
         .Select(g => new
         {
             Usuario = g.responsable
         })
          .OrderBy(x => x.Usuario);
            return trendData.Select(v => new ResponsablesViewModel
            {
                usuarioResponsable = v.Usuario
            });
        }
        //Lista las areas con la relacion de areas y usuarios reponsabnles
        // GET: api/RegistrosanomaliasMatriz/MatrizDatos
        [HttpGet("[action]/{usuario}")]
        public async Task<IEnumerable<AreasViewModel>> MatrizDatosAreas([FromRoute] int usuario)

        {

            List<Area> areas = await _context.Areas.ToListAsync();

            var trendData =
             (from a in areas
              join resp in _context.responsable_area
              on a.idarea equals resp.idarea

              join u in _context.UsuariosNet
             on resp.idusuario equals u.id
              where u.id == usuario

              group a by new
              {
                  Area = a.idarea,
                  Nombre = a.nombre
              } into g
              select new
              {
                  Area = g.Key.Area,
                  Nombre = g.Key.Nombre
              }
        ).AsEnumerable()
         .Select(g => new
         {

             Area = g.Area,
             Nombre = g.Nombre,

         })
          .OrderBy(x => x.Area);

            return trendData.Select(v => new AreasViewModel
            {

                idarea = v.Area,
                area = v.Nombre,

            });



        }

        //Listar datos principlaes de datos y total resultados de matriz tarjetas

        //Preubas unitarios de los datos

        // GET: api/RegistrosanomaliasMatriz/MatrizDatos
        [HttpGet("[action]/{anio}/{estadoDataTarjeta}/{usuario}")]
        public async Task<IEnumerable<MatrizViewModel>> MatrizDatos([FromRoute] int anio, string estadoDataTarjeta, int usuario)

        {
            ValidacionIsNullOrEmpy validar = new ValidacionIsNullOrEmpy();

            List<Registrosanomalias> anomalias = await _context.Registrosanomalias.ToListAsync();

            var trendData =
             (from r in anomalias
              join resp in _context.responsable_area
              on r.idarea equals resp.idarea

              join a in _context.Areas
               on resp.idarea equals a.idarea

              join u in _context.UsuariosNet
             on resp.idusuario equals u.id
              where r.confirmacion_tec == validar.ControlEstadoTarjetaAtendido(estadoDataTarjeta)


              && r.emision_ts.Year == anio && u.id == usuario
              group r by new
              {
                  Year = r.emision_ts.Year,
                  // Month = d.emision_ts.Month,
                  Area = a.idarea,
                  Nombre = a.nombre
              } into g
              select new
              {
                  Year = g.Key.Year,
                  //Month = g.Key.Month,
                  Area = g.Key.Area,
                  Nombre = g.Key.Nombre,
                  Total = g.Count(),

                  // AveragePerTrans = Math.Round(g.Average(x => x.idregistroanomalia), 2)
              }
        ).AsEnumerable()
         .Select(g => new
         {
             //Period = g.Year + "-" + g.Month,
             Year = g.Year,
             Area = g.Area,
             Nombre = g.Nombre,
             Total = g.Total,
         })
          .OrderBy(x => x.Area)
           .Take(12);

            return trendData.Select(v => new MatrizViewModel
            {
                mes = v.Year,
                area = v.Area,
                nombre = v.Nombre,
                total = v.Total
            });



        }
        //Relizar el calculo de la matriz total
        //Calculo mejroado de datos
        private bool RegistrosanomaliasExists(int id)
        {
            return _context.Registrosanomalias.Any(e => e.idregistroanomalia == id);
        }
    }
}
