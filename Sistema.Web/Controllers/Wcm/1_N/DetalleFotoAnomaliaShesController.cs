﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sistema.Datos;
using Sistema.Entidades.Wcm._1_N;
using Sistema.Web.Models.Wcm._1_N.FotoShe;

namespace Sistema.Web.Controllers.Wcm._1_N
{
    [Route("api/[controller]")]
    [ApiController]
    public class DetalleFotoAnomaliaShesController : ControllerBase
    {
        private readonly DbContextSistema _context;

        public DetalleFotoAnomaliaShesController(DbContextSistema context)
        {
            _context = context;
        }
        // Get: api/DetalleFotoAnomaliaShes/VerFotoIngreso/id
        [HttpGet("[action]/{id}")]
        public async Task<IEnumerable<VerFotoViewModel>> VerFotoIngreso([FromRoute] int id)
        {
            var anomalia = await _context.DetallefotoShe.Where(a => a.idshe == id && a.tipo_foto == "Ingreso" && a.eliminado == false)
               .ToListAsync();

            return anomalia.Select(p => new VerFotoViewModel
            {
                idfoto = p.idfoto,
                idshe = p.idshe,
                foto_anomalia = p.foto_anomalia,
                tipo_foto = p.tipo_foto,



            });
        }

        // PUT: api/DetalleFotoAnomaliaShes/Actualizar
        //[Authorize(Roles = "Almacenero,Vendedor,Administrador")]
        [HttpPut("[action]")]
        public async Task<IActionResult> Actualizar([FromBody] ActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idshe <= 0)
            {
                return BadRequest();
            }

            var foto = await _context.DetallefotoShe.FirstOrDefaultAsync(p => p.idshe == model.idshe && p.tipo_foto == "Ingreso");
            var fotoicon = await _context.DetallefotosThumbnailShe.FirstOrDefaultAsync(p => p.idshe == model.idshe && p.tipo_foto == "Ingreso");

            if (foto == null)
            {
                return NotFound();
            }

            foto.foto_anomalia = model.foto_anomalia;
            fotoicon.foto_anomalia = model.foto_anomalia_icon;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }

        // POST: api/DetalleFotoAnomaliaShes/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] CrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            DetalleFotoAnomaliaShe foto = new DetalleFotoAnomaliaShe
            {
                idshe = model.idregistroshe,
                foto_anomalia = model.foto_anomalia,
                tipo_foto = model.tipo_foto,
                eliminado = false
            };

            _context.DetallefotoShe.Add(foto);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

            return Ok();
        }


        // Get: api/DetalleFotoAnomaliaShes/VerFotoSolucion/id
        [HttpGet("[action]/{id}")]
        public async Task<IEnumerable<VerFotoViewModel>> VerFotoSolucion([FromRoute] int id)
        {
            var anomalia = await _context.DetallefotoShe.Where(a => a.idshe == id && a.tipo_foto == "Solucion" && a.eliminado == false)
               .ToListAsync();

            return anomalia.Select(p => new VerFotoViewModel
            {
                idfoto = p.idfoto,
                idshe = p.idshe,
                foto_anomalia = p.foto_anomalia,
                tipo_foto = p.tipo_foto,



            });
        }

        // PUT: api/DetalleFotoAnomaliaShes/ActualizarFotoSolucion
        //[Authorize(Roles = "Almacenero,Vendedor,Administrador")]
        [HttpPut("[action]")]
        public async Task<IActionResult> ActualizarFotoSolucion([FromBody] ActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idfoto <= 0)
            {
                return BadRequest();
            }

            var foto = await _context.DetallefotoShe.FirstOrDefaultAsync(p => p.idfoto == model.idfoto && p.tipo_foto == "Solucion");

            if (foto == null)
            {
                return NotFound();
            }

            foto.foto_anomalia = model.foto_anomalia;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }
        // GET: api/DetalleFotoAnomaliaShes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DetalleFotoAnomaliaShe>>> GetDetallefotoShe()
        {
            return await _context.DetallefotoShe.ToListAsync();
        }

        // GET: api/DetalleFotoAnomaliaShes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DetalleFotoAnomaliaShe>> GetDetalleFotoAnomaliaShe(int id)
        {
            var detalleFotoAnomaliaShe = await _context.DetallefotoShe.FindAsync(id);

            if (detalleFotoAnomaliaShe == null)
            {
                return NotFound();
            }

            return detalleFotoAnomaliaShe;
        }

        // PUT: api/DetalleFotoAnomaliaShes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDetalleFotoAnomaliaShe(int id, DetalleFotoAnomaliaShe detalleFotoAnomaliaShe)
        {
            if (id != detalleFotoAnomaliaShe.idfoto)
            {
                return BadRequest();
            }

            _context.Entry(detalleFotoAnomaliaShe).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DetalleFotoAnomaliaSheExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/DetalleFotoAnomaliaShes
        [HttpPost]
        public async Task<ActionResult<DetalleFotoAnomaliaShe>> PostDetalleFotoAnomaliaShe(DetalleFotoAnomaliaShe detalleFotoAnomaliaShe)
        {
            _context.DetallefotoShe.Add(detalleFotoAnomaliaShe);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDetalleFotoAnomaliaShe", new { id = detalleFotoAnomaliaShe.idfoto }, detalleFotoAnomaliaShe);
        }

        // DELETE: api/DetalleFotoAnomaliaShes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<DetalleFotoAnomaliaShe>> DeleteDetalleFotoAnomaliaShe(int id)
        {
            var detalleFotoAnomaliaShe = await _context.DetallefotoShe.FindAsync(id);
            if (detalleFotoAnomaliaShe == null)
            {
                return NotFound();
            }

            _context.DetallefotoShe.Remove(detalleFotoAnomaliaShe);
            await _context.SaveChangesAsync();

            return detalleFotoAnomaliaShe;
        }

        private bool DetalleFotoAnomaliaSheExists(int id)
        {
            return _context.DetallefotoShe.Any(e => e.idfoto == id);
        }
    }
}
