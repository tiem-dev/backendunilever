﻿//MODIFICADO 12/08/2019 ff
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sistema.Datos;
using Sistema.Entidades.Proyecto;
using Sistema.Entidades.Usuarios;
using Sistema.Entidades.Wcm;
using Sistema.Entidades.Wcm._1_N;
using Sistema.Web.Controllers.CalculosAuxliares;
using Sistema.Web.Models.Wcm._1_N.RegistroAnomalia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Controllers.Wcm._1_N
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegistrosAnomaliasController : ControllerBase
    {
        private readonly DbContextSistema _context;

        public RegistrosAnomaliasController(DbContextSistema context)
        {
            _context = context;
        }

        // GET: api/RegistrosAnomalias/TarjetasMes12
        //[Authorize(Roles = "Almacenero,Vendedor,Administrador")]

        [HttpGet("[action]/{anio}/{estadoDataTarjeta}/{tipoCriticidad}/{tarjeta}")]
        public async Task<IEnumerable<ConsultaViewModel>> VentasMes12([FromRoute] int anio, string estadoDataTarjeta, string tipoCriticidad, int tarjeta)
        {

            ValidacionIsNullOrEmpy validar = new ValidacionIsNullOrEmpy();

            List<Registrosanomalias> anomalias = await _context.Registrosanomalias.ToListAsync();

            if (estadoDataTarjeta == "ATENDIDO")
            {
                var trendData =
                 (from d in anomalias
                  where d.confirmacion_tec == validar.ControlEstadoTarjetaAtendido(estadoDataTarjeta)

                  && d.emision_ts.Year == anio && d.criticidad == tipoCriticidad && d.idtarjeta == tarjeta
                  group d by new
                  {
                      Year = d.emision_ts.Year,
                      Month = d.emision_ts.Month
                  } into g
                  select new
                  {
                      Year = g.Key.Year,
                      Month = g.Key.Month,
                      Total = g.Count(),

                      // AveragePerTrans = Math.Round(g.Average(x => x.idregistroanomalia), 2)
                  }
            ).AsEnumerable()
             .Select(g => new
             {
                 //Period = g.Year + "-" + g.Month,
                 Mes = g.Month,
                 Total = g.Total,
             })
              .OrderBy(x => x.Mes)
               .Take(12);

                return trendData.Select(v => new ConsultaViewModel
                {
                    mes = v.Mes,
                    criticidad = tipoCriticidad,
                    total = v.Total
                });
            }
            else
            {

                var trendData =
                (from d in anomalias
                 where d.emision_ts.Year == anio && d.criticidad == tipoCriticidad && d.idtarjeta == tarjeta
                 group d by new
                 {
                     Year = d.emision_ts.Year,
                     Month = d.emision_ts.Month
                 } into g
                 select new
                 {
                     Year = g.Key.Year,
                     Month = g.Key.Month,
                     Total = g.Count(),

                     // AveragePerTrans = Math.Round(g.Average(x => x.idregistroanomalia), 2)
                 }
           ).AsEnumerable()
            .Select(g => new
            {
                //Period = g.Year + "-" + g.Month,
                Mes = g.Month,
                Total = g.Total,
            })
             .OrderBy(x => x.Mes)
              .Take(12);

                return trendData.Select(v => new ConsultaViewModel
                {
                    mes = v.Mes,
                    criticidad = tipoCriticidad,
                    total = v.Total
                });


            }


            //var consulta = await _context.Registrosanomalias
            //    // .Where(i => i.idtarjeta == 1)
            //    .Where(i => i.confirmacion_tec == true)
            //    .Where(i => i.confirmacion_super == false)
            //    .Where(i => i.confirmacion_lider == false)
            //    .Where(i => i.eliminado == false)
            //    //.GroupBy(v => v.criticidad.Month , v => v.criticidad)
            //    .GroupBy(v => new { v.emision_ts.Month, v.criticidad })
            //    .Select(x => new
            //    {
            //        Periodo = x.Key.Month,
            //        Criticidad = x.Key.criticidad,
            //        Result = x.Count(),
            //    })
            //    .OrderByDescending(x => x.Periodo)
            //    .Take(12)
            //    .ToListAsync();


            //List<string> allRecords = new List<string>();
            ////List<string> allTheData = new List<string>();
            ////S sheets = spreadsheetDocument.WorkbookPart.Workbook.Sheets;


            //    foreach (var attr in consulta)
            //    {

            //        Console.WriteLine(attr);
            //        allRecords.Add(attr.Result.ToString());
            //    }
            //    var list = allRecords.ToList();



        }



        // GET: api/RegistrosAnomalias/Listar
        // [Authorize(Roles = "Almacenero,Administrador")]
        [HttpGet("[action]")]
        public async Task<IEnumerable<RegistroAnomaliaViewModel>> Listar()
        {
            List<Registrosanomalias> anomalia = await _context.Registrosanomalias
               .Include(i => i.usuario)
               .Include(i => i.area)
               .Include(i => i.maquina)
               .Include(i => i.anomalia)
               .Include(i => i.suceso)
               .Include(i => i.tarjeta)
               .Where(i => i.eliminado == false)
               .OrderByDescending(i => i.idregistroanomalia)
               .Take(100)
               .ToListAsync();

            return anomalia.Select(i => new RegistroAnomaliaViewModel
            {
                idregistroanomalia = i.idregistroanomalia,
                codigo = i.codigo,
                idusuario = i.idusuario,
                usuario = i.usuario.username,
                emision_ts = i.emision_ts.ToString("dd/MM/yyyy HH:mm"),
                paso_ma = i.paso_ma,
                criticidad = i.criticidad,
                turno = i.turno,
                idarea = i.idarea,
                area = i.area.nombre,
                idmaquina = i.idmaquina,
                maquina = i.maquina.nombre,
                idanomalia = i.idanomalia,
                anomaliac = i.anomalia.nombre,
                idsuceso = i.idsuceso,
                relacionado = i.suceso.nombre,
                idtarjeta = i.idtarjeta,
                tarjeta = i.tarjeta.nombre,
                descripcion = i.descripcion,


            });


        }



        // GET: api/RegistrosAnomalias/ListarTipoTarjetaOptimize   por id
        // [Authorize(Roles = "Almacenero,Administrador")]
        [HttpGet("[action]/{id}")]
        public async Task<IEnumerable<RegistroAnomaliaViewModel>> ListarTipoTarjetaOptimize([FromRoute] int id)
        {
            //Metodo de calculo de dias vencimiento
            CriticidadActual calDias = new CriticidadActual();

            List<Registrosanomalias> anomalia = await _context.Registrosanomalias

                .Include(i => i.usuario)
                .Include(i => i.area)
                .Include(i => i.sector)
                .Include(i => i.maquina)
                .Include(i => i.anomalia)
                .Include(i => i.proyecto)
                .Include(i => i.suceso)
                .Include(i => i.tarjeta)
                .Include(i => i.foto_icon)
                //.Include(i=>i.detalles)
                .Where(i => i.idtarjeta == id)
                .Where(i => i.confirmacion_tec == false)
                .Where(i => i.confirmacion_super == false)
                .Where(i => i.confirmacion_lider == false)
                .Where(i => i.eliminado == false)

                .OrderBy(i => i.programado_ts)
                .Take(100)
                .ToListAsync();

            return anomalia.Select(i => new RegistroAnomaliaViewModel
            {

                idregistroanomalia = i.idregistroanomalia,
                codigo = i.codigo,
                //emision_ts = i.emision_ts.ToString("dd/MM/yy HH:mm"),
                emision_ts = i.emision_ts.ToString("dd/MM/yy"),
                idusuario = i.idusuario,
                usuario = i.usuario.username,
                //avatar = i.usuario.avatar,
                paso_ma = i.paso_ma,
                criticidad = i.criticidad,
                turno = i.turno,
                idarea = i.idarea,
                area = i.area.nombre,
                idsector = i.idsector,
                sector = i.sector.nombre,
                idmaquina = i.idmaquina,
                maquina = i.maquina.nombre,
                idanomalia = i.idanomalia,
                anomaliac = i.anomalia.nombre,
                idsuceso = i.idsuceso,
                relacionado = i.suceso.nombre,
                idtarjeta = i.idtarjeta,
                tarjeta = i.tarjeta.nombre,
                idproyecto = i.idproyecto.Value,
                proyecto = i.proyecto.nombre,
                descripcion = i.descripcion,
                programado_ts = i.programado_ts.ToString("dd/MM/yy HH:mm"),
                tiempo_vence = calDias.VmtoDias(i.programado_ts),
                alarma = calDias.Alarma(i.programado_ts),
                // valor true: ya no puede modificar la tarjeta , false: aun puede modificar la tarjeta
                edicion = calDias.TiempoLimiteModificarTarjeta(i.emision_ts),
                foto = i.foto_icon.foto_anomalia



            });


        }

        /// <summary>
        /// CONTROL DE DATOS POR JOIN INER JOIN
        /// </summary>
        /// <returns></returns>
        /// 


        // GET: api/RegistrosAnomalias/ListarTipoTarjetaJoin   por id
        // [Authorize(Roles = "Almacenero,Administrador")]
        [HttpGet("[action]/{id}")]
        public async Task<IEnumerable<RegistroAnomaliaViewModel>> ListarTipoTarjetaJoin([FromRoute] int id)
        {
            //Metodo de calculo de dias vencimiento
            CriticidadActual calDias = new CriticidadActual();


            List<Registrosanomalias> anomalias = await _context.Registrosanomalias.ToListAsync();
            List<DetalleFotoAnomaliaThumbnail> fotos = await _context.DetallefotosThumbnail.ToListAsync();
            List<UsuarioNet> usuarios = await _context.UsuariosNet.ToListAsync();
            List<Area> areas = await _context.Areas.ToListAsync();
            List<Sector> sector = await _context.Sector.ToListAsync();
            List<Maquina> maquinas = await _context.Maquinas.ToListAsync();
            List<Anomalia> anomalia = await _context.Anomalias.ToListAsync();
            List<Suceso> suceso = await _context.Sucesos.ToListAsync();
            List<Tarjeta> tarjeta = await _context.Tarjetas.ToListAsync();
            List<Proyectos> proyecto = await _context.Proyectos.ToListAsync();

            var entryPoint = (from re in anomalias
                              join usu in usuarios on re.idusuario equals usu.id
                              join area in areas on re.idarea equals area.idarea
                              join sec in sector on re.idsector equals sec.idsector
                              join anom in anomalia on re.idanomalia equals anom.idanomalia
                              join suc in suceso on re.idsuceso equals suc.idsuceso
                              join tar in tarjeta on re.idtarjeta equals tar.idtarjeta
                              join pro in proyecto on re.idproyecto equals pro.idproyecto
                              join ma in maquinas on re.idmaquina equals ma.idmaquina
                              join fo in fotos on re.idregistroanomalia equals fo.idregistroanomalia

                              where fo.tipo_foto == "Ingreso" && re.idtarjeta == id
                                     && re.confirmacion_tec == false && re.confirmacion_super == false
                                     && re.confirmacion_lider == false && re.eliminado == false
                              orderby re.programado_ts ascending
                              select new RegistroAnomaliaViewModel
                              {
                                  idregistroanomalia = re.idregistroanomalia,
                                  codigo = re.codigo,
                                  //emision_ts = i.emision_ts.ToString("dd/MM/yy HH:mm"),
                                  emision_ts = re.emision_ts.ToString("dd/MM/yy"),
                                  idusuario = re.idusuario,
                                  usuario = usu.username,
                                  //avatar = i.usuario.avatar,
                                  paso_ma = re.paso_ma,
                                  criticidad = re.criticidad,
                                  turno = re.turno,
                                  idarea = re.idarea,
                                  area = area.nombre,
                                  idsector = re.idsector,
                                  sector = sec.nombre,
                                  idmaquina = re.idmaquina,
                                  maquina = ma.nombre,
                                  idanomalia = re.idanomalia,
                                  anomaliac = anom.nombre,
                                  idsuceso = re.idsuceso,
                                  relacionado = suc.nombre,
                                  idtarjeta = re.idtarjeta,
                                  tarjeta = tar.nombre,
                                  idproyecto = re.idproyecto.Value,
                                  proyecto = pro.nombre,
                                  descripcion = re.descripcion,
                                  programado_ts = re.programado_ts.ToString("dd/MM/yy HH:mm"),
                                  tiempo_vence = calDias.VmtoDias(re.programado_ts),
                                  alarma = calDias.Alarma(re.programado_ts),
                                  // valor true: ya no puede modificar la tarjeta , false: aun puede modificar la tarjeta
                                  edicion = calDias.TiempoLimiteModificarTarjeta(re.emision_ts),
                                  // foto = fo.foto_anomalia
                              });
            return entryPoint.ToList();
        }





        // GET: api/RegistrosAnomalias/ListarTipoTarjetaAmarilla 
        // [Authorize(Roles = "Almacenero,Administrador")]
        [HttpGet("[action]")]
        public async Task<IEnumerable<RegistroAnomaliaViewModel>> ListarTipoTarjetaAmarilla()
        {
            //Metodo de calculo de dias vencimiento
            CriticidadActual calDias = new CriticidadActual();

            List<Registrosanomalias> anomalia = await _context.Registrosanomalias
                .Include(i => i.usuario)
                .Include(i => i.area)
                .Include(i => i.sector)
                .Include(i => i.maquina)
                .Include(i => i.anomalia)
                .Include(i => i.proyecto)
                .Include(i => i.suceso)
                .Include(i => i.tarjeta)
                .Include(i => i.foto_icon)

                .Where(i => i.idtarjeta == 3)
                .Where(i => i.confirmacion_tec == false)
                .Where(i => i.confirmacion_super == false)
                .Where(i => i.confirmacion_lider == false)
                .Where(i => i.eliminado == false)

                .OrderBy(i => i.programado_ts)
                .Take(100)
                .ToListAsync();

            return anomalia.Select(i => new RegistroAnomaliaViewModel
            {

                idregistroanomalia = i.idregistroanomalia,
                codigo = i.codigo,
                //emision_ts = i.emision_ts.ToString("dd/MM/yy HH:mm"),
                emision_ts = i.emision_ts.ToString("dd/MM/yy"),
                idusuario = i.idusuario,
                usuario = i.usuario.username,
                //avatar = i.usuario.avatar,
                paso_ma = i.paso_ma,
                criticidad = i.criticidad,
                turno = i.turno,
                idarea = i.idarea,
                area = i.area.nombre,
                idsector = i.idsector,
                sector = i.sector.nombre,
                idmaquina = i.idmaquina.Value,
                maquina = i.maquina.nombre,
                idanomalia = i.idanomalia,
                anomaliac = i.anomalia.nombre,
                idsuceso = i.idsuceso,
                relacionado = i.suceso.nombre,
                idtarjeta = i.idtarjeta,
                tarjeta = i.tarjeta.nombre,
                idproyecto = i.idproyecto.Value,
                proyecto = i.proyecto.nombre,
                descripcion = i.descripcion,
                programado_ts = i.programado_ts.ToString("dd/MM/yy HH:mm"),
                tiempo_vence = calDias.VmtoDias(i.programado_ts),
                alarma = calDias.Alarma(i.programado_ts),
                // valor true: ya no puede modificar la tarjeta , false: aun puede modificar la tarjeta
                edicion = calDias.TiempoLimiteModificarTarjeta(i.emision_ts),
                foto = i.foto_icon.foto_anomalia



            });


        }

        // GET: api/RegistrosAnomalias/ListarTipoTarjetaRecibirAmarillo   por id
        // [Authorize(Roles = "Almacenero,Administrador")]
        [HttpGet("[action]")]
        public async Task<IEnumerable<RegistroViewModelRecibir>> ListarTipoTarjetaRecibirAmarillo()
        {
            ValidacionIsNullOrEmpy validar = new ValidacionIsNullOrEmpy();

            List<Registrosanomalias> anomalia = await _context.Registrosanomalias

                .Include(i => i.usuario)
                .Include(i => i.usuariotecnico)
                .Include(i => i.area)
                .Include(i => i.maquina)
                .Include(i => i.anomalia)
                .Include(i => i.suceso)
                .Include(i => i.tarjeta)
                .Include(i => i.foto_icon)
                .Include(i => i.proyecto)
                .Where(i => i.idtarjeta == 3)
                .Where(i => i.confirmacion_tec == true)
                .Where(i => i.confirmacion_super == false)
                .Where(i => i.confirmacion_lider == false)
                .Where(i => i.eliminado == false)

                .OrderByDescending(i => i.idregistroanomalia)
                .Take(100)
                .ToListAsync();

            return anomalia.Select(i => new RegistroViewModelRecibir
            {
                idregistroanomalia = i.idregistroanomalia,
                codigo = i.codigo,
                emision_ts = i.emision_ts.ToString("dd/MM/yyyy HH:mm"),
                idusuario = i.idusuario,
                usuario = i.usuario.username,
                tecnico = i.usuariotecnico.username,
                paso_ma = i.paso_ma,
                criticidad = i.criticidad,
                turno = i.turno,
                idarea = i.idarea,
                area = i.area.nombre,
                idmaquina = i.idmaquina.Value,
                maquina = i.maquina.nombre,
                idanomalia = i.idanomalia,
                anomaliac = i.anomalia.nombre,
                idsuceso = i.idsuceso,
                relacionado = i.suceso.nombre,
                idtarjeta = i.idtarjeta,
                tarjeta = i.tarjeta.nombre,
                idproyecto = i.idproyecto.Value,
                proyecto = i.proyecto.nombre,
                descripcion = i.descripcion,
                sol_implementada = i.sol_implementada,
                fecha_solucion = i.ejecucion_ts.ToString("dd/MM/yyyy HH:mm"),
                foto = i.foto_icon.foto_anomalia



            });


        }


        // GET: api/RegistrosAnomalias/ListarTarjetaSectores   por id, idareas
        // [Authorize(Roles = "Almacenero,Administrador")]
        [HttpGet("[action]/{id}/{idSectorData}")]
        public async Task<IEnumerable<RegistroAnomaliaViewModel>> ListarTarjetaSectores([FromRoute] int id, string idSectorData)
        {
            //Metodo de calculo de dias vencimiento
            CriticidadActual calDias = new CriticidadActual();

            ValidacionIsNullOrEmpy validar = new ValidacionIsNullOrEmpy();
#pragma warning disable EF1000 // Possible SQL injection vulnerability.
            List<Registrosanomalias> anomalia = await _context.Registrosanomalias

                .FromSql($"SELECT * FROM tb_registro_anomalia where " +
                          $"" + validar.ValidarDatoCadena(idSectorData, "idarea") + " IN (" + validar.ValidarDato(idSectorData) + ")")
                .Include(i => i.usuario)
                .Include(i => i.area)
                .Include(i => i.sector)
                .Include(i => i.maquina)
                .Include(i => i.anomalia)
                .Include(i => i.proyecto)
                .Include(i => i.suceso)
                .Include(i => i.tarjeta)
                .Include(i => i.foto_icon)
                .Where(i => i.idtarjeta == id)
                .Where(i => i.confirmacion_tec == false)
                .Where(i => i.confirmacion_super == false)
                .Where(i => i.confirmacion_lider == false)
                .Where(i => i.eliminado == false)

                .OrderBy(i => i.programado_ts)
                .Take(100)
                .ToListAsync();

            return anomalia.Select(i => new RegistroAnomaliaViewModel
            {

                idregistroanomalia = i.idregistroanomalia,
                codigo = i.codigo,
                //emision_ts = i.emision_ts.ToString("dd/MM/yy HH:mm"),
                emision_ts = i.emision_ts.ToString("dd/MM/yy"),
                idusuario = i.idusuario,
                usuario = i.usuario.username,
                //avatar = i.usuario.avatar,
                paso_ma = i.paso_ma,
                criticidad = i.criticidad,
                turno = i.turno,
                idarea = i.idarea,
                area = i.area.nombre,
                idsector = i.idsector,
                sector = i.sector.nombre,
                idmaquina = i.idmaquina.Value,
                maquina = i.maquina.nombre,
                idanomalia = i.idanomalia,
                anomaliac = i.anomalia.nombre,
                idsuceso = i.idsuceso,
                relacionado = i.suceso.nombre,
                idtarjeta = i.idtarjeta,
                tarjeta = i.tarjeta.nombre,
                idproyecto = i.idproyecto.Value,
                proyecto = i.proyecto.nombre,
                descripcion = i.descripcion,
                programado_ts = i.programado_ts.ToString("dd/MM/yy HH:mm"),
                tiempo_vence = calDias.VmtoDias(i.programado_ts),
                alarma = calDias.Alarma(i.programado_ts),
                // valor true: ya no puede modificar la tarjeta , false: aun puede modificar la tarjeta
                edicion = calDias.TiempoLimiteModificarTarjeta(i.emision_ts),
                foto = i.foto_icon.foto_anomalia



            });


        }


        // GET: api/RegistrosAnomalias/ListarProgramacion   por id
        // [Authorize(Roles = "Almacenero,Administrador")]
        // [HttpGet("[action]/{id}")]
        //public async Task<IEnumerable<RegistroAnomaliaViewModel>> ListarProgramacion([FromRoute] int id)
        [HttpGet("[action]")]
        public async Task<IEnumerable<RegistroAnomaliaViewModel>> ListarProgramacion()
        {
            ValidacionIsNullOrEmpy validar = new ValidacionIsNullOrEmpy();
            //string estadoData2 = "RETRASADO";

#pragma warning disable EF1000 // Possible SQL .


            CriticidadActual calDias = new CriticidadActual();


            List<Registrosanomalias> anomalia = await _context.Registrosanomalias

               .Include(i => i.usuario)
               .Include(i => i.area)
               .Include(i => i.sector)
               .Include(i => i.maquina)
               .Include(i => i.anomalia)
               .Include(i => i.proyecto)
               .Include(i => i.suceso)
               .Include(i => i.tarjeta)
               .Include(i => i.foto_icon)
               //.Where(i => i.idtarjeta == id)
               .Where(i => i.eliminado == false)
               //.Where(i=>i.prog==false)

               .OrderByDescending(i => i.programado_ts)
               .Take(200)
               .ToListAsync();

            return anomalia.Select(i => new RegistroAnomaliaViewModel
            {
                idregistroanomalia = i.idregistroanomalia,
                codigo = i.codigo,
                emision_ts = i.emision_ts.ToString("dd/MM/yy HH:mm"),
                idusuario = i.idusuario,
                usuario = i.usuario.username,
                //avatar = i.usuario.avatar,
                paso_ma = i.paso_ma,
                criticidad = i.criticidad,
                turno = i.turno,
                idarea = i.idarea,
                area = i.area.nombre,
                idsector = i.idsector,
                sector = i.sector.nombre,
                idmaquina = i.idmaquina.Value,
                maquina = i.maquina.nombre,
                idanomalia = i.idanomalia,
                anomaliac = i.anomalia.nombre,
                idsuceso = i.idsuceso,
                relacionado = i.suceso.nombre,
                idtarjeta = i.idtarjeta,
                tarjeta = i.tarjeta.nombre,
                idproyecto = i.idproyecto.Value,
                proyecto = i.proyecto.nombre,
                descripcion = i.descripcion,
                programado_ts = i.programado_ts.ToString("dd/MM/yy HH:mm"),
                tiempo_vence = calDias.VmtoDias(i.programado_ts),
                alarma = calDias.Alarma(i.programado_ts),
                // valor true: ya no puede modificar la tarjeta , false: aun puede modificar la tarjeta
                edicion = calDias.TiempoLimiteModificarTarjeta(i.emision_ts),
                programado = i.prog,
                estadoTarjeta = validar.ControlEstado(i.confirmacion_tec, i.confirmacion_super, i.confirmacion_lider),
                foto = i.foto_icon.foto_anomalia

            });


        }

        // GET: api/RegistrosAnomalias/ListarTipoTarjetaRecibir   por id
        // [Authorize(Roles = "Almacenero,Administrador")]
        [HttpGet("[action]/{id}/{idSectorData}")]
        public async Task<IEnumerable<RegistroViewModelRecibir>> ListarTipoTarjetaRecibir([FromRoute] int id, string idSectorData)
        {
            ValidacionIsNullOrEmpy validar = new ValidacionIsNullOrEmpy();

            List<Registrosanomalias> anomalia = await _context.Registrosanomalias
                 .FromSql($"SELECT * FROM tb_registro_anomalia where " +
                          $"" + validar.ValidarDatoCadena(idSectorData, "idarea") + " IN (" + validar.ValidarDato(idSectorData) + ")")
                .Include(i => i.usuario)
                .Include(i => i.usuariotecnico)
                .Include(i => i.area)
                .Include(i => i.sector)
                .Include(i => i.maquina)
                .Include(i => i.anomalia)
                .Include(i => i.suceso)
                .Include(i => i.tarjeta)
                .Include(i => i.foto_icon)
                .Include(i => i.proyecto)
                .Where(i => i.idtarjeta == id)
                .Where(i => i.confirmacion_tec == true)
                .Where(i => i.confirmacion_super == false)
                .Where(i => i.confirmacion_lider == false)
                .Where(i => i.eliminado == false)

                .OrderByDescending(i => i.idregistroanomalia)
                .Take(100)
                .ToListAsync();

            return anomalia.Select(i => new RegistroViewModelRecibir
            {
                idregistroanomalia = i.idregistroanomalia,
                codigo = i.codigo,
                emision_ts = i.emision_ts.ToString("dd/MM/yyyy HH:mm"),
                idusuario = i.idusuario,
                usuario = i.usuario.username,
                tecnico = i.usuariotecnico.username,
                paso_ma = i.paso_ma,
                criticidad = i.criticidad,
                turno = i.turno,
                idarea = i.idarea,
                area = i.area.nombre,
                idsector = i.idsector,
                sector = i.sector.nombre,
                idmaquina = i.idmaquina.Value,
                maquina = i.maquina.nombre,
                idanomalia = i.idanomalia,
                anomaliac = i.anomalia.nombre,
                idsuceso = i.idsuceso,
                relacionado = i.suceso.nombre,
                idtarjeta = i.idtarjeta,
                tarjeta = i.tarjeta.nombre,
                idproyecto = i.idproyecto.Value,
                proyecto = i.proyecto.nombre,
                descripcion = i.descripcion,
                sol_implementada = i.sol_implementada,
                fecha_solucion = i.ejecucion_ts.ToString("dd/MM/yyyy HH:mm"),
                foto = i.foto_icon.foto_anomalia


            });


        }

        ValidacionIsNullOrEmpy validar = new ValidacionIsNullOrEmpy();

        // GET: api/RegistrosAnomalias/ListarTipoTarjetaRecibirSector   por id
        // [Authorize(Roles = "Almacenero,Administrador")]
        [HttpGet("[action]/{id}/{idSectorData}")]

        public async Task<IEnumerable<RegistroViewModelRecibir>> ListarTipoTarjetaRecibirSector([FromRoute] int id, string idSectorData)
        {


            List<Registrosanomalias> anomalia = await _context.Registrosanomalias

                .FromSql($"SELECT * FROM tb_registro_anomalia where " +
                          $"" + validar.ValidarDatoCadena(idSectorData, "idarea") + " IN (" + validar.ValidarDato(idSectorData) + ")")
                .Include(i => i.usuario)
                .Include(i => i.usuariotecnico)
                .Include(i => i.area)
                .Include(i => i.maquina)
                .Include(i => i.anomalia)
                .Include(i => i.suceso)
                .Include(i => i.tarjeta)
                .Include(i => i.proyecto)
                 .Include(i => i.foto_icon)
                .Where(i => i.idtarjeta == id)
                .Where(i => i.confirmacion_tec == true)
                .Where(i => i.confirmacion_super == false)
                .Where(i => i.confirmacion_lider == false)
                .Where(i => i.eliminado == false)

                .OrderByDescending(i => i.idregistroanomalia)
                .Take(100)
                .ToListAsync();

            return anomalia.Select(i => new RegistroViewModelRecibir
            {
                idregistroanomalia = i.idregistroanomalia,
                codigo = i.codigo,
                emision_ts = i.emision_ts.ToString("dd/MM/yyyy HH:mm"),
                idusuario = i.idusuario,
                usuario = i.usuario.username,
                tecnico = i.usuariotecnico.username,
                paso_ma = i.paso_ma,
                criticidad = i.criticidad,
                turno = i.turno,
                idarea = i.idarea,
                area = i.area.nombre,
                idmaquina = i.idmaquina.Value,
                maquina = i.maquina.nombre,
                idanomalia = i.idanomalia,
                anomaliac = i.anomalia.nombre,
                idsuceso = i.idsuceso,
                relacionado = i.suceso.nombre,
                idtarjeta = i.idtarjeta,
                tarjeta = i.tarjeta.nombre,
                idproyecto = i.idproyecto.Value,
                proyecto = i.proyecto.nombre,
                descripcion = i.descripcion,
                sol_implementada = i.sol_implementada,
                fecha_solucion = i.ejecucion_ts.ToString("dd/MM/yyyy HH:mm"),
                foto = i.foto_icon.foto_anomalia


            });


        }


        // GET: api/RegistrosAnomalias/ListarTipoTarjetaRecibirRoles   por id
        // [Authorize(Roles = "Almacenero,Administrador")]
        [HttpGet("[action]/{id}/{rol}/{idSectorData}")]
        public async Task<IEnumerable<RegistroViewModelRecibir>> ListarTipoTarjetaRecibirRoles([FromRoute] int id, string rol, string idSectorData)
        {
            if (rol == "TECNICO" || rol == "SUPERVISOR MANTENIMIENTO" || rol == "LIDER" || rol == "OPERADOR" || rol == "SUPERVISOR PRODUCCION")
            {

                List<Registrosanomalias> anomalia = await _context.Registrosanomalias
                       .FromSql($"SELECT * FROM tb_registro_anomalia where " +
                          $"" + validar.ValidarDatoCadena(idSectorData, "idarea") + " IN (" + validar.ValidarDato(idSectorData) + ")")
                .Include(i => i.usuario)
                .Include(i => i.usuariotecnico)
                .Include(i => i.area)
                .Include(i => i.sector)
                .Include(i => i.maquina)
                .Include(i => i.anomalia)
                .Include(i => i.suceso)
                .Include(i => i.tarjeta)
                .Include(i => i.proyecto)
                .Include(i => i.foto_icon)

                //.Where(i => i.foto_icon.tipo_foto=="Solucion")
                .Where(i => i.idtarjeta == id)
                .Where(i => i.confirmacion_tec == true)
                .Where(i => i.confirmacion_super == false)
                .Where(i => i.confirmacion_lider == false)
                .Where(i => i.eliminado == false)

                .OrderByDescending(i => i.idregistroanomalia)
                .Take(100)
                .ToListAsync();

                return anomalia.Select(i => new RegistroViewModelRecibir
                {
                    idregistroanomalia = i.idregistroanomalia,
                    codigo = i.codigo,
                    emision_ts = i.emision_ts.ToString("dd/MM/yyyy HH:mm"),
                    idusuario = i.idusuario,
                    usuario = i.usuario.username,
                    tecnico = i.usuariotecnico.username,
                    paso_ma = i.paso_ma,
                    criticidad = i.criticidad,
                    turno = i.turno,
                    idarea = i.idarea,
                    area = i.area.nombre,
                    idsector = i.idsector,
                    sector = i.sector.nombre,
                    idmaquina = i.idmaquina.Value,
                    maquina = i.maquina.nombre,
                    idanomalia = i.idanomalia,
                    anomaliac = i.anomalia.nombre,
                    idsuceso = i.idsuceso,
                    relacionado = i.suceso.nombre,
                    idtarjeta = i.idtarjeta,
                    tarjeta = i.tarjeta.nombre,
                    idproyecto = i.idproyecto.Value,
                    proyecto = i.proyecto.nombre,
                    descripcion = i.descripcion,
                    sol_implementada = i.sol_implementada,
                    fecha_solucion = i.ejecucion_ts.ToString("dd/MM/yyyy HH:mm"),
                    foto = i.foto_icon.foto_anomalia





                });
            }
            else
            {
                List<Registrosanomalias> anomalia = await _context.Registrosanomalias
                .Include(i => i.usuario)
                .Include(i => i.usuariotecnico)
                .Include(i => i.area)
                .Include(i => i.maquina)
                .Include(i => i.anomalia)
                .Include(i => i.suceso)
                .Include(i => i.tarjeta)
                 .Include(i => i.foto_icon)
                .Include(i => i.proyecto)

                //.Where(i => i.foto_icon.tipo_foto == "Solucion")
                .Where(i => i.idtarjeta == id)
                .Where(i => i.confirmacion_tec == true)
                .Where(i => i.confirmacion_super == false)
                .Where(i => i.confirmacion_lider == false)
                .Where(i => i.eliminado == false)

                .OrderByDescending(i => i.idregistroanomalia)
                .Take(100)
                .ToListAsync();

                return anomalia.Select(i => new RegistroViewModelRecibir
                {
                    idregistroanomalia = i.idregistroanomalia,
                    codigo = i.codigo,
                    emision_ts = i.emision_ts.ToString("dd/MM/yyyy HH:mm"),
                    idusuario = i.idusuario,
                    usuario = i.usuario.username,
                    tecnico = i.usuariotecnico.username,
                    paso_ma = i.paso_ma,
                    criticidad = i.criticidad,
                    turno = i.turno,
                    idarea = i.idarea,
                    area = i.area.nombre,
                    idmaquina = i.idmaquina.Value,
                    maquina = i.maquina.nombre,
                    idanomalia = i.idanomalia,
                    anomaliac = i.anomalia.nombre,
                    idsuceso = i.idsuceso,
                    relacionado = i.suceso.nombre,
                    idtarjeta = i.idtarjeta,
                    tarjeta = i.tarjeta.nombre,
                    idproyecto = i.idproyecto.Value,
                    proyecto = i.proyecto.nombre,
                    descripcion = i.descripcion,
                    sol_implementada = i.sol_implementada,
                    fecha_solucion = i.ejecucion_ts.ToString("dd/MM/yyyy HH:mm"),
                    foto = i.foto_icon.foto_anomalia



                });
            }
        }




        // GET: api/RegistrosAnomalias/ListarTipoTarjetaRecibirLiderSuperProduccion   por id
        // [Authorize(Roles = "Almacenero,Administrador")]
        [HttpGet("[action]/{id}/{rol}/{idSectorData}")]
        public async Task<IEnumerable<RegistroViewModelRecibir>> ListarTipoTarjetaRecibirLiderSuperProduccion([FromRoute] int id, string rol, string idSectorData)
        {
            if (rol == "OPERADOR LIDER" || rol == "LIDER" || rol == "SUPERVISOR PRODUCCION" || rol == "TECNICO" || rol == "SUPERVISOR MANTENIMIENTO")
            {

                List<Registrosanomalias> anomalia = await _context.Registrosanomalias
                    .FromSql($"SELECT * FROM tb_registro_anomalia where " +
                          $"" + validar.ValidarDatoCadena(idSectorData, "idarea") + " IN (" + validar.ValidarDato(idSectorData) + ")")
                    .Include(i => i.usuario)
                    .Include(i => i.usuariotecnico)
                     .Include(i => i.usuariosupervisor)
                    .Include(i => i.area)
                    .Include(i => i.maquina)
                    .Include(i => i.anomalia)
                    .Include(i => i.suceso)
                    .Include(i => i.tarjeta)
                    .Include(i => i.proyecto)
                     .Include(i => i.foto_icon)
                    .Where(i => i.idtarjeta == id)
                    .Where(i => i.confirmacion_tec == true)
                    .Where(i => i.confirmacion_super == false)
                    .Where(i => i.confirmacion_lider == false)
                    .Where(i => i.eliminado == false)

                    .OrderByDescending(i => i.idregistroanomalia)
                    .Take(100)
                    .ToListAsync();

                return anomalia.Select(i => new RegistroViewModelRecibir
                {
                    idregistroanomalia = i.idregistroanomalia,
                    codigo = i.codigo,
                    emision_ts = i.emision_ts.ToString("dd/MM/yyyy HH:mm"),
                    idusuario = i.idusuario,
                    usuario = i.usuario.username,
                    tecnico = i.usuariotecnico.username,
                    paso_ma = i.paso_ma,
                    criticidad = i.criticidad,
                    turno = i.turno,
                    idarea = i.idarea,
                    area = i.area.nombre,
                    idmaquina = i.idmaquina.Value,
                    maquina = i.maquina.nombre,
                    idanomalia = i.idanomalia,
                    anomaliac = i.anomalia.nombre,
                    idsuceso = i.idsuceso,
                    relacionado = i.suceso.nombre,
                    idtarjeta = i.idtarjeta,
                    tarjeta = i.tarjeta.nombre,
                    idproyecto = i.idproyecto.Value,
                    proyecto = i.proyecto.nombre,
                    descripcion = i.descripcion,
                    sol_implementada = i.sol_implementada,
                    fecha_solucion = i.ejecucion_ts.ToString("dd/MM/yyyy HH:mm"),
                    foto = i.foto_icon.foto_anomalia



                });
            }
            else
            {
                List<Registrosanomalias> anomalia = await _context.Registrosanomalias
                    .Include(i => i.usuario)
                    .Include(i => i.usuariotecnico)
                     .Include(i => i.usuariosupervisor)
                    .Include(i => i.area)
                    .Include(i => i.maquina)
                    .Include(i => i.anomalia)
                    .Include(i => i.suceso)
                    .Include(i => i.tarjeta)
                    .Include(i => i.proyecto)
                    .Include(i => i.foto_icon)
                    .Where(i => i.idtarjeta == id)
                    .Where(i => i.confirmacion_tec == true)
                    .Where(i => i.confirmacion_super == false)
                    .Where(i => i.confirmacion_lider == false)
                    .Where(i => i.eliminado == false)

                    .OrderByDescending(i => i.idregistroanomalia)
                    .Take(100)
                    .ToListAsync();

                return anomalia.Select(i => new RegistroViewModelRecibir
                {
                    idregistroanomalia = i.idregistroanomalia,
                    codigo = i.codigo,
                    emision_ts = i.emision_ts.ToString("dd/MM/yyyy HH:mm"),
                    idusuario = i.idusuario,
                    usuario = i.usuario.username,
                    tecnico = i.usuariotecnico.username,
                    paso_ma = i.paso_ma,
                    criticidad = i.criticidad,
                    turno = i.turno,
                    idarea = i.idarea,
                    area = i.area.nombre,
                    idmaquina = i.idmaquina.Value,
                    maquina = i.maquina.nombre,
                    idanomalia = i.idanomalia,
                    anomaliac = i.anomalia.nombre,
                    idsuceso = i.idsuceso,
                    relacionado = i.suceso.nombre,
                    idtarjeta = i.idtarjeta,
                    tarjeta = i.tarjeta.nombre,
                    idproyecto = i.idproyecto.Value,
                    proyecto = i.proyecto.nombre,
                    descripcion = i.descripcion,
                    sol_implementada = i.sol_implementada,
                    fecha_solucion = i.ejecucion_ts.ToString("dd/MM/yyyy HH:mm"),
                    foto = i.foto_icon.foto_anomalia

                });
            }
        }






        // GET: api/RegistrosAnomalias/ListarTipoTarjetaCerrar   por id
        // [Authorize(Roles = "Almacenero,Administrador")]
        [HttpGet("[action]/{id}/{rol}/{idSectorData}")]
        public async Task<IEnumerable<RegistroViewModelCerrar>> ListarTipoTarjetaCerrar([FromRoute] int id, string rol, string idSectorData)
        {
            if (rol == "OPERADOR LIDER" || rol == "SUPERVISOR PRODUCCION" || rol == "SUPERVISOR MANTENIMIENTO")
            {

                List<Registrosanomalias> anomalia = await _context.Registrosanomalias
                    .FromSql($"SELECT * FROM tb_registro_anomalia where " +
                          $"" + validar.ValidarDatoCadena(idSectorData, "idarea") + " IN (" + validar.ValidarDato(idSectorData) + ")")
                    .Include(i => i.usuario)
                    .Include(i => i.usuariotecnico)
                     .Include(i => i.usuariosupervisor)
                    .Include(i => i.area)
                    .Include(i => i.maquina)
                    .Include(i => i.anomalia)
                    .Include(i => i.suceso)
                    .Include(i => i.tarjeta)
                    .Include(i => i.proyecto)
                    .Include(i => i.foto_icon)

                    .Where(i => i.idtarjeta == id)
                    .Where(i => i.confirmacion_tec == true)
                    .Where(i => i.confirmacion_super == true)
                    .Where(i => i.confirmacion_lider == false)
                    .Where(i => i.eliminado == false)

                    .OrderByDescending(i => i.idregistroanomalia)
                    .Take(100)
                    .ToListAsync();

                return anomalia.Select(i => new RegistroViewModelCerrar
                {
                    idregistroanomalia = i.idregistroanomalia,
                    codigo = i.codigo,
                    emision_ts = i.emision_ts.ToString("dd/MM/yyyy HH:mm"),
                    idusuario = i.idusuario,
                    usuario = i.usuario.username,
                    tecnico = i.usuariotecnico.username,
                    paso_ma = i.paso_ma,
                    criticidad = i.criticidad,
                    turno = i.turno,
                    idarea = i.idarea,
                    area = i.area.nombre,
                    idmaquina = i.idmaquina.Value,
                    maquina = i.maquina.nombre,
                    idanomalia = i.idanomalia,
                    anomaliac = i.anomalia.nombre,
                    idsuceso = i.idsuceso,
                    relacionado = i.suceso.nombre,
                    idtarjeta = i.idtarjeta,
                    tarjeta = i.tarjeta.nombre,
                    idproyecto = i.idproyecto.Value,
                    proyecto = i.proyecto.nombre,
                    descripcion = i.descripcion,
                    sol_implementada = i.sol_implementada,
                    fecha_solucion = i.ejecucion_ts.ToString("dd/MM/yyyy HH:mm"),
                    fecha_recibido = i.recepcion_ts.ToString("dd/MM/yyyy HH:mm"),
                    supervisor = i.usuariosupervisor.username,
                    foto = i.foto_icon.foto_anomalia





                });
            }
            else
            {
                List<Registrosanomalias> anomalia = await _context.Registrosanomalias
                    .Include(i => i.usuario)
                    .Include(i => i.usuariotecnico)
                    .Include(i => i.usuariosupervisor)
                    .Include(i => i.area)
                    .Include(i => i.maquina)
                    .Include(i => i.anomalia)
                    .Include(i => i.suceso)
                    .Include(i => i.tarjeta)
                    .Include(i => i.proyecto)
                    .Include(i => i.foto_icon)

                    .Where(i => i.idtarjeta == id)
                    .Where(i => i.confirmacion_tec == true)
                    .Where(i => i.confirmacion_super == true)
                    .Where(i => i.confirmacion_lider == false)
                    .Where(i => i.eliminado == false)

                    .OrderByDescending(i => i.idregistroanomalia)
                    .Take(100)
                    .ToListAsync();

                return anomalia.Select(i => new RegistroViewModelCerrar
                {
                    idregistroanomalia = i.idregistroanomalia,
                    codigo = i.codigo,
                    emision_ts = i.emision_ts.ToString("dd/MM/yyyy HH:mm"),
                    idusuario = i.idusuario,
                    usuario = i.usuario.username,
                    tecnico = i.usuariotecnico.username,
                    paso_ma = i.paso_ma,
                    criticidad = i.criticidad,
                    turno = i.turno,
                    idarea = i.idarea,
                    area = i.area.nombre,
                    idmaquina = i.idmaquina.Value,
                    maquina = i.maquina.nombre,
                    idanomalia = i.idanomalia,
                    anomaliac = i.anomalia.nombre,
                    idsuceso = i.idsuceso,
                    relacionado = i.suceso.nombre,
                    idtarjeta = i.idtarjeta,
                    tarjeta = i.tarjeta.nombre,
                    idproyecto = i.idproyecto.Value,
                    proyecto = i.proyecto.nombre,
                    descripcion = i.descripcion,
                    sol_implementada = i.sol_implementada,
                    fecha_solucion = i.ejecucion_ts.ToString("dd/MM/yyyy HH:mm"),
                    fecha_recibido = i.recepcion_ts.ToString("dd/MM/yyyy HH:mm"),
                    supervisor = i.usuariosupervisor.username,
                    foto = i.foto_icon.foto_anomalia
                });
            }
        }


        // GET: api/RegistrosAnomalias/ListarFilter
        // [Authorize(Roles = "Almacenero,Administrador")]
        [HttpGet("[action]/{areaData}/{maquinaData}/{usuarioData}/{tarjetaData}/{programadoData}/{proyectoData}/{pageSize}/{codigodata}/{estadoData}/{estadoDataTarjeta}/{fechadataInicio}/{fechadataFin}/{Sinfecha}")]
        public async Task<IEnumerable<RegistroAnomaliaViewModel>> ListarFilter([FromRoute] string areaData, string maquinaData, string usuarioData, string tarjetaData, string estadoDataTarjeta, string programadoData, string proyectoData, int pageSize, string codigodata, string estadoData, string fechadataInicio, string fechadataFin, bool Sinfecha)
        {
            ValidacionIsNullOrEmpy validar = new ValidacionIsNullOrEmpy();
            //string estadoData2 = "RETRASADO";

#pragma warning disable EF1000 // Possible SQL .

            bool Atendido = validar.ControlEstadoTarjetaAtendido(estadoDataTarjeta);
            bool Recibido = validar.ControlEstadoTarjetaRecibido(estadoDataTarjeta);
            bool Cerrado = validar.ControlEstadoTarjetaCerrado(estadoDataTarjeta);
            string estado;

            if (Cerrado == true)
            {
                estado = "cerrado";
            }
            else if (Recibido == true)
            {
                estado = "recibido";
            }
            else if (Atendido == true)
            {
                estado = "atendido";
            }
            else
            {
                estado = "ingresado";

            }


            DateTime nuevaFecha = Convert.ToDateTime(fechadataFin);

            nuevaFecha = nuevaFecha.AddDays(1);

            CriticidadActual calDias = new CriticidadActual();

            if (Sinfecha == true)
            {
                List<Registrosanomalias> anomalia = await _context.Registrosanomalias

                          .FromSql($"SELECT * FROM tb_registro_anomalia where " +
                          $"" + validar.ValidarDatoCadena(areaData, "idarea") + " IN (" + validar.ValidarDato(areaData) + ") " + "and" + " " +
                          $"" + validar.ValidarDatoCadena(maquinaData, "idmaquina") + " IN (" + validar.ValidarDato(maquinaData) + ") " + "and" + " " +
                          $"" + validar.ValidarDatoCadena(usuarioData, "idusuario") + " IN (" + validar.ValidarDato(usuarioData) + ") " + "and" + " " +
                          $"" + validar.ValidarDatoCadena(programadoData, "prog") + " = (" + validar.ValidarDatoProgramado(programadoData) + ") " + "and" + " " +
                          $"" + validar.ValidarDatoCadena(proyectoData, "idproyecto") + " IN (" + validar.ValidarDato(proyectoData) + ") " + "and" + " " +
                          $"" + validar.ValidarDatoCadena(tarjetaData, "idtarjeta") + " IN (" + validar.ValidarDatoTarjeta(tarjetaData) + ") " +
                          "and " + validar.ValidarDatoCadena(estadoData, "GETDATE()") + " " + validar.ValidarDatoSigno(estadoData) + " " + validar.ValidarDatoCadena(estadoData, "programado_ts"))

#pragma warning restore EF1000 // Possible SQL .
                          .Include(i => i.usuario)
                .Include(i => i.area)
                .Include(i => i.sector)
                .Include(i => i.maquina)
                .Include(i => i.anomalia)
                .Include(i => i.proyecto)
                .Include(i => i.suceso)
                .Include(i => i.tarjeta)
                .Include(i => i.foto_icon)
                //.Where(i => i.idtarjeta == id)

                .Where(i => i.confirmacion_tec == Atendido)
                .Where(i => i.confirmacion_super == Recibido)
                .Where(i => i.confirmacion_lider == Cerrado)
                .Where(i => i.eliminado == false)
                //.Where(i=>i.prog==false)

                .Where(i => i.codigo.Contains(codigodata))

                .OrderBy(i => i.programado_ts)
                           .Take(pageSize)
                           .ToListAsync();

                return anomalia.Select(i => new RegistroAnomaliaViewModel
                {
                    idregistroanomalia = i.idregistroanomalia,
                    codigo = i.codigo,
                    emision_ts = i.emision_ts.ToString("dd/MM/yy HH:mm"),
                    idusuario = i.idusuario,
                    usuario = i.usuario.username,
                    //avatar = i.usuario.avatar,
                    paso_ma = i.paso_ma,
                    criticidad = i.criticidad,
                    turno = i.turno,
                    idarea = i.idarea,
                    area = i.area.nombre,
                    idsector = i.idsector,
                    sector = i.sector.nombre,
                    idmaquina = i.idmaquina.Value,
                    maquina = i.maquina.nombre,
                    idanomalia = i.idanomalia,
                    anomaliac = i.anomalia.nombre,
                    idsuceso = i.idsuceso,
                    relacionado = i.suceso.nombre,
                    idtarjeta = i.idtarjeta,
                    tarjeta = i.tarjeta.nombre,
                    idproyecto = i.idproyecto.Value,
                    proyecto = i.proyecto.nombre,
                    descripcion = i.descripcion,
                    programado_ts = i.programado_ts.ToString("dd/MM/yy HH:mm"),
                    tiempo_vence = calDias.VmtoDias(i.programado_ts),
                    alarma = calDias.Alarma(i.programado_ts),
                    // valor true: ya no puede modificar la tarjeta , false: aun puede modificar la tarjeta
                    edicion = calDias.TiempoLimiteModificarTarjeta(i.emision_ts),
                    programado = i.prog,
                    estadoTarjeta = estado,
                    foto = i.foto_icon.foto_anomalia

                });
            }
            else
            {
#pragma warning disable EF1000 // Possible SQL injection vulnerability.
                List<Registrosanomalias> anomalia = await _context.Registrosanomalias

                         .FromSql($"SELECT * FROM tb_registro_anomalia where " +
                         $"" + validar.ValidarDatoCadena(areaData, "idarea") + " IN (" + validar.ValidarDato(areaData) + ") " + "and" + " " +
                         $"" + validar.ValidarDatoCadena(maquinaData, "idmaquina") + " IN (" + validar.ValidarDato(maquinaData) + ") " + "and" + " " +
                         $"" + validar.ValidarDatoCadena(usuarioData, "idusuario") + " IN (" + validar.ValidarDato(usuarioData) + ") " + "and" + " " +
                         $"" + validar.ValidarDatoCadena(programadoData, "prog") + " = (" + validar.ValidarDatoProgramado(programadoData) + ") " + "and" + " " +
                         $"" + validar.ValidarDatoCadena(proyectoData, "idproyecto") + " IN (" + validar.ValidarDato(proyectoData) + ") " + "and" + " " +
                         $"" + validar.ValidarDatoCadena(tarjetaData, "idtarjeta") + " IN (" + validar.ValidarDatoTarjeta(tarjetaData) + ") " +
                         "and " + validar.ValidarDatoCadena(estadoData, "GETDATE()") + " " + validar.ValidarDatoSigno(estadoData) + " " + validar.ValidarDatoCadena(estadoData, "programado_ts") + "" +
                         " and emision_ts  BETWEEN '" + fechadataInicio + "' AND '" + nuevaFecha + "'  ")
#pragma warning restore EF1000 // Possible SQL injection vulnerability.

#pragma warning restore EF1000 // Possible SQL .
                          .Include(i => i.usuario)
               .Include(i => i.area)
               .Include(i => i.sector)
               .Include(i => i.maquina)
               .Include(i => i.anomalia)
               .Include(i => i.proyecto)
               .Include(i => i.suceso)
               .Include(i => i.tarjeta)
               .Include(i => i.foto_icon)
               //.Where(i => i.idtarjeta == id)

               .Where(i => i.confirmacion_tec == Atendido)
               .Where(i => i.confirmacion_super == Recibido)
               .Where(i => i.confirmacion_lider == Cerrado)
               .Where(i => i.eliminado == false)
               //.Where(i=>i.prog==false)

               .Where(i => i.codigo.Contains(codigodata))

               .OrderBy(i => i.programado_ts)
                          .Take(pageSize)
                          .ToListAsync();

                return anomalia.Select(i => new RegistroAnomaliaViewModel
                {
                    idregistroanomalia = i.idregistroanomalia,
                    codigo = i.codigo,
                    emision_ts = i.emision_ts.ToString("dd/MM/yy HH:mm"),
                    idusuario = i.idusuario,
                    usuario = i.usuario.username,
                    //avatar = i.usuario.avatar,
                    paso_ma = i.paso_ma,
                    criticidad = i.criticidad,
                    turno = i.turno,
                    idarea = i.idarea,
                    area = i.area.nombre,
                    idsector = i.idsector,
                    sector = i.sector.nombre,
                    idmaquina = i.idmaquina.Value,
                    maquina = i.maquina.nombre,
                    idanomalia = i.idanomalia,
                    anomaliac = i.anomalia.nombre,
                    idsuceso = i.idsuceso,
                    relacionado = i.suceso.nombre,
                    idtarjeta = i.idtarjeta,
                    tarjeta = i.tarjeta.nombre,
                    idproyecto = i.idproyecto.Value,
                    proyecto = i.proyecto.nombre,
                    descripcion = i.descripcion,
                    programado_ts = i.programado_ts.ToString("dd/MM/yy HH:mm"),
                    tiempo_vence = calDias.VmtoDias(i.programado_ts),
                    alarma = calDias.Alarma(i.programado_ts),
                    // valor true: ya no puede modificar la tarjeta , false: aun puede modificar la tarjeta
                    edicion = calDias.TiempoLimiteModificarTarjeta(i.emision_ts),
                    programado = i.prog,
                    estadoTarjeta = estado,
                    foto = i.foto_icon.foto_anomalia
                });
            }
        }


        // POST: api/RegistrosAnomalias/CrearAnomalias
        [HttpPost("[action]")]
        public async Task<IActionResult> CrearAnomalias([FromBody] CrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            DateTime fechaHora = DateTime.Now;
            //Calcula el turno actual T1,T2,T3
            TurnoActual turnoactual = new TurnoActual();
            CriticidadActual calDias = new CriticidadActual();



            Registrosanomalias anomalia = new Registrosanomalias
            {
                emision_ts = fechaHora,
                idusuario = model.idusuario,
                paso_ma = model.paso_ma,
                criticidad = model.criticidad,
                turno = turnoactual.TurnoActualSystema(),
                idarea = model.idarea,
                idsector = model.idsector,
                idmaquina = model.idmaquina,
                idanomalia = model.idanomalia,
                idsuceso = model.idsuceso,
                idtarjeta = model.idtarjeta,
                descripcion = model.descripcion,
                programado_ts = calDias.CalculoCriticidadDias(model.criticidad)

            };



            try
            {
                _context.Registrosanomalias.Add(anomalia);
                await _context.SaveChangesAsync();

                var id = anomalia.idregistroanomalia;
                foreach (var det in model.detalles)
                {
                    DetalleFotoAnomalia detalle = new DetalleFotoAnomalia
                    {
                        idregistroanomalia = id,
                        foto_anomalia = det.foto_anomalia,
                        tipo_foto = det.tipo_foto,
                        eliminado = det.eliminado
                    };
                    _context.DetallefotosShe.Add(detalle);
                }

                foreach (var det1 in model.detallesThumbail)
                {
                    DetalleFotoAnomaliaThumbnail detallesThumbail = new DetalleFotoAnomaliaThumbnail
                    {
                        idregistroanomalia = id,
                        foto_anomalia = det1.foto_anomalia,
                        tipo_foto = det1.tipo_foto,
                        eliminado = det1.eliminado
                    };
                    _context.DetallefotosThumbnail.Add(detallesThumbail);
                }
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            var idtarjetamsg = anomalia.codigo;
            return Ok(idtarjetamsg);
        }

        // POST: api/RegistrosAnomalias/CrearAnomaliasProyecto
        [HttpPost("[action]")]
        public async Task<IActionResult> CrearAnomaliasProyecto([FromBody] CrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            DateTime fechaHora = DateTime.Now;
            //Calcula el turno actual T1,T2,T3
            TurnoActual turnoactual = new TurnoActual();
            CriticidadActual calDias = new CriticidadActual();


            Registrosanomalias anomalia = new Registrosanomalias
            {
                emision_ts = fechaHora,
                idusuario = model.idusuario,
                paso_ma = model.paso_ma,
                criticidad = model.criticidad,
                turno = turnoactual.TurnoActualSystema(),
                idarea = model.idarea,
                idmaquina = 14,
                idanomalia = model.idanomalia,
                idsuceso = model.idsuceso,
                idtarjeta = model.idtarjeta,
                descripcion = model.descripcion,
                idproyecto = model.idproyecto,
                programado_ts = calDias.CalculoCriticidadDias(model.criticidad)

            };

            try
            {
                _context.Registrosanomalias.Add(anomalia);
                await _context.SaveChangesAsync();

                var id = anomalia.idregistroanomalia;
                foreach (var det in model.detalles)
                {
                    DetalleFotoAnomalia detalle = new DetalleFotoAnomalia
                    {
                        idregistroanomalia = id,
                        foto_anomalia = det.foto_anomalia,
                        tipo_foto = det.tipo_foto,
                        eliminado = det.eliminado
                    };
                    _context.DetallefotosShe.Add(detalle);
                }

                foreach (var det1 in model.detallesThumbail)
                {
                    DetalleFotoAnomaliaThumbnail detallesThumbail = new DetalleFotoAnomaliaThumbnail
                    {
                        idregistroanomalia = id,
                        foto_anomalia = det1.foto_anomalia,
                        tipo_foto = det1.tipo_foto,
                        eliminado = det1.eliminado
                    };
                    _context.DetallefotosThumbnail.Add(detallesThumbail);
                }
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            var idtarjetamsg = anomalia.codigo;
            return Ok(idtarjetamsg);
        }

        // PUT: api/RegistrosAnomalias/AplicarContramedida
        [HttpPut("[action]")]
        public async Task<IActionResult> AplicarContramedida([FromBody] ActualizarViewModelContramedida model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idregistroanomalia <= 0)
            {
                return BadRequest();
            }

            Registrosanomalias anomalia = await _context.Registrosanomalias.FirstOrDefaultAsync(c => c.idregistroanomalia == model.idregistroanomalia);

            if (anomalia == null)
            {
                return NotFound();
            }
            DateTime fechaHora = DateTime.Now;

            anomalia.sol_implementada = model.sol_implementada;
            anomalia.ejecucion_ts = fechaHora;
            anomalia.idtecnico = model.idtecnico;
            anomalia.confirmacion_tec = true;
            //anomalia.foto_solucion = model.foto_solucion;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok(anomalia.codigo);
        }

        // PUT: api/RegistrosAnomalias/ConfirmarRecepcion
        [HttpPut("[action]")]
        public async Task<IActionResult> ConfirmarRecepcion([FromBody] ActualizarViewModelConfirmacionTarjeta model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idregistroanomalia <= 0)
            {
                return BadRequest();
            }

            Registrosanomalias anomalia = await _context.Registrosanomalias.FirstOrDefaultAsync(c => c.idregistroanomalia == model.idregistroanomalia);

            if (anomalia == null)
            {
                return NotFound();
            }
            DateTime fechaHora = DateTime.Now;

            anomalia.confirmacion_super = true;
            anomalia.idsupervisor = model.idsupervisor;
            anomalia.recepcion_ts = fechaHora;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok(anomalia.codigo);
        }
        // PUT: api/RegistrosAnomalias/ConfirmarCerrar
        [HttpPut("[action]")]
        public async Task<IActionResult> ConfirmarCerrar([FromBody] ActualizarViewModelCerrar model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idregistroanomalia <= 0)
            {
                return BadRequest();
            }

            Registrosanomalias anomalia = await _context.Registrosanomalias.FirstOrDefaultAsync(c => c.idregistroanomalia == model.idregistroanomalia);

            if (anomalia == null)
            {
                return NotFound();
            }
            DateTime fechaHora = DateTime.Now;

            anomalia.confirmacion_lider = true;
            anomalia.idlider = model.idlider;
            anomalia.cierre_ts = fechaHora;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok(anomalia.codigo);
        }

        // PUT: api/RegistrosAnomalias/ProgramacionTarjeta
        [HttpPut("[action]")]
        public async Task<IActionResult> ProgramacionTarjeta([FromBody] ActualizarViewModelCerrar model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idregistroanomalia <= 0)
            {
                return BadRequest();
            }

            Registrosanomalias anomalia = await _context.Registrosanomalias.FirstOrDefaultAsync(c => c.idregistroanomalia == model.idregistroanomalia);

            if (anomalia == null)
            {
                return NotFound();
            }
            DateTime fechaHora = DateTime.Now;

            anomalia.confirmacion_lider = true;
            anomalia.idlider = model.idlider;
            anomalia.cierre_ts = fechaHora;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }



        // PUT: api/RegistrosAnomalias/Actualizar
        [HttpPut("[action]")]
        public async Task<IActionResult> Actualizar([FromBody] ActualizarViewModelDatos model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idregistroanomalia <= 0)
            {
                return BadRequest();
            }

            var anomalia = await _context.Registrosanomalias.FirstOrDefaultAsync(c => c.idregistroanomalia == model.idregistroanomalia);
            CriticidadActual calDias = new CriticidadActual();

            if (anomalia == null)
            {
                return NotFound();
            }

            anomalia.paso_ma = model.paso_ma;
            anomalia.criticidad = model.criticidad;
            anomalia.idarea = model.idarea;
            anomalia.idsector = model.idsector;
            anomalia.idmaquina = model.idmaquina;
            anomalia.idanomalia = model.idanomalia;
            anomalia.idsuceso = model.idsuceso;
            anomalia.descripcion = model.descripcion;
            anomalia.programado_ts = calDias.CalculoCriticidadDias(model.criticidad);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }

        // PUT: api/RegistrosAnomalias/ActualizarAmarillo
        [HttpPut("[action]")]
        public async Task<IActionResult> ActualizarAmarillo([FromBody] ActualizarViewModelAmarillo model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idregistroanomalia <= 0)
            {
                return BadRequest();
            }

            var anomalia = await _context.Registrosanomalias.FirstOrDefaultAsync(c => c.idregistroanomalia == model.idregistroanomalia);

            if (anomalia == null)
            {
                return NotFound();
            }

            anomalia.paso_ma = model.paso_ma;
            anomalia.criticidad = model.criticidad;
            anomalia.idarea = model.idarea;
            anomalia.idmaquina = model.idmaquina;
            anomalia.idanomalia = model.idanomalia;
            anomalia.idsuceso = model.idsuceso;
            anomalia.descripcion = model.descripcion;
            anomalia.idproyecto = model.idproyecto;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }

        // PUT: api/RegistrosAnomalias/Desactivar/id
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Desactivar([FromRoute] int id)
        {

            if (id <= 0)
            {
                return BadRequest();
            }

            var maquina = await _context.Registrosanomalias.FirstOrDefaultAsync(c => c.idregistroanomalia == id);

            if (maquina == null)
            {
                return NotFound();
            }

            maquina.prog = false;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }

        // PUT: api/RegistrosAnomalias/Activar/id
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Activar([FromRoute] int id)
        {

            if (id <= 0)
            {
                return BadRequest();
            }

            var maquina = await _context.Registrosanomalias.FirstOrDefaultAsync(c => c.idregistroanomalia == id);

            if (maquina == null)
            {
                return NotFound();
            }

            maquina.prog = true;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }

        // PUT: api/RegistrosAnomalias/RevertirRecepcion/id
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> RevertirRecepcion([FromRoute] int id)
        {

            if (id <= 0)
            {
                return BadRequest();
            }

            var registro = await _context.Registrosanomalias.FirstOrDefaultAsync(c => c.idregistroanomalia == id);

            if (registro == null)
            {
                return NotFound();
            }

            registro.idsupervisor = null;
            registro.confirmacion_super = false;


            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }



        private bool RegistroAnomaliaExists(int id)
        {
            return _context.Registrosanomalias.Any(e => e.idregistroanomalia == id);
        }
    }
}
