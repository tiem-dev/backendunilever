﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sistema.Datos;
using Sistema.Entidades.Wcm._1_N;
using Sistema.Web.Models.Wcm._1_N.Sector;

namespace Sistema.Web.Controllers.Wcm._1_N
{
    [Route("api/[controller]")]
    [ApiController]
    public class SectoresController : ControllerBase
    {
        private readonly DbContextSistema _context;

        public SectoresController(DbContextSistema context)
        {
            _context = context;
        }



        // GET:  api/Sectores/SaltoFlujoNivel/id
        // [Authorize(Roles = "Almacenero,Administrador")]
        [HttpGet("[action]/{id}")]
        public async Task<IEnumerable<SelectViewModel>> SaltoFlujoNivel([FromRoute] int id)


        {
            var TotalSubSector = await _context.Sector.Where(r => r.idarea == id).ToListAsync();

            var cont = TotalSubSector.Count();

            if (cont > 1)
            {
                var subsector = await _context.Sector
                    .Include(u => u.area)
                    .Where(a => a.idarea == id)
                    .Where(a => a.activo == true)
                    .Where(a => a.eliminado == false)
                    .ToListAsync();


                return subsector.Select(a => new SelectViewModel
                {
                    idsector = a.idsector,
                    idarea = a.idarea,
                    area = a.area.nombre,
                    nombre = a.nombre

                });
            }
            else
            {
                var proceso = await _context.Sector
                    .Include(u => u.area)
                    .Where(a => a.idarea == id)
                    .Where(a => a.activo == true)
                     .Where(a => a.eliminado == false)
                    .ToListAsync();


                return proceso.Select(a => new SelectViewModel
                {

                    idsector = a.idsector,
                    nombre = a.nombre

                });




            }
        }



        // GET: api/Sectores/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<SectorViewModel>> Listar()
        {
            var maquina = await _context.Sector
                .Where(a=>a.eliminado==false)
                .Include(a => a.area).ToListAsync();

            return maquina.Select(a => new SectorViewModel
            {
                idsector = a.idsector,
                idarea = a.idarea,
                area = a.area.nombre,
                nombre = a.nombre,
                descripcion = a.descripcion,
                activo = a.activo
            });
        }

        // GET: api/Sectores/ListarMaquinas
        [HttpGet("[action]")]
        public async Task<IEnumerable<SelectViewModel>> ListarSectores()

        {
            var maquina = await _context.Sector.Include(a => a.area)
                //.Where(a => a.activo == true)
                .Where(a => a.eliminado == false)

                .ToListAsync();

            return maquina.Select(a => new SelectViewModel
            {
                idsector = a.idsector
,
                nombre = a.nombre,
                idarea = a.idarea,
                area = a.area.nombre,

            });
        }


        // GET: api/Sectores/ListarAreas
        [HttpGet("[action]")]
        public async Task<IEnumerable<SelectViewModel>> ListarAreas()
        {
            var maquina = await _context.Sector
                .Include(u => u.area)
                .Where(a => a.activo == true)
                .ToListAsync();


            return maquina.Select(a => new SelectViewModel
            {
                idsector = a.idsector,
                area = a.area.nombre,
                idarea=a.idarea,
                nombre = a.nombre

            });


        }
        // GET: api/Sectores/ListarAreasid
        [HttpGet("[action]/{id}")]
        public async Task<IEnumerable<SelectViewModel>> ListarAreasid([FromRoute] int id)
        {
            var maquina = await _context.Sector
                .Include(u => u.area)
                .Where(a => a.idarea == id)
                .Where(a => a.activo == true)
                .ToListAsync();


            return maquina.Select(a => new SelectViewModel
            {
                idsector = a.idsector,
                idarea=a.idarea,
                area = a.area.nombre,
                nombre = a.nombre

            });


        }

        // POST: api/Sectores/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] CrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Sector sector = new Sector
            {
                idarea = model.idarea,
                nombre = model.nombre,
                descripcion = model.descripcion,
                activo = true
            };

            _context.Sector.Add(sector);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

            return Ok();
        }
        // PUT: api/Sectores/Actualizar
        [HttpPut("[action]")]
        public async Task<IActionResult> Actualizar([FromBody] ActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idsector <= 0)
            {
                return BadRequest();
            }

            var maquina = await _context.Sector.FirstOrDefaultAsync(c => c.idsector == model.idsector);

            if (maquina == null)
            {
                return NotFound();
            }

            maquina.idarea = model.idarea;
            maquina.nombre = model.nombre;
            maquina.descripcion = model.descripcion;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }


        // PUT: api/Sectores/Desactivar/1
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Desactivar([FromRoute] int id)
        {

            if (id <= 0)
            {
                return BadRequest();
            }

            var sector = await _context.Sector.FirstOrDefaultAsync(c => c.idsector == id);

            if (sector == null)
            {
                return NotFound();
            }

            sector.activo = false;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }

        // PUT: api/Sectores/Activar/1
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Activar([FromRoute] int id)
        {

            if (id <= 0)
            {
                return BadRequest();
            }

            var sector = await _context.Sector.FirstOrDefaultAsync(c => c.idsector == id);

            if (sector == null)
            {
                return NotFound();
            }

            sector.activo = true;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }

        private bool SectorExists(int id)
            {
                return _context.Sector.Any(e => e.idsector == id);
            }
        }
    }

