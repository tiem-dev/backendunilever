﻿//MODIFICADO 12/08/2019 ff
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Sistema.Web.Controllers.Wcm._1_N
{
    [Serializable]
    internal class HttpResponseException : Exception
    {
        private HttpResponseMessage<List<string>> responseMessage;

        public HttpResponseException()
        {
        }

        public HttpResponseException(HttpResponseMessage<List<string>> responseMessage)
        {
            this.responseMessage = responseMessage;
        }

        public HttpResponseException(string message) : base(message)
        {
        }

        public HttpResponseException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected HttpResponseException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}