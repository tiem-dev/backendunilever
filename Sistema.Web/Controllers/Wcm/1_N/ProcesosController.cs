﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sistema.Datos;
using Sistema.Entidades.Wcm._1_N;
using Sistema.Web.Models.Wcm._1_N.Proceso;

namespace Sistema.Web.Controllers.Wcm._1_N
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProcesosController : ControllerBase
    {
        private readonly DbContextSistema _context;

        public ProcesosController(DbContextSistema context)
        {
            _context = context;
        }


        // GET: api/Procesos/Listarid
        [HttpGet("[action]/{id}")]
        public async Task<IEnumerable<SelectViewModel>> Listarid([FromRoute] int id)
        {
            var proceso = await _context.Proceso
                .Include(u => u.planta)
                .Where(a => a.idplanta == id)
                .Where(a => a.activo == true)
                .ToListAsync();


            return proceso.Select(a => new SelectViewModel
            {

                idproceso = a.idproceso,
                idplanta = a.idplanta,
                planta = a.planta.nombre,
                nombre = a.nombre

            });

        }


        // GET: api/Procesos/SaltoFlujoNivel/id
        // [Authorize(Roles = "Almacenero,Administrador")]
        [HttpGet("[action]/{id}")]
        public async Task<IEnumerable<SelectViewModel>> SaltoFlujoNivel([FromRoute] int id)


        {
            var TotalProcesos = await _context.Proceso.Where(r => r.idplanta == id).ToListAsync();

            var cont = TotalProcesos.Count();

            if (cont > 1)
            {
                var proceso = await _context.Proceso
                    .Include(u => u.planta)
                    .Where(a => a.idplanta == id)
                    .Where(a => a.activo == true)
                    .Where(a => a.eliminado == false)
                    .ToListAsync();


                return proceso.Select(a => new SelectViewModel
                {

                    idproceso = a.idproceso,
                    idplanta = a.idplanta,
                    planta = a.planta.nombre,
                    nombre = a.nombre

                });
            }
            else
            {
                var proceso = await _context.Proceso
                    .Include(u => u.planta)
                    .Where(a => a.idplanta == id)
                    .Where(a => a.activo == true)
                    .ToListAsync();


                return proceso.Select(a => new SelectViewModel
                {

                   idproceso=a.idproceso,
                   planta=a.planta.nombre,
                   nombre=a.nombre
                   

                });




            }
        }


        private bool ProcesoExists(int id)
        {
            return _context.Proceso.Any(e => e.idproceso == id);
        }
    }
}
