﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sistema.Datos;
using Sistema.Entidades.Wcm._1_N;
using Sistema.Web.Models.Wcm._1_N.Componentes;

namespace Sistema.Web.Controllers.Wcm._1_N
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComponentesController : ControllerBase
    {
        private readonly DbContextSistema _context;

        public ComponentesController(DbContextSistema context)
        {
            _context = context;
        }


        // GET: api/Componentes/Listarid
        [HttpGet("[action]/{id}")]
        public async Task<IEnumerable<SelectViewModel>> Listarid([FromRoute] int id)
        {
            var proceso = await _context.Componente
                .Include(u => u.maquina)
                .Where(a => a.idmaquina == id)
                .Where(a => a.activo == true)
                .ToListAsync();


            return proceso.Select(a => new SelectViewModel
            {

                idcomponente = a.idcomponente,
                idmaquina = a.idmaquina,
                maquina = a.maquina.nombre,
                nombre = a.nombre

            });

        }

        // GET: api/Componentes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Componente>>> GetComponente()
        {
            return await _context.Componente.ToListAsync();
        }

        // GET: api/Componentes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Componente>> GetComponente(int id)
        {
            var componente = await _context.Componente.FindAsync(id);

            if (componente == null)
            {
                return NotFound();
            }

            return componente;
        }

        // PUT: api/Componentes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutComponente(int id, Componente componente)
        {
            if (id != componente.idcomponente)
            {
                return BadRequest();
            }

            _context.Entry(componente).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ComponenteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Componentes
        [HttpPost]
        public async Task<ActionResult<Componente>> PostComponente(Componente componente)
        {
            _context.Componente.Add(componente);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetComponente", new { id = componente.idcomponente }, componente);
        }

        // DELETE: api/Componentes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Componente>> DeleteComponente(int id)
        {
            var componente = await _context.Componente.FindAsync(id);
            if (componente == null)
            {
                return NotFound();
            }

            _context.Componente.Remove(componente);
            await _context.SaveChangesAsync();

            return componente;
        }

        private bool ComponenteExists(int id)
        {
            return _context.Componente.Any(e => e.idcomponente == id);
        }
    }
}
