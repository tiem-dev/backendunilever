﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sistema.Datos;
using Sistema.Entidades.Wcm._1_N;
using Sistema.Web.Controllers.CalculosAuxliares;
using Sistema.Web.Models.Wcm._1_N.RegistroShe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Controllers.Wcm._1_N
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegistroShesController : ControllerBase
    {
        private readonly DbContextSistema _context;

        public RegistroShesController(DbContextSistema context)
        {
            _context = context;
        }



        // GET: api/RegistroShes/Listar
        // [Authorize(Roles = "Almacenero,Administrador")]
        [HttpGet("[action]")]
        public async Task<IEnumerable<RegistroSheViewModel>> Listar()
        {
            CriticidadActual calDias = new CriticidadActual();
            List<RegistroShe> she = await _context.Registroshe

                .Include(i => i.usuario)
                .Include(i => i.area)
                .Include(i => i.sector)
                .Include(i => i.maquina)
                .Include(i => i.condicionInsegura)
                .Include(i => i.Falla)
                .Include(i => i.foto_icon)

                .Where(i => i.confirmacion_tec == false)
                .Where(i => i.confirmacion_super == false)
                .Where(i => i.confirmacion_lider == false)
                .Where(i => i.eliminado == false)
                .OrderBy(i => i.programado_ts)
                .Take(300)
                .ToListAsync();

            return she.Select(i => new RegistroSheViewModel
            {
                idregistroshe = i.idshe,
                //idproveedor = i.idproveedor,
                //proveedor = i.persona.nombre,
                codigo = i.codigo,
                emision_ts = i.emision_ts.ToString("dd/MM/yyyy"),
                idusuario = i.idusuario,
                usuario = i.usuario.username,
                prioridad = i.prioridad,
                turno = i.turno,
                hora_reporte = i.emision_ts.ToString("hh:mm:ss tt"),
                idarea = i.idarea,
                area = i.area.nombre,
                idsector = i.sector.idsector,
                sector = i.sector.nombre,
                idmaquina = i.idmaquina,
                maquina = i.maquina.nombre,
                she_ma = i.she_ma,
                paro_equipo = i.paro_equipo,
                idfalla = i.idfalla,
                falla = i.Falla.nombre,
                idcondicion = i.idcondicion,
                condicioinsegura = i.condicionInsegura.nombre,
                descripcion = i.descripcion,
                ope_mtto = i.ope_mtto,
                programado_ts = i.programado_ts.ToString("dd/MM/yy HH:mm"),
                tiempo_vence = calDias.VmtoDias(i.programado_ts),
                alarma = calDias.Alarma(i.programado_ts),
                // valor true: ya no puede modificar la tarjeta , false: aun puede modificar la tarjeta
                edicion = calDias.TiempoLimiteModificarTarjeta(i.emision_ts),
                foto=i.foto_icon.foto_anomalia



            });
        }

        // GET: api/RegistroShes/ListarVerdeRojaAtencion
        // [Authorize(Roles = "Almacenero,Administrador")]
        [HttpGet("[action]")]
        public async Task<IEnumerable<RegistroSheViewModel>> ListarVerdeRojaAtencion()
        {
            CriticidadActual calDias = new CriticidadActual();
            List<RegistroShe> she = await _context.Registroshe

                .Include(i => i.usuario)
                .Include(i => i.area)
                .Include(i => i.sector)
                .Include(i => i.maquina)
                .Include(i => i.condicionInsegura)
                .Include(i => i.Falla)
                .Include(i => i.foto_icon)


                .Where(i => i.ope_mtto == "mtto")
                .Where(i => i.confirmacion_tec == false)
                .Where(i => i.confirmacion_super == false)
                .Where(i => i.confirmacion_lider == false)
                .Where(i => i.eliminado == false)
                .OrderBy(i => i.programado_ts)
                .Take(300)
                .ToListAsync();

            return she.Select(i => new RegistroSheViewModel
            {
                idregistroshe = i.idshe,
                //idproveedor = i.idproveedor,
                //proveedor = i.persona.nombre,
                codigo = i.codigo,
                emision_ts = i.emision_ts.ToString("dd/MM/yyyy"),
                idusuario = i.idusuario,
                usuario = i.usuario.username,
                prioridad = i.prioridad,
                turno = i.turno,
                hora_reporte = i.emision_ts.ToString("hh:mm:ss tt"),
                idarea = i.idarea,
                area = i.area.nombre,
                idsector = i.sector.idsector,
                sector = i.sector.nombre,
                idmaquina = i.idmaquina,
                maquina = i.maquina.nombre,
                she_ma = i.she_ma,
                paro_equipo = i.paro_equipo,
                idfalla = i.idfalla,
                falla = i.Falla.nombre,
                idcondicion = i.idcondicion,
                condicioinsegura = i.condicionInsegura.nombre,
                descripcion = i.descripcion,
                ope_mtto = i.ope_mtto,
                programado_ts = i.programado_ts.ToString("dd/MM/yy HH:mm"),
                tiempo_vence = calDias.VmtoDias(i.programado_ts),
                alarma = calDias.Alarma(i.programado_ts),
                // valor true: ya no puede modificar la tarjeta , false: aun puede modificar la tarjeta
                edicion = calDias.TiempoLimiteModificarTarjeta(i.emision_ts),
                foto=i.foto_icon.foto_anomalia
                 



            });
        }

        // GET: api/RegistroShes/ListarFilter
        // [Authorize(Roles = "Almacenero,Administrador")]
        [HttpGet("[action]/{areaData}/{maquinaData}/{usuarioData}/{programadoData}/{pageSize}/{codigodata}/{estadoData}/{opmttoData}/{estadoDataTarjeta}/{fechadataInicio}/{fechadataFin}/{Sinfecha}")]
        public async Task<IEnumerable<RegistroSheViewModel>> ListarFilter([FromRoute] string areaData, string maquinaData, string usuarioData, string estadoDataTarjeta, string programadoData, int pageSize, string codigodata, string estadoData,string opmttoData, string fechadataInicio, string fechadataFin, bool Sinfecha)
        {
            ValidacionIsNullOrEmpy validar = new ValidacionIsNullOrEmpy();
            //string estadoData2 = "RETRASADO";

#pragma warning disable EF1000 // Possible SQL .

            bool Atendido = validar.ControlEstadoTarjetaAtendido(estadoDataTarjeta);
            bool Recibido = validar.ControlEstadoTarjetaRecibido(estadoDataTarjeta);
            bool Cerrado = validar.ControlEstadoTarjetaCerrado(estadoDataTarjeta);
            string estado;

            if (Cerrado == true)
            {
                estado = "cerrado";
            }
            else if (Recibido == true)
            {
                estado = "recibido";
            }
            else if (Atendido == true)
            {
                estado = "atendido";
            }
            else
            {
                estado = "ingresado";

            }
            DateTime nuevaFecha = Convert.ToDateTime(fechadataFin);

            nuevaFecha = nuevaFecha.AddDays(1);

            CriticidadActual calDias = new CriticidadActual();

            if (Sinfecha == true)
            {
                List<RegistroShe> anomalia = await _context.Registroshe

                          .FromSql($"SELECT * FROM tb_registro_she where " +
                          $"" + validar.ValidarDatoCadena(areaData, "idarea") + " IN (" + validar.ValidarDato(areaData) + ") " + "and" + " " +
                          $"" + validar.ValidarDatoCadena(maquinaData, "idmaquina") + " IN (" + validar.ValidarDato(maquinaData) + ") " + "and" + " " +
                          $"" + validar.ValidarDatoCadena(usuarioData, "idusuario") + " IN (" + validar.ValidarDato(usuarioData) + ") " + "and" + " " +
                          $"" + validar.ValidarDatoCadena(programadoData, "prog") + " = (" + validar.ValidarDatoProgramado(programadoData) + ") " + "and" + " " +
                          $"" + validar.ValidarDatoCadena(opmttoData, "ope_mtto") + " = (" +"'"+ validar.ValidarDato(opmttoData) +"'"+") " + "and" + " " +
                          $"" + validar.ValidarDatoCadena(estadoData, "GETDATE()") + " " + validar.ValidarDatoSigno(estadoData) + " " + validar.ValidarDatoCadena(estadoData, "programado_ts"))

#pragma warning restore EF1000 // Possible SQL .
                    .Include(i => i.usuario)
                .Include(i => i.area)
                .Include(i => i.sector)
                .Include(i => i.maquina)
                .Include(i => i.condicionInsegura)
                .Include(i => i.Falla)

                .Include(i => i.foto_icon)
                //.Where(i => i.ope_mtto == "mtto")

                .Where(i => i.confirmacion_tec == Atendido)
                .Where(i => i.confirmacion_super == Recibido)
                .Where(i => i.confirmacion_lider == Cerrado)
                .Where(i => i.eliminado == false)
                //.Where(i=>i.prog==false)

                .Where(i => i.codigo.Contains(codigodata))

                .OrderBy(i => i.programado_ts)
                           .Take(pageSize)
                           .ToListAsync();

            return anomalia.Select(i => new RegistroSheViewModel
            {
                idregistroshe = i.idshe,
                codigo = i.codigo,
                emision_ts = i.emision_ts.ToString("dd/MM/yyyy"),
                idusuario = i.idusuario,
                usuario = i.usuario.username,
                prioridad = i.prioridad,
                turno = i.turno,
                hora_reporte = i.emision_ts.ToString("hh:mm:ss tt"),
                idarea = i.idarea,
                area = i.area.nombre,
                idsector = i.sector.idsector,
                sector = i.sector.nombre,
                idmaquina = i.idmaquina,
                maquina = i.maquina.nombre,
                she_ma = i.she_ma,
                paro_equipo = i.paro_equipo,
                idfalla = i.idfalla,
                falla = i.Falla.nombre,
                idcondicion = i.idcondicion,
                condicioinsegura = i.condicionInsegura.nombre,
                ope_mtto = i.ope_mtto,
                descripcion = i.descripcion,
                programado_ts = i.programado_ts.ToString("dd/MM/yy HH:mm"),
                tiempo_vence = calDias.VmtoDias(i.programado_ts),
                alarma = calDias.Alarma(i.programado_ts),
                // valor true: ya no puede modificar la tarjeta , false: aun puede modificar la tarjeta
                edicion = calDias.TiempoLimiteModificarTarjeta(i.emision_ts),
                estadoTarjeta = estado,
                prog = i.prog,
                foto = i.foto_icon.foto_anomalia





            });
            }
            else
            {
#pragma warning disable EF1000 // Possible SQL injection vulnerability.
                List<RegistroShe> anomalia = await _context.Registroshe

                         .FromSql($"SELECT * FROM tb_registro_she where " +
                         $"" + validar.ValidarDatoCadena(areaData, "idarea") + " IN (" + validar.ValidarDato(areaData) + ") " + "and" + " " +
                         $"" + validar.ValidarDatoCadena(maquinaData, "idmaquina") + " IN (" + validar.ValidarDato(maquinaData) + ") " + "and" + " " +
                         $"" + validar.ValidarDatoCadena(usuarioData, "idusuario") + " IN (" + validar.ValidarDato(usuarioData) + ") " + "and" + " " +
                         $"" + validar.ValidarDatoCadena(programadoData, "prog") + " = (" + validar.ValidarDatoProgramado(programadoData) + ") " + "and" + " " +
                         $"" + validar.ValidarDatoCadena(opmttoData, "ope_mtto") + " = (" + "'" + validar.ValidarDato(opmttoData) + "'" + ") " + "and" + " " +
                         $"" + validar.ValidarDatoCadena(estadoData, "GETDATE()") + " " + validar.ValidarDatoSigno(estadoData) + " " + validar.ValidarDatoCadena(estadoData, "programado_ts") + "" +
                         " and emision_ts  BETWEEN '" + fechadataInicio + "' AND '" + nuevaFecha + "' ")

#pragma warning restore EF1000 // Possible SQL .
                    .Include(i => i.usuario)
               .Include(i => i.area)
               .Include(i => i.sector)
               .Include(i => i.maquina)
               .Include(i => i.condicionInsegura)
               .Include(i => i.Falla)

               .Include(i => i.foto_icon)
               //.Where(i => i.ope_mtto == "mtto")

               .Where(i => i.confirmacion_tec == Atendido)
               .Where(i => i.confirmacion_super == Recibido)
               .Where(i => i.confirmacion_lider == Cerrado)
               .Where(i => i.eliminado == false)
               //.Where(i=>i.prog==false)

               .Where(i => i.codigo.Contains(codigodata))

               .OrderBy(i => i.programado_ts)
                          .Take(pageSize)
                          .ToListAsync();

                return anomalia.Select(i => new RegistroSheViewModel
                {
                    idregistroshe = i.idshe,
                    codigo = i.codigo,
                    emision_ts = i.emision_ts.ToString("dd/MM/yyyy"),
                    idusuario = i.idusuario,
                    usuario = i.usuario.username,
                    prioridad = i.prioridad,
                    turno = i.turno,
                    hora_reporte = i.emision_ts.ToString("hh:mm:ss tt"),
                    idarea = i.idarea,
                    area = i.area.nombre,
                    idsector = i.sector.idsector,
                    sector = i.sector.nombre,
                    idmaquina = i.idmaquina,
                    maquina = i.maquina.nombre,
                    she_ma = i.she_ma,
                    paro_equipo = i.paro_equipo,
                    idfalla = i.idfalla,
                    falla = i.Falla.nombre,
                    idcondicion = i.idcondicion,
                    condicioinsegura = i.condicionInsegura.nombre,
                    ope_mtto = i.ope_mtto,
                    descripcion = i.descripcion,
                    programado_ts = i.programado_ts.ToString("dd/MM/yy HH:mm"),
                    tiempo_vence = calDias.VmtoDias(i.programado_ts),
                    alarma = calDias.Alarma(i.programado_ts),
                    // valor true: ya no puede modificar la tarjeta , false: aun puede modificar la tarjeta
                    edicion = calDias.TiempoLimiteModificarTarjeta(i.emision_ts),
                    estadoTarjeta = estado,
                    prog = i.prog,
                    foto = i.foto_icon.foto_anomalia
                });
            }
        }



        // GET: api/RegistroShes/ListarVerdeAzulAtencion
        // [Authorize(Roles = "Almacenero,Administrador")]
        [HttpGet("[action]/{idSectorData}")]
        public async Task<IEnumerable<RegistroSheViewModel>> ListarVerdeAzulAtencion([FromRoute] string idSectorData)
        {
            CriticidadActual calDias = new CriticidadActual();
            ValidacionIsNullOrEmpy validar = new ValidacionIsNullOrEmpy();

#pragma warning disable EF1000 // Possible SQL injection vulnerability.
            List<RegistroShe> she = await _context.Registroshe
                  .FromSql($"SELECT * FROM tb_registro_she where " +
                          $"" + validar.ValidarDatoCadena(idSectorData, "idarea") + " IN (" + validar.ValidarDato(idSectorData) + ")")
#pragma warning restore EF1000 // Possible SQL injection vulnerability.


                .Include(i => i.usuario)
                .Include(i => i.area)
                .Include(i => i.sector)
                .Include(i => i.maquina)
                .Include(i => i.condicionInsegura)
                .Include(i => i.Falla)
                .Include(i => i.foto_icon)

                .Where(i => i.ope_mtto == "operador")
                .Where(i => i.confirmacion_tec == false)
                .Where(i => i.confirmacion_super == false)
                .Where(i => i.confirmacion_lider == false)
                .Where(i => i.eliminado == false)
                .OrderBy(i => i.programado_ts)
                .Take(300)
                .ToListAsync();

            return she.Select(i => new RegistroSheViewModel
            {
                idregistroshe = i.idshe,
                //idproveedor = i.idproveedor,
                //proveedor = i.persona.nombre,
                codigo = i.codigo,
                emision_ts = i.emision_ts.ToString("dd/MM/yyyy"),
                idusuario = i.idusuario,
                usuario = i.usuario.username,
                prioridad = i.prioridad,
                turno = i.turno,
                hora_reporte = i.emision_ts.ToString("hh:mm:ss tt"),
                idarea = i.idarea,
                area = i.area.nombre,
                idsector = i.sector.idsector,
                sector = i.sector.nombre,
                idmaquina = i.idmaquina,
                maquina = i.maquina.nombre,
                she_ma = i.she_ma,
                paro_equipo = i.paro_equipo,
                idfalla = i.idfalla,
                falla = i.Falla.nombre,
                idcondicion = i.idcondicion,
                condicioinsegura = i.condicionInsegura.nombre,
                descripcion = i.descripcion,
                ope_mtto = i.ope_mtto,
                programado_ts = i.programado_ts.ToString("dd/MM/yy HH:mm"),
                tiempo_vence = calDias.VmtoDias(i.programado_ts),
                alarma = calDias.Alarma(i.programado_ts),
                // valor true: ya no puede modificar la tarjeta , false: aun puede modificar la tarjeta
                edicion = calDias.TiempoLimiteModificarTarjeta(i.emision_ts),
                foto=i.foto_icon.foto_anomalia
                 


            });
        }

        // GET: api/RegistroShes/ListarVerdeAzulLiderAtencion
        // [Authorize(Roles = "Almacenero,Administrador")]
        [HttpGet("[action]")]
        public async Task<IEnumerable<RegistroSheViewModel>> ListarVerdeAzulLiderAtencion()
        {
            CriticidadActual calDias = new CriticidadActual();
            ValidacionIsNullOrEmpy validar = new ValidacionIsNullOrEmpy();
            List<RegistroShe> she = await _context.Registroshe

                .Include(i => i.usuario)
                .Include(i => i.area)
                .Include(i => i.sector)
                .Include(i => i.maquina)
                .Include(i => i.condicionInsegura)
                .Include(i => i.Falla)
                .Include(i => i.foto_icon)
                .Where(i => i.ope_mtto == "operador")
                .Where(i => i.confirmacion_tec == false)
                .Where(i => i.confirmacion_super == false)
                .Where(i => i.confirmacion_lider == false)
                .Where(i => i.eliminado == false)
                .OrderBy(i => i.programado_ts)
                .Take(300)
                .ToListAsync();

            return she.Select(i => new RegistroSheViewModel
            {
                idregistroshe = i.idshe,
                //idproveedor = i.idproveedor,
                //proveedor = i.persona.nombre,
                codigo = i.codigo,
                emision_ts = i.emision_ts.ToString("dd/MM/yyyy"),
                idusuario = i.idusuario,
                usuario = i.usuario.username,
                prioridad = i.prioridad,
                turno = i.turno,
                hora_reporte = i.emision_ts.ToString("hh:mm:ss tt"),
                idarea = i.idarea,
                area = i.area.nombre,
                idsector = i.sector.idsector,
                sector = i.sector.nombre,
                idmaquina = i.idmaquina,
                maquina = i.maquina.nombre,
                she_ma = i.she_ma,
                paro_equipo = i.paro_equipo,
                idfalla = i.idfalla,
                falla = i.Falla.nombre,
                idcondicion = i.idcondicion,
                condicioinsegura = i.condicionInsegura.nombre,
                descripcion = i.descripcion,
                ope_mtto = i.ope_mtto,
                programado_ts = i.programado_ts.ToString("dd/MM/yy HH:mm"),
                tiempo_vence = calDias.VmtoDias(i.programado_ts),
                alarma = calDias.Alarma(i.programado_ts),
                // valor true: ya no puede modificar la tarjeta , false: aun puede modificar la tarjeta
                edicion = calDias.TiempoLimiteModificarTarjeta(i.emision_ts),
                foto=i.foto_icon.foto_anomalia
                 


            });
        }

        // GET: api/RegistroShes/ListarTipoTarjetaRecibir
        // [Authorize(Roles = "Almacenero,Administrador")]
        [HttpGet("[action]/{idSectorData}")]
        public async Task<IEnumerable<RegistroViewModelRecibir>> ListarTipoTarjetaRecibir([FromRoute] string idSectorData)
        {
            ValidacionIsNullOrEmpy validar = new ValidacionIsNullOrEmpy();



#pragma warning disable EF1000 // Possible SQL injection vulnerability.
            List<RegistroShe> she = await _context.Registroshe

                .FromSql($"SELECT * FROM tb_registro_she where " +
                          $"" + validar.ValidarDatoCadena(idSectorData, "idarea") + " IN (" + validar.ValidarDato(idSectorData) + ")")
#pragma warning restore EF1000 // Possible SQL injection vulnerability.

                .Include(i => i.usuario)
                .Include(i => i.usuariotecnico)
                .Include(i => i.area)
                .Include(i => i.sector)
                .Include(i => i.maquina)
                .Include(i => i.condicionInsegura)
                .Include(i => i.Falla)

                .Where(i => i.confirmacion_tec == true)
                .Where(i => i.confirmacion_super == false)
                .Where(i => i.confirmacion_lider == false)
                .Where(i => i.eliminado == false)
                .OrderBy(i => i.programado_ts)
                .Take(300)
                .ToListAsync();

            return she.Select(i => new RegistroViewModelRecibir
            {
                idregistroshe = i.idshe,
                codigo = i.codigo,
                emision_ts = i.emision_ts.ToString("dd/MM/yyyy"),
                idusuario = i.idusuario,
                usuario = i.usuario.username,
                tecnico = i.usuariotecnico.username,
                prioridad = i.prioridad,
                turno = i.turno,
                hora_reporte = i.emision_ts.ToString("hh:mm:ss tt"),
                idarea = i.idarea,
                area = i.area.nombre,
                idsector = i.sector.idsector,
                sector = i.sector.nombre,
                idmaquina = i.idmaquina,
                maquina = i.maquina.nombre,
                she_ma = i.she_ma,
                paro_equipo = i.paro_equipo,
                idfalla = i.idfalla,
                falla = i.Falla.nombre,
                idcondicion = i.idcondicion,
                condicioinsegura = i.condicionInsegura.nombre,
                descripcion = i.descripcion,
                ope_mtto = i.ope_mtto,
                sol_implementada = i.sol_implementada,
                fecha_solucion = i.ejecucion_ts.ToString("dd/MM/yyyy HH:mm"),
                foto = i.foto_icon.foto_anomalia

            });
        }

        // GET: api/RegistroShes/ListarTipoTarjetaRecibirLiderSuperProduccion   por id
        // [Authorize(Roles = "Almacenero,Administrador")]
        [HttpGet("[action]/{id}/{rol}/{idSectorData}")]
        public async Task<IEnumerable<RegistroViewModelRecibir>> ListarTipoTarjetaRecibirLiderSuperProduccion([FromRoute] int id, string rol, string idSectorData)
        {
            if (rol == "TECNICO" || rol == "LIDER" || rol == "OPERADOR LIDER" || rol == "OPERADOR" || rol == "SUPERVISOR PRODUCCION"  || rol == "SUPERVISOR MANTENIMIENTO")
            {

#pragma warning disable EF1000 // Possible SQL injection vulnerability.
                List<RegistroShe> anomalia = await _context.Registroshe
                    .FromSql($"SELECT * FROM tb_registro_she where " +
                          $"" + validar.ValidarDatoCadena(idSectorData, "idarea") + " IN (" + validar.ValidarDato(idSectorData) + ")")
#pragma warning restore EF1000 // Possible SQL injection vulnerability.
                   .Include(i => i.usuario)
                .Include(i => i.usuariotecnico)
                .Include(i => i.area)
                .Include(i => i.sector)
                .Include(i => i.maquina)
                .Include(i => i.condicionInsegura)
                .Include(i => i.Falla)
                .Include(i => i.foto_icon)

                .Where(i => i.confirmacion_tec == true)
                .Where(i => i.confirmacion_super == false)
                .Where(i => i.confirmacion_lider == false)
                .Where(i => i.eliminado == false)
                .OrderBy(i => i.programado_ts)
                .Take(300)
                .ToListAsync();

                return anomalia.Select(i => new RegistroViewModelRecibir
                {
                    idregistroshe = i.idshe,
                    codigo = i.codigo,
                    emision_ts = i.emision_ts.ToString("dd/MM/yyyy"),
                    idusuario = i.idusuario,
                    usuario = i.usuario.username,
                    tecnico = i.usuariotecnico.username,
                    prioridad = i.prioridad,
                    turno = i.turno,
                    hora_reporte = i.emision_ts.ToString("hh:mm:ss tt"),
                    idarea = i.idarea,
                    area = i.area.nombre,
                    idsector = i.sector.idsector,
                    sector = i.sector.nombre,
                    idmaquina = i.idmaquina,
                    maquina = i.maquina.nombre,
                    she_ma = i.she_ma,
                    paro_equipo = i.paro_equipo,
                    idfalla = i.idfalla,
                    falla = i.Falla.nombre,
                    idcondicion = i.idcondicion,
                    condicioinsegura = i.condicionInsegura.nombre,
                    descripcion = i.descripcion,
                    ope_mtto = i.ope_mtto,
                    sol_implementada = i.sol_implementada,
                    fecha_solucion = i.ejecucion_ts.ToString("dd/MM/yyyy HH:mm"),
                    foto = i.foto_icon.foto_anomalia



                });
            }
            else
            {
                List<RegistroShe> anomalia = await _context.Registroshe
                    .Include(i => i.usuario)
                .Include(i => i.usuariotecnico)
                .Include(i => i.area)
                .Include(i => i.sector)
                .Include(i => i.maquina)
                .Include(i => i.condicionInsegura)
                .Include(i => i.Falla)
                .Include(i => i.foto_icon)

                .Where(i => i.confirmacion_tec == true)
                .Where(i => i.confirmacion_super == false)
                .Where(i => i.confirmacion_lider == false)
                .Where(i => i.eliminado == false)
                .OrderBy(i => i.programado_ts)
                .Take(300)
                .ToListAsync();

                return anomalia.Select(i => new RegistroViewModelRecibir
                {
                    idregistroshe = i.idshe,
                    codigo = i.codigo,
                    emision_ts = i.emision_ts.ToString("dd/MM/yyyy"),
                    idusuario = i.idusuario,
                    usuario = i.usuario.username,
                    tecnico = i.usuariotecnico.username,
                    prioridad = i.prioridad,
                    turno = i.turno,
                    hora_reporte = i.emision_ts.ToString("hh:mm:ss tt"),
                    idarea = i.idarea,
                    area = i.area.nombre,
                    idsector = i.sector.idsector,
                    sector = i.sector.nombre,
                    idmaquina = i.idmaquina,
                    maquina = i.maquina.nombre,
                    she_ma = i.she_ma,
                    paro_equipo = i.paro_equipo,
                    idfalla = i.idfalla,
                    falla = i.Falla.nombre,
                    idcondicion = i.idcondicion,
                    condicioinsegura = i.condicionInsegura.nombre,
                    descripcion = i.descripcion,
                    ope_mtto = i.ope_mtto,
                    sol_implementada = i.sol_implementada,
                    fecha_solucion = i.ejecucion_ts.ToString("dd/MM/yyyy HH:mm"),
                    foto = i.foto_icon.foto_anomalia

                });
            }
        }


        // GET: api/RegistroShes/ListarTipoTarjetaCerrar
        // [Authorize(Roles = "Almacenero,Administrador")]
        [HttpGet("[action]")]
        public async Task<IEnumerable<RegistroViewModelCerrar>> ListarTipoTarjetaCerrar()
        {
            List<RegistroShe> she = await _context.Registroshe

                .Include(i => i.usuario)
                .Include(i => i.usuariotecnico)
                .Include(i => i.usuariosupervisor)
                .Include(i => i.area)
                .Include(i => i.sector)
                .Include(i => i.maquina)
                .Include(i => i.condicionInsegura)
                .Include(i => i.Falla)
                .Where(i => i.confirmacion_tec == true)
                .Where(i => i.confirmacion_super == true)
                .Where(i => i.confirmacion_lider == false)
                .Where(i => i.eliminado == false)
                .OrderBy(i => i.programado_ts)
                .Take(300)
                .ToListAsync();

            return she.Select(i => new RegistroViewModelCerrar
            {
                idregistroshe = i.idshe,
                codigo = i.codigo,
                emision_ts = i.emision_ts.ToString("dd/MM/yyyy"),
                idusuario = i.idusuario,
                usuario = i.usuario.username,
                tecnico = i.usuariotecnico.username,
                supervisor = i.usuariosupervisor.username,
                prioridad = i.prioridad,
                turno = i.turno,
                hora_reporte = i.emision_ts.ToString("hh:mm:ss tt"),
                idarea = i.idarea,
                area = i.area.nombre,
                idsector = i.sector.idsector,
                sector = i.sector.nombre,
                idmaquina = i.idmaquina,
                maquina = i.maquina.nombre,
                she_ma = i.she_ma,
                paro_equipo = i.paro_equipo,
                idfalla = i.idfalla,
                falla = i.Falla.nombre,
                idcondicion = i.idcondicion,
                condicioinsegura = i.condicionInsegura.nombre,
                descripcion = i.descripcion,
                ope_mtto = i.ope_mtto,
                sol_implementada = i.sol_implementada,
                fecha_solucion = i.ejecucion_ts.ToString("dd/MM/yyyy HH:mm"),
                fecha_recibido = i.recepcion_ts.ToString("dd/MM/yyyy HH:mm"),
                foto = i.foto_icon.foto_anomalia

            });
        }

        ValidacionIsNullOrEmpy validar = new ValidacionIsNullOrEmpy();
        // GET: api/RegistroShes/ListarTipoTarjetaCerrarShe   por id
        // [Authorize(Roles = "Almacenero,Administrador")]
        [HttpGet("[action]/{id}/{rol}/{idSectorData}")]

        public async Task<IEnumerable<RegistroViewModelCerrar>> ListarTipoTarjetaCerrarShe([FromRoute] int id, string rol, string idSectorData)
        {
            if (rol == "OPERADOR LIDER" || rol == "SUPERVISOR PRODUCCION" || rol == "SUPERVISOR MANTENIMIENTO")
            {

#pragma warning disable EF1000 // Possible SQL injection vulnerability.
                List<RegistroShe> anomalia = await _context.Registroshe
                    .FromSql($"SELECT * FROM tb_registro_she where " +
                          $"" + validar.ValidarDatoCadena(idSectorData, "idarea") + " IN (" + validar.ValidarDato(idSectorData) + ")")
#pragma warning restore EF1000 // Possible SQL injection vulnerability.
                .Include(i => i.usuario)
                .Include(i => i.usuariotecnico)
                .Include(i => i.usuariosupervisor)
                .Include(i => i.area)
                .Include(i => i.sector)
                .Include(i => i.maquina)
                .Include(i => i.condicionInsegura)
                .Include(i => i.Falla)
                .Include(i => i.foto_icon)

                .Where(i => i.confirmacion_tec == true)
                .Where(i => i.confirmacion_super == true)
                .Where(i => i.confirmacion_lider == false)
                .Where(i => i.eliminado == false)
                .OrderBy(i => i.programado_ts)
                .Take(300)
                .ToListAsync();

                return anomalia.Select(i => new RegistroViewModelCerrar
                {
                    idregistroshe = i.idshe,
                    codigo = i.codigo,
                    emision_ts = i.emision_ts.ToString("dd/MM/yyyy"),
                    idusuario = i.idusuario,
                    usuario = i.usuario.username,
                    tecnico = i.usuariotecnico.username,
                    supervisor = i.usuariosupervisor.username,
                    prioridad = i.prioridad,
                    turno = i.turno,
                    hora_reporte = i.emision_ts.ToString("hh:mm:ss tt"),
                    idarea = i.idarea,
                    area = i.area.nombre,
                    idsector = i.sector.idsector,
                    sector = i.sector.nombre,
                    idmaquina = i.idmaquina,
                    maquina = i.maquina.nombre,
                    she_ma = i.she_ma,
                    paro_equipo = i.paro_equipo,
                    idfalla = i.idfalla,
                    falla = i.Falla.nombre,
                    idcondicion = i.idcondicion,
                    condicioinsegura = i.condicionInsegura.nombre,
                    descripcion = i.descripcion,
                    ope_mtto = i.ope_mtto,
                    sol_implementada = i.sol_implementada,
                    fecha_solucion = i.ejecucion_ts.ToString("dd/MM/yyyy HH:mm"),
                    fecha_recibido = i.recepcion_ts.ToString("dd/MM/yyyy HH:mm"),
                    foto = i.foto_icon.foto_anomalia




                });
            }
            else
            {
                List<RegistroShe> anomalia = await _context.Registroshe
                   .Include(i => i.usuario)
                .Include(i => i.usuariotecnico)
                .Include(i => i.usuariosupervisor)
                .Include(i => i.area)
                .Include(i => i.sector)
                .Include(i => i.maquina)
                .Include(i => i.condicionInsegura)
                .Include(i => i.Falla)
                .Include(i => i.foto_icon)
                .Where(i => i.confirmacion_tec == true)
                .Where(i => i.confirmacion_super == true)
                .Where(i => i.confirmacion_lider == false)
                .Where(i => i.eliminado == false)
                .OrderBy(i => i.programado_ts)
                .Take(300)
                .ToListAsync();

                return anomalia.Select(i => new RegistroViewModelCerrar
                {
                    idregistroshe = i.idshe,
                    codigo = i.codigo,
                    emision_ts = i.emision_ts.ToString("dd/MM/yyyy"),
                    idusuario = i.idusuario,
                    usuario = i.usuario.username,
                    tecnico = i.usuariotecnico.username,
                    supervisor = i.usuariosupervisor.username,
                    prioridad = i.prioridad,
                    turno = i.turno,
                    hora_reporte = i.emision_ts.ToString("hh:mm:ss tt"),
                    idarea = i.idarea,
                    area = i.area.nombre,
                    idsector = i.sector.idsector,
                    sector = i.sector.nombre,
                    idmaquina = i.idmaquina,
                    maquina = i.maquina.nombre,
                    she_ma = i.she_ma,
                    paro_equipo = i.paro_equipo,
                    idfalla = i.idfalla,
                    falla = i.Falla.nombre,
                    idcondicion = i.idcondicion,
                    condicioinsegura = i.condicionInsegura.nombre,
                    descripcion = i.descripcion,
                    ope_mtto = i.ope_mtto,
                    sol_implementada = i.sol_implementada,
                    fecha_solucion = i.ejecucion_ts.ToString("dd/MM/yyyy HH:mm"),
                    fecha_recibido = i.recepcion_ts.ToString("dd/MM/yyyy HH:mm"),
                    foto = i.foto_icon.foto_anomalia
                });
            }
        }




        // POST: api/RegistroShes/CrearAnomaliasShe
        [HttpPost("[action]")]
        public async Task<IActionResult> CrearAnomaliasShe([FromBody] CrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            DateTime fechaHora = DateTime.Now;
            //Calcula el turno actual T1,T2,T3
            TurnoActual turnoactual = new TurnoActual();
            CriticidadActual calDias = new CriticidadActual();


            RegistroShe anomalia = new RegistroShe
            {
                emision_ts = fechaHora,
                idusuario = model.idusuario,
                turno = turnoactual.TurnoActualSystema(),
                idarea = model.idarea,
                idsector = model.idsector,
                she_ma = model.she_ma,
                paro_equipo = model.paro_equipo,
                idfalla = model.idfalla,
                prioridad = model.prioridad,
                idcondicion = model.idcondicion,
                idmaquina = model.idmaquina,
                idtarjeta = 4,
                ope_mtto = model.ope_mtto,
                descripcion = model.descripcion,
                programado_ts = calDias.CalculoCriticidadDias(model.prioridad)



            };



            try
            {
                _context.Registroshe.Add(anomalia);
                await _context.SaveChangesAsync();

                var id = anomalia.idshe;
                foreach (var det in model.detalles)
                {
                    DetalleFotoAnomaliaShe detalle = new DetalleFotoAnomaliaShe
                    {
                        idshe = id,
                        foto_anomalia = det.foto_anomalia,
                        tipo_foto = det.tipo_foto,
                        eliminado = det.eliminado
                    };
                    _context.DetallefotoShe.Add(detalle);
                }
                foreach (var det1 in model.detallesThumbail)
                {
                    DetalleFotoAnomaliaThumbailShe detallesThumbail = new DetalleFotoAnomaliaThumbailShe
                    {
                        idshe = id,
                        foto_anomalia = det1.foto_anomalia,
                        tipo_foto = det1.tipo_foto,
                        eliminado = det1.eliminado
                    };
                    _context.DetallefotosThumbnailShe.Add(detallesThumbail);
                }

                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

            return Ok(anomalia.codigo);
        }


        // PUT: api/RegistroShes/Actualizar
        [HttpPut("[action]")]
        public async Task<IActionResult> Actualizar([FromBody] ActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idregistroshe <= 0)
            {
                return BadRequest();
            }

            var anomalia = await _context.Registroshe.FirstOrDefaultAsync(c => c.idshe == model.idregistroshe);
            CriticidadActual calDias = new CriticidadActual();

            if (anomalia == null)
            {
                return NotFound();
            }

            anomalia.prioridad = model.prioridad;
            anomalia.idarea = model.idarea;
            anomalia.idsector = model.idsector;
            anomalia.idmaquina = model.idmaquina;
            anomalia.idfalla = model.idfalla;
            anomalia.idcondicion = model.idcondicion;
            anomalia.descripcion = model.descripcion;
            anomalia.she_ma = model.she_ma;
            anomalia.paro_equipo = model.paro_equipo;
            anomalia.ope_mtto = model.ope_mtto;
            anomalia.programado_ts = calDias.CalculoCriticidadDias(model.prioridad);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }
        // PUT: api/RegistroShes/AplicarContramedida
        [HttpPut("[action]")]
        public async Task<IActionResult> AplicarContramedida([FromBody] ActualizarViewModelContramedida model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idregistroshe <= 0)
            {
                return BadRequest();
            }

            RegistroShe anomalia = await _context.Registroshe.FirstOrDefaultAsync(c => c.idshe == model.idregistroshe);

            if (anomalia == null)
            {
                return NotFound();
            }
            DateTime fechaHora = DateTime.Now;

            anomalia.sol_implementada = model.sol_implementada;
            anomalia.ejecucion_ts = fechaHora;
            anomalia.idtecnico = model.idtecnico;
            anomalia.confirmacion_tec = true;
            //anomalia.foto_solucion = model.foto_solucion;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }


        // PUT: api/RegistroShes/ConfirmarRecepcion
        [HttpPut("[action]")]
        public async Task<IActionResult> ConfirmarRecepcion([FromBody] ActualizarViewModelConfirmacionTarjeta model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idregistroshe <= 0)
            {
                return BadRequest();
            }

            RegistroShe anomalia = await _context.Registroshe.FirstOrDefaultAsync(c => c.idshe == model.idregistroshe);

            if (anomalia == null)
            {
                return NotFound();
            }
            DateTime fechaHora = DateTime.Now;

            anomalia.confirmacion_super = true;
            anomalia.idsupervisor = model.idsupervisor;
            anomalia.recepcion_ts = fechaHora;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }

        // PUT: api/RegistrosAnomalias/ConfirmarCerrar
        [HttpPut("[action]")]
        public async Task<IActionResult> ConfirmarCerrar([FromBody] ActualizarViewModelCerrar model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idregistroshe <= 0)
            {
                return BadRequest();
            }

            RegistroShe anomalia = await _context.Registroshe.FirstOrDefaultAsync(c => c.idshe == model.idregistroshe);

            if (anomalia == null)
            {
                return NotFound();
            }
            DateTime fechaHora = DateTime.Now;

            anomalia.confirmacion_lider = true;
            anomalia.idlider = model.idlider;
            anomalia.cierre_ts = fechaHora;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }
        // PUT: api/RegistrosAnomalias/RevertirRecepcion/id
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> RevertirRecepcion([FromRoute] int id)
        {

            if (id <= 0)
            {
                return BadRequest();
            }

            var registro = await _context.Registroshe.FirstOrDefaultAsync(c => c.idshe == id);

            if (registro == null)
            {
                return NotFound();
            }

            registro.idsupervisor = null;
            registro.confirmacion_super = false;


            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }

        private bool RegistroSheExists(int id)
        {
            return _context.Registroshe.Any(e => e.idshe == id);
        }
    }
}
