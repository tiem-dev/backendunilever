﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sistema.Datos;
using Sistema.Entidades.Wcm._1_N;
using Sistema.Web.Models.Wcm._1_N.FotoAnomalia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Controllers.Wcm._1_N
{
    [Route("api/[controller]")]
    [ApiController]
    public class DetalleFotoAnomaliasController : ControllerBase
    {
        private readonly DbContextSistema _context;

        public DetalleFotoAnomaliasController(DbContextSistema context)
        {
            _context = context;
        }

        // Get: api/DetalleFotoAnomalias/VerFotoIngreso/id
        [HttpGet("[action]/{id}")]
        public async Task<IEnumerable<VerFotoViewModel>> VerFotoIngreso([FromRoute] int id)
        {
            var anomalia = await _context.DetallefotosShe.Where(a => a.idregistroanomalia == id && a.tipo_foto=="Ingreso" && a.eliminado==false)
               .ToListAsync();

            return anomalia.Select(p => new VerFotoViewModel
            {
                idfoto=p.idfoto,
                idregistroanomalia = p.idregistroanomalia,
                foto_anomalia = p.foto_anomalia,
                tipo_foto=p.tipo_foto,
                


            });
        }


        // Get: api/DetalleFotoAnomalias/VerFotoSolucion/id
        [HttpGet("[action]/{id}")]
        public async Task<IEnumerable<VerFotoViewModel>> VerFotoSolucion([FromRoute] int id)
        {
            var anomalia = await _context.DetallefotosShe.Where(a => a.idregistroanomalia == id && a.tipo_foto == "Solucion" && a.eliminado == false)
               .ToListAsync();

            return anomalia.Select(p => new VerFotoViewModel
            {
                idfoto = p.idfoto,
                idregistroanomalia = p.idregistroanomalia,
                foto_anomalia = p.foto_anomalia,
                tipo_foto = p.tipo_foto,



            });
        }

        // POST: api/DetalleFotoAnomalias/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] CrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            DetalleFotoAnomalia foto = new DetalleFotoAnomalia
            {
                idregistroanomalia = model.idregistroanomalia,
                foto_anomalia = model.foto_anomalia,
                tipo_foto = model.tipo_foto,
                eliminado = false
            };

            _context.DetallefotosShe.Add(foto);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

            return Ok();
        }


        // PUT: api/DetalleFotoAnomalias/Actualizar
        //[Authorize(Roles = "Almacenero,Vendedor,Administrador")]
        [HttpPut("[action]")]
        public async Task<IActionResult> Actualizar([FromBody] ActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idregistroanomalia <= 0)
            {
                return BadRequest();
            }

            var foto = await _context.DetallefotosShe.FirstOrDefaultAsync(p => p.idregistroanomalia == model.idregistroanomalia && p.tipo_foto=="Ingreso");
            var fotoicon = await _context.DetallefotosThumbnail.FirstOrDefaultAsync(p => p.idregistroanomalia == model.idregistroanomalia && p.tipo_foto == "Ingreso");


            if (foto == null)
            {
                return NotFound();
            }

            foto.foto_anomalia = model.foto_anomalia;
            fotoicon.foto_anomalia = model.foto_anomalia_icon;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }

        // PUT: api/DetalleFotoAnomalias/ActualizarFotoSolucion
        //[Authorize(Roles = "Almacenero,Vendedor,Administrador")]
        [HttpPut("[action]")]
        public async Task<IActionResult> ActualizarFotoSolucion([FromBody] ActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.idfoto <= 0)
            {
                return BadRequest();
            }

            var foto = await _context.DetallefotosShe.FirstOrDefaultAsync(p => p.idfoto == model.idfoto && p.tipo_foto == "Solucion");

            if (foto == null)
            {
                return NotFound();
            }

            foto.foto_anomalia = model.foto_anomalia;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }


        // GET: api/DetalleFotoAnomalias
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DetalleFotoAnomalia>>> GetDetallefotos()
        {
            return await _context.DetallefotosShe.ToListAsync();
        }

        // GET: api/DetalleFotoAnomalias/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DetalleFotoAnomalia>> GetDetalleFotoAnomalia(int id)
        {
            var detalleFotoAnomalia = await _context.DetallefotosShe.FindAsync(id);

            if (detalleFotoAnomalia == null)
            {
                return NotFound();
            }

            return detalleFotoAnomalia;
        }

        // PUT: api/DetalleFotoAnomalias/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDetalleFotoAnomalia(int id, DetalleFotoAnomalia detalleFotoAnomalia)
        {
            if (id != detalleFotoAnomalia.idfoto)
            {
                return BadRequest();
            }

            _context.Entry(detalleFotoAnomalia).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DetalleFotoAnomaliaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/DetalleFotoAnomalias
        [HttpPost]
        public async Task<ActionResult<DetalleFotoAnomalia>> PostDetalleFotoAnomalia(DetalleFotoAnomalia detalleFotoAnomalia)
        {
            _context.DetallefotosShe.Add(detalleFotoAnomalia);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDetalleFotoAnomalia", new { id = detalleFotoAnomalia.idfoto }, detalleFotoAnomalia);
        }

        // DELETE: api/DetalleFotoAnomalias/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<DetalleFotoAnomalia>> DeleteDetalleFotoAnomalia(int id)
        {
            var detalleFotoAnomalia = await _context.DetallefotosShe.FindAsync(id);
            if (detalleFotoAnomalia == null)
            {
                return NotFound();
            }

            _context.DetallefotosShe.Remove(detalleFotoAnomalia);
            await _context.SaveChangesAsync();

            return detalleFotoAnomalia;
        }

        private bool DetalleFotoAnomaliaExists(int id)
        {
            return _context.DetallefotosShe.Any(e => e.idfoto == id);
        }
    }
}
