﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sistema.Datos;
using Sistema.Entidades.Wcm;
using Sistema.Web.Models.Wcm.Area;

namespace Sistema.Web.Controllers.Wcm
//password wifi b850sbe5
{
    [Route("api/[controller]")]
    [ApiController]
    public class AreasController : ControllerBase
    {
        private readonly DbContextSistema _context;

        public AreasController(DbContextSistema context)
        {
            _context = context;
        }

        // GET:  api/Areas/SaltoFlujoNivel/id
        // [Authorize(Roles = "Almacenero,Administrador")]
        [HttpGet("[action]/{id}")]
        public async Task<IEnumerable<SelectViewModel>> SaltoFlujoNivel([FromRoute] int id)


        {
            var TotalSectorMa = await _context.Areas.Where(r => r.idproceso == id).ToListAsync();

            var cont = TotalSectorMa.Count();

            if (cont > 1)
            {
                var proceso = await _context.Areas
                    .Include(u => u.proceso)
                    .Where(a => a.idproceso == id)
                    .Where(a => a.activo == true)
                    .Where(a => a.eliminado == false)
                    .ToListAsync();


                return proceso.Select(a => new SelectViewModel
                {

                    idarea = a.idarea,
                    idproceso = a.idproceso,
                    proceso = a.proceso.nombre,
                    nombre = a.nombre

                });
            }
            else
            {
                var proceso = await _context.Areas
                    .Include(u => u.proceso)
                    .Where(a => a.idproceso == id)
                    .Where(a => a.activo == true)
                    .ToListAsync();


                return proceso.Select(a => new SelectViewModel
                {

                    idarea = a.idarea,
                    nombre=a.nombre

                });




            }
        }


        // GET: api/Areas/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<AreaViewModel>> Listar()
        {
            var categoria = await _context.Areas
                .Where(a => a.eliminado == false)
                .ToListAsync();

            return categoria.Select(c => new AreaViewModel
            {
                id = c.idarea,
                nombre = c.nombre,
                descripcion = c.descripcion,
                activo = c.activo,
                eliminado = c.eliminado
            });
        }


        // GET: api/Areas/SelectAreas
        [HttpGet("[action]")]

        public async Task<IEnumerable<SelectViewModel>> SelectAreas()
        {
            var persona = await _context.Areas
                .Where(a => a.eliminado == false)
                .Where(p => p.activo == true).ToListAsync();

            return persona.Select(p => new SelectViewModel
            {
                idarea = p.idarea,
                nombre = p.nombre
            });

        }
        // GET: api/Areas/Select
        [HttpGet("[action]")]
        public async Task<IEnumerable<SelectViewModel>> Select()
        {
            var categoria = await _context.Areas
                .Where(a => a.eliminado == false)
                .Where(c => c.activo == true)
                .ToListAsync();

            return categoria.Select(c => new SelectViewModel
            {
                idarea = c.idarea,
                nombre = c.nombre,

            });
        }

        // GET: api/Areas/Mostrar/Listarid
        [HttpGet("[action]/{id}")]
        public async Task<IEnumerable<SelectViewModel>> Listarid([FromRoute] int id)
        {
            var area = await _context.Areas
                .Include(u => u.proceso)
                .Where(a =>a.idproceso == id)
                .Where(a => a.activo == true)
                .ToListAsync();


            return area.Select(a => new SelectViewModel
            {

                idarea = a.idarea,
                idproceso = a.idproceso,
                proceso = a.proceso.nombre,
                nombre = a.nombre

            });

        }
        // GET: api/Areas/Mostrar/1
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Mostrar([FromRoute] int id)
        {

            var area = await _context.Areas.FindAsync(id);

            if (area == null)
            {
                return NotFound();
            }

            return Ok(new AreaViewModel
            {
                id = area.idarea,
                nombre = area.nombre,
                descripcion = area.descripcion,
                activo = area.activo,
                eliminado = area.eliminado
            });
        }


        // PUT: api/Areas/Actualizar
        [HttpPut("[action]")]
        public async Task<IActionResult> Actualizar([FromBody] ActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.id <= 0)
            {
                return BadRequest();
            }

            var categoria = await _context.Areas.FirstOrDefaultAsync(c => c.idarea == model.id);

            if (categoria == null)
            {
                return NotFound();
            }

            categoria.nombre = model.nombre;
            categoria.descripcion = model.descripcion;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }

        // POST: api/Areas/Crear
        [HttpPost("[action]")]
        public async Task<IActionResult> Crear([FromBody] CrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Area area = new Area
            {
                nombre = model.nombre,
                descripcion = model.descripcion,
                activo = true,
                eliminado = false
            };

            _context.Areas.Add(area);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

            return Ok();
        }

        /*
        // DELETE: api/Areas/Eliminar/1
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> Eliminar([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var areas = await _context.Areas.FindAsync(id);
            if (areas == null)
            {
                return NotFound();
            }

            _context.Areas.Remove(areas);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

            return Ok(areas);


        }
        */

        // PUT: api/Areas/Desactivar/1
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Desactivar([FromRoute] int id)
        {

            if (id <= 0)
            {
                return BadRequest();
            }

            var area = await _context.Areas.FirstOrDefaultAsync(c => c.idarea == id);

            if (area == null)
            {
                return NotFound();
            }

            area.activo = false;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }

        // PUT: api/Categorias/Activar/1
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Activar([FromRoute] int id)
        {

            if (id <= 0)
            {
                return BadRequest();
            }

            var area = await _context.Areas.FirstOrDefaultAsync(c => c.idarea == id);

            if (area == null)
            {
                return NotFound();
            }

            area.activo = true;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }


        // PUT: api/Areas/Eliminar/1
        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Eliminar([FromRoute] int id)
        {

            if (id <= 0)
            {
                return BadRequest();
            }

            var area = await _context.Areas.FirstOrDefaultAsync(c => c.idarea == id);

            if (area == null)
            {
                return NotFound();
            }

            area.eliminado = true;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                // Guardar Excepción
                return BadRequest();
            }

            return Ok();
        }

    }
}

