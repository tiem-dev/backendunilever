﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sistema.Datos;
using Sistema.Entidades.Wcm._1_N;
using Sistema.Web.Models.Wcm.Planta;

namespace Sistema.Web.Controllers.Wcm
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlantasController : ControllerBase
    {
        private readonly DbContextSistema _context;

        public PlantasController(DbContextSistema context)
        {
            _context = context;
        }

        // GET: api/Plantas/listar
        [HttpGet("[action]")]

        public async Task<IEnumerable<SelectViewModel>> listar()
        {
            var planta = await _context.Planta
                .Where(a => a.eliminado == false)
                .Where(p => p.activo == true).ToListAsync();

            return planta.Select(p => new SelectViewModel
            {
                idplanta = p.idplanta,
                nombre = p.nombre
            });

        }

        // GET: api/Plantas
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Planta>>> GetPlanta()
        {
            return await _context.Planta.ToListAsync();
        }

        // GET: api/Plantas/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Planta>> GetPlanta(int id)
        {
            var planta = await _context.Planta.FindAsync(id);

            if (planta == null)
            {
                return NotFound();
            }

            return planta;
        }

        // PUT: api/Plantas/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPlanta(int id, Planta planta)
        {
            if (id != planta.idplanta)
            {
                return BadRequest();
            }

            _context.Entry(planta).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlantaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Plantas
        [HttpPost]
        public async Task<ActionResult<Planta>> PostPlanta(Planta planta)
        {
            _context.Planta.Add(planta);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPlanta", new { id = planta.idplanta }, planta);
        }

        // DELETE: api/Plantas/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Planta>> DeletePlanta(int id)
        {
            var planta = await _context.Planta.FindAsync(id);
            if (planta == null)
            {
                return NotFound();
            }

            _context.Planta.Remove(planta);
            await _context.SaveChangesAsync();

            return planta;
        }

        private bool PlantaExists(int id)
        {
            return _context.Planta.Any(e => e.idplanta == id);
        }
    }
}
