﻿using System;

namespace Sistema.Web.Controllers.CalculosAuxliares
{
    public class CriticidadActual
    {


        public bool TiempoLimiteModificarTarjeta(DateTime param1)
        {

            DateTime startTime = param1;
            DateTime endTime = DateTime.Now;

            TimeSpan span = endTime.Subtract(startTime);
            //Console.WriteLine("Time Difference (seconds): " + span.Seconds);
           // Console.WriteLine("Time Difference (minutes): " + span.Minutes);
           // Console.WriteLine("Time Difference (hours): " + span.Hours);
            //Console.WriteLine("Time Difference (days): " + span.Days);

            bool horas;
            if (span.Hours >= 4 || span.Days >= 1)
            {
                horas = true;
                // horas = "No se puede editar la tarjeta  operacion restringida";

            }
            else
            {
                //horas = "Puede editar la tarjeta  operacion vigente";
                horas = false;

            }
            return horas;

            //return System.Convert.ToString(fechaConvertida);

        }

        public bool Alarma(DateTime fecha)
        {
            DateTime tiempoCalculado = fecha;
            DateTime fechasys = DateTime.Now;

            bool horas;

            if (tiempoCalculado < fechasys)
            {
                //Este código devuelve una cadena del tipo 04d 01h 44m
                horas = true;

            }

            else if (tiempoCalculado > fechasys)
            {
                //Este código devuelve una cadena del tipo 04d 01h 44m
                horas = false;

            }
            else
            {
                horas = false;

            }
            return horas;
            //return System.Convert.ToString(fechaConvertida);

        }

        public string VmtoDias(DateTime fecha)
        {
            DateTime tiempoCalculado = fecha;
            DateTime fechasys = DateTime.Now;

            var horas = "";

            if (tiempoCalculado < fechasys)
            {
                //Este código devuelve una cadena del tipo 04d 01h 44m
                //horas = "RETRASADO:" + (DateTime.Now - tiempoCalculado).ToString(@"dd\d\ hh\h\ mm\m\ ");
                horas = "RETRASADO: " + (DateTime.Now - tiempoCalculado).ToString(@"dd\d\ hh\h");

            }

            else if (tiempoCalculado > fechasys)
            {
                //Este código devuelve una cadena del tipo 04d 01h 44m
                horas = "EJECUCIÓN: " + (DateTime.Now - tiempoCalculado).ToString(@"dd\d\ hh\h");

            }
            else
            {
                horas = ("Na");

            }
            return horas;
            //return System.Convert.ToString(fechaConvertida);

        }

        public DateTime CalculoCriticidadDias(string TipoCriticidad)
        {
            DateTime fechasys = DateTime.Now;
            //sumar dias

            var criticidad = TipoCriticidad;

            DateTime horas;

            if (criticidad == "A")
            {
                //Este código devuelve una cadena con la suma de dias
                horas = fechasys.AddDays(1);

            }

            else if (criticidad == "B")
            {

                //Este código devuelve una cadena con la suma de dias               
                horas = fechasys.AddDays(2);
            }
            else if (criticidad == "C")
            {
                //Este código devuelve una cadena con la suma de dias
                horas = fechasys.AddDays(7);
            }
            else if (criticidad == "VA")
            {
                //Este código devuelve una cadena con la suma de dias
                horas = fechasys.AddDays(1);
            }

            else if (criticidad == "VB")
            {
                //Este código devuelve una cadena con la suma de dias
                horas = fechasys.AddDays(2);
            }
            else if (criticidad == "VC")
            {
                //Este código devuelve una cadena con la suma de dias
                horas = fechasys.AddDays(3);
            }
            else
            {
                horas = fechasys;

            }
            return horas;
            //return System.Convert.ToString(fechaConvertida);

        }
    }
}
