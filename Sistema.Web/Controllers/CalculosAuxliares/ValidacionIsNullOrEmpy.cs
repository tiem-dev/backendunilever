﻿namespace Sistema.Web.Controllers.CalculosAuxliares
{
    public class ValidacionIsNullOrEmpy
    {

        public string ControlEstado(bool atendido,bool recibido,bool cerrado)
        {

            if (cerrado == true)
            {
                return "cerrado";
            }
            else if (recibido == true)
            {
                return "Recibido";
            }
            
            else if (atendido == true)
            {
                return "Atendido";
            }
            else
            {
                return "ingresado"; 
            }
        }
        public bool ControlEstadoTarjetaAtendido(string input)
        {

            if (input == "ATENDIDO" || input == "RECIBIDO" || input == "CERRADO")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool ControlEstadoTarjetaRecibido(string input)
        {

            if (input == "RECIBIDO" || input == "CERRADO")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool ControlEstadoTarjetaCerrado(string input)
        {

            if (input == "CERRADO")
            {
                return true;
            }
            else
            {
                return false;
            }


        }
        public string ValidarDato(string input)
        {

            if (input == "0")
            {
                return input = "1";
            }
            else
            {
                return input;
            }


        }
        public string ValidarDatoCadena(string input, string condicion)
        {

            if (input == "0")
            {
                return "1";
            }
            else
            {
                return condicion;
            }

        }
        public string ValidarDatoContains(string input)
        {

            if (input == "0")
            {
                return input = "";
            }
            else
            {
                return input;
            }

        }
        public string ValidarDatoSigno(string input)
        {

            if (input == "0")
            {
                return input = "=";
            }
            else if (input == "RETRASADO")
            {
                return input = ">=";
            }

            else if (input == "EJECUCION")
            {
                return input = "<=";
            }
            else
            {
                return input = "=";
            }

        }
        public string ValidarDatoTarjeta(string input)
        {

            if (input == "0")
            {
                return input = "1";
            }
            else if (input == "ROJA")
            {
                return input = "1";
            }

            else if (input == "AZUL")
            {
                return input = "2";
            }
            else if (input == "AMARILLA")
            {
                return input = "3";
            }
            else
            {
                return input = "1";
            }

        }
        public string ValidarDatoProgramado(string input)
        {

            if (input == "0")
            {
                return input = "1";
            }
            else if (input == "SI")
            {
                return input = "'" + "true" + "'";
            }

            else if (input == "NO")
            {
                return input = "'" + "false" + "'";
            }
            else
            {
                return input = "1";
            }

        }

    }
}
