﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Controllers.CalculosAuxliares
{
    public class EncriptacionPassword
    {

        public string GetHashCodeInt64(string input)
        {
            var data = System.Text.Encoding.ASCII.GetBytes(input);
            data = System.Security.Cryptography.SHA1.Create().ComputeHash(data);
            return Convert.ToBase64String(data);
        }
    }


}
