﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Sistema.Web.Controllers.CalculosAuxliares
{
    public class Fechas
    {
        public string[] ResumenMeses(int[] anio)
        {

            CultureInfo ci = CultureInfo.CreateSpecificCulture("es-ES");
            DateTimeFormatInfo dtfi = ci.DateTimeFormat;
            dtfi.AbbreviatedMonthNames = new string[] { "Ene", "Feb", "Mar",
                                                  "Abr", "May", "Jun",
                                                  "Jul", "Ago", "Sep",
                                                  "Oct", "Nov", "Dec", "" };

            int[] years = anio;
            Console.WriteLine("Days in the Month for the {0} culture " +
                              "using the {1} calendar\n",
                              CultureInfo.CurrentCulture.Name,
                              dtfi.Calendar.GetType().Name.Replace("Calendar", ""));
            Console.WriteLine("Year" + "-" + "Month");
            List<string> data = new List<string>();

            foreach (var year in years)
            {
                for (int ctr = 0; ctr <= dtfi.MonthNames.Length - 1; ctr++)
                {
                    if (String.IsNullOrEmpty(dtfi.MonthNames[ctr]))
                        continue;

                    data.Add(dtfi.AbbreviatedMonthNames[ctr] + "-" + year);

                    //Console.WriteLine(dtfi.AbbreviatedMonthNames[ctr] + "-" + year);
                }
                Console.WriteLine();

            }
            return data.ToArray();


        }


    }
}
