﻿
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Security", "EF1000:Possible SQL injection vulnerability.", Justification = "<Pendiente>", Scope = "member", Target = "~M:Sistema.Web.Controllers.Wcm._1_N.RegistroShesController.ListarTipoTarjetaRecibir(System.String,System.String)~System.Threading.Tasks.Task{System.Collections.Generic.IEnumerable{Sistema.Web.Models.Wcm._1_N.RegistroShe.RegistroViewModelRecibir}}")]

