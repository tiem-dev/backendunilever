﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Models.Usuarios.Usuario_rol
{
    public class SelectViewModel
    {
        public int idusuariorol { get; set; }
        public int idusuario { get; set; }
        public int idrol { get; set; }
    }
}
