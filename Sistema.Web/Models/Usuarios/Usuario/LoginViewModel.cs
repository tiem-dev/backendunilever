﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Models.Usuarios.Usuario
{
    public class LoginViewModel
    {
        [Required]
        //[usernameAddress]
        public string username { get; set; }
        [Required]
        public string password { get; set; }
    }
}
