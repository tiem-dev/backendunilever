﻿using System.Collections.Generic;

namespace Sistema.Web.Models.Usuarios.UsuarioNet
{
    public class UsuarioListaRolesViewModel
    {
        public int idusuario { get; set; }
        public int id_rol { get; set; }
        public string nombre_rol { get; set; }
    }
}
