﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Models.Usuarios.UsuarioNet
{
    public class Usuario_areaViewModel
    {
        public int idrelacionarea { get; set; }
        public int idusuario { get; set; }
        public int idarea { get; set; }
    }
}
