﻿

namespace Sistema.Web.Models.Usuarios.UsuarioNet
{
    public class UsuarioViewModel
    {
        public int idusuario { get; set; }
        public string username { get; set; }
        public string fname { get; set; }
        public string lname { get; set; }
        public string telefono { get; set; }
        public string email { get; set; }
        public byte[] password_hash { get; set; }
        public bool activo { get; set; }
        public string avatar { get; set; }
        public int idrol { get; set; }
        public string rol { get; set; }




    }
}
