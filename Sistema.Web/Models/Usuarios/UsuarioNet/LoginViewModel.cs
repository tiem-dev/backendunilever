﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Models.Usuarios.UsuarioNet
{
    public class LoginViewModel
    {
       // public string username { get; set; }
        [Required]
        public string password { get; set; }
        [Required]
        public string username { get; set; }

    }
}
