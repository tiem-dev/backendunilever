﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Models.Usuarios.UsuarioNet
{
    public class Usuario_rolViewModel
    {
        public int id { get; set; }
        public int user_id { get; set; }
        public int role_id { get; set; }
    }
}