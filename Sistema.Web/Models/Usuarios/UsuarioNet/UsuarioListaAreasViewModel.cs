﻿namespace Sistema.Web.Models.Usuarios.UsuarioNet
{
    public class UsuarioListaAreasViewModel
    {
        public int idusuario { get; set; }
        public int id_area { get; set; }
        public string nombre_area { get; set; }
    }
}
