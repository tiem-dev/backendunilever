﻿using Sistema.Web.Models.Usuarios.Usuario_area;
using Sistema.Web.Models.Usuarios.Usuario_rol;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sistema.Web.Models.Usuarios.UsuarioNet
{
    public class CrearViewModel
    {
        [StringLength(50, MinimumLength = 3, ErrorMessage = "El nombre no debe de tener más de 50 caracteres, ni menos de 3 caracteres.")]
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string telefono { get; set; }
        public string username { get; set; }
        [Required]
        public string password { get; set; }
        public string avatar { get; set; }
        public List<CrearRelacionUsuarioViewModel> UsuariorolRelacion { get; set; }
        public List<CrearRelacionUsuarioAreaViewModel> UsuarioAreaRelacion { get; set; }

    }
}
