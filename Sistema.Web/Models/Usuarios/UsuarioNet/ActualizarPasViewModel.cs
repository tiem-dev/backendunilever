﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Models.Usuarios.UsuarioNet
{
    public class ActualizarPasViewModel
    {
        [Required]
        public int idusuario { get; set; }
        [Required]
        public string password { get; set; }
        public bool act_password { get; set; }
    }
}
