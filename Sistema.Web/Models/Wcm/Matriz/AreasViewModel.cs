﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Models.Wcm.Matriz
{
    public class AreasViewModel
    {
        public int idarea { get; set; }
        public string area { get; set; }
    }
}
