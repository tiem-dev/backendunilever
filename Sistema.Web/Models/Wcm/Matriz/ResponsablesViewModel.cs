﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Models.Wcm.Matriz
{
    public class ResponsablesViewModel
    {
        public string usuarioResponsable { get; set; }
        public int id { get; set; }
    }
}
