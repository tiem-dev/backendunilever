﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Models.Wcm._1_N.RegistroAnomalia
{
    public class RegistroViewModelRecibir
    {
        public int idregistroanomalia { get; set; }
        public string codigo { get; set; }
        public string emision_ts { get; set; }
        public int idusuario { get; set; }
        public string usuario { get; set; }
        public string tecnico { get; set; }
        public int paso_ma { get; set; }
        public string criticidad { get; set; }
        public string turno { get; set; }
        public int idarea { get; set; }
        public string area { get; set; }
        public int idsector { get; set; }
        public string sector { get; set; }

        public int idmaquina { get; set; }
        public string maquina { get; set; }

        public int idanomalia { get; set; }
        public string anomaliac { get; set; }

        public int idsuceso { get; set; }
        public string relacionado { get; set; }

        public int idtarjeta { get; set; }
        public string tarjeta { get; set; }

        public string descripcion { get; set; }
        public string sol_implementada { get; set; }
        public string fecha_solucion { get; set; }
        public int idproyecto { get; set; }
        public string proyecto { get; set; }
        public string foto { get; set; }


    }
}
