﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Models.Wcm._1_N.RegistroAnomalia
{
    public class MatrizViewModel
    {
        public int mes { set; get; }
        public int total { set; get; }
        public int area { set; get; }
        public string nombre { set; get; }

    }
}
