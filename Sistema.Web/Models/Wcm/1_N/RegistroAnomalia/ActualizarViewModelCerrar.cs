﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Models.Wcm._1_N.RegistroAnomalia
{
    public class ActualizarViewModelCerrar
    {
        public int idregistroanomalia { get; set; }
        [Required]
        public int idlider { get; set; }
    }
}
