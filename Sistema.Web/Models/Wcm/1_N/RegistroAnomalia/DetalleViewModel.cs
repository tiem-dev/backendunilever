﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Sistema.Web.Models.Wcm._1_N.RegistroAnomalia
{
    public class DetalleViewModel
    {
       public  string foto_anomalia { set; get; }
        public string tipo_foto { set; get; }
        public bool eliminado { set; get; }

    }
}
