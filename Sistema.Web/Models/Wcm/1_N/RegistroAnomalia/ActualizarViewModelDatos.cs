﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Models.Wcm._1_N.RegistroAnomalia
{
    public class ActualizarViewModelDatos
    {

        //Maestro data
        public int idregistroanomalia { get; set; }

        public int paso_ma { get; set; }
        public string criticidad { get; set; }
        [Required]
        public int idarea { get; set; }
        public int idsector { get; set; }
        [Required]
        public int idmaquina { get; set; }

        [Required]
        public int idanomalia { get; set; }
        [Required]
        public int idsuceso { get; set; }
        [Required]
        public string descripcion { get; set; }
    }
}
