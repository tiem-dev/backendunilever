﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Models.Wcm._1_N.Componentes
{
    public class SelectViewModel
    {
        public int idcomponente { get; set; }

        public string nombre { get; set; }
        public int idmaquina { get; set; }
        public string maquina { get; set; }
    }
}
