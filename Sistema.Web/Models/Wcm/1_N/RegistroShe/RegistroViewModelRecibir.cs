﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Models.Wcm._1_N.RegistroShe
{
    public class RegistroViewModelRecibir
    {
        public int idregistroshe { get; set; }
        public string codigo { get; set; }
        public string emision_ts { get; set; }
        public int idusuario { get; set; }
        public string usuario { get; set; }
        public string tecnico { get; set; }
        public string prioridad { get; set; }
        public string hora_reporte { get; set; }

        public string turno { get; set; }
        public int idarea { get; set; }
        public string area { get; set; }
        public int idsector { get; set; }

        public string sector { get; set; }

        public int idmaquina { get; set; }
        public string maquina { get; set; }
        public string she_ma { get; set; }
        public bool paro_equipo { get; set; }
        public int idfalla { get; set; }
        public string falla { get; set; }
        public int idcondicion { get; set; }
        public string condicioinsegura { get; set; }
        public string descripcion { get; set; }
        public string ope_mtto { get; set; }
        public string sol_implementada { get; set; }
        public string fecha_solucion { get; set; }

        public string foto { get; set; }
    }
}
