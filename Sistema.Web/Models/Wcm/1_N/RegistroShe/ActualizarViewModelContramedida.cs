﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Models.Wcm._1_N.RegistroShe
{
    public class ActualizarViewModelContramedida
    {
        public int idregistroshe { get; set; }
        public string sol_implementada { get; set; }
        public int idtecnico { get; set; }
        public string foto_solucion { get; set; }
    }
}
