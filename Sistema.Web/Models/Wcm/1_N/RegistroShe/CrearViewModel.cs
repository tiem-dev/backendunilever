﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Models.Wcm._1_N.RegistroShe
{
    public class CrearViewModel
    {

        [Required]
        public int idusuario { get; set; }
        public string prioridad { get; set; }
        public string turno { get; set; }

        [Required]
        public int idarea { get; set; }
        [Required]
        public int sector { get; set; }
        [Required]
        public int idmaquina { get; set; }
        [Required]
        public string she_ma { get; set; }
        [Required]
        public Boolean paro_equipo { get; set; }
        [Required]
        public int idfalla { get; set; }
        public int idcondicion { get; set; }
        public int idsector { get; set; }
        public int idtarjeta { get; set; }



        public string descripcion { get; set; }
        public string ope_mtto { get; set; }
        public List<DetalleViewModel> detalles { get; set; }
        public List<DetalleViewModel> detallesThumbail { get; set; }



    }
}
