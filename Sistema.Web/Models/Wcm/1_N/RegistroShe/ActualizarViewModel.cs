﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Models.Wcm._1_N.RegistroShe
{
    public class ActualizarViewModel
    {
        public int idregistroshe { get; set; }
        public string prioridad { get; set; }

        public int idarea { get; set; }
        public int idsector { get; set; }
        public int idmaquina { get; set; }
        public string she_ma { get; set; }
        public Boolean paro_equipo { get; set; }
        public int idfalla { get; set; }
        public int idcondicion { get; set; }
        public string descripcion { get; set; }
        public string ope_mtto { get; set; }
         
    }
}
