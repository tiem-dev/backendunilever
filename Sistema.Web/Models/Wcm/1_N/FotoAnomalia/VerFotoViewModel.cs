﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Models.Wcm._1_N.FotoAnomalia
{
    public class VerFotoViewModel
    {
        public int idfoto { get; set; }

        public int idregistroanomalia { get; set; }
        public string  foto_anomalia  { get; set; }
        public string tipo_foto { get; set; }
        public bool eliminado  { get; set; }

    }
}
