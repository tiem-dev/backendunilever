﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema.Web.Models.Wcm._1_N.FotoShe
{
    public class ActualizarViewModel
    {

        public int idfoto { get; set; }

        public int idshe { get; set; }
        public string foto_anomalia { get; set; }
        public string foto_anomalia_icon { get; set; }

    }
}
