﻿using Microsoft.EntityFrameworkCore;

using Sistema.Datos.Mapping.Pamco;
using Sistema.Datos.Mapping.ProyectoMap;
using Sistema.Datos.Mapping.Usuarios;
using Sistema.Datos.Mapping.Wcm;
using Sistema.Datos.Mapping.Wcm._1_N;
using Sistema.Entidades.Pamco;
using Sistema.Entidades.Proyecto;
using Sistema.Entidades.Usuarios;
using Sistema.Entidades.Wcm;
using Sistema.Entidades.Wcm._1_N;

namespace Sistema.Datos
{
    public class DbContextSistema : DbContext
    {
        /////////////////////////////////////////////////////////////////////////////////// Proyecto Curso
       

        /////////////////////////////////////////////////////////////////////////////////// Login
        public DbSet<RolNet> RolesNet { get; set; }
        public DbSet<UsuarioNet> UsuariosNet { get; set; }
        public DbSet<Area> Areas { get; set; }
        public DbSet<usuario_rol> usuario_rol { get; set; }
        public DbSet<usuario_area> usuario_area { get; set; }
        public DbSet<responsable_area> responsable_area { get; set; }

        /////////////////////////////////////////////////////////////////////////////////// PAMCO
        public DbSet<Categoria_perdida> Categoria_perdida { get; set; }
        public DbSet<Registro_pamco> Registro_Pamcos { get; set; }

        /////////////////////////////////////////////////////////////////////////////////// imagen
        public DbSet<DetalleFotoAnomalia> DetallefotosShe { get; set; }
        public DbSet<DetalleFotoAnomaliaThumbnail> DetallefotosThumbnail{ get; set; }

        public DbSet<DetalleFotoAnomaliaShe> DetallefotoShe { get; set; }
        public DbSet<DetalleFotoAnomaliaThumbailShe> DetallefotosThumbnailShe { get; set; }





        /////////////////////////////////////////////////////////////////////////////////// WCM
        public DbSet<Anomalia> Anomalias { get; set; }
        public DbSet<Tarjeta> Tarjetas { get; set; }
        public DbSet<Falla> Fallas { get; set; }
        public DbSet<CondicionInsegura> CondicionesInseguras { get; set; }
        public DbSet<Suceso> Sucesos { get; set; }
        public DbSet<Componente> Componente { get; set; }
        public DbSet<Maquina> Maquinas { get; set; }
        public DbSet<Sector> Sector { get; set; }
        public DbSet<Proceso> Proceso { get; set; }
        public DbSet<Planta> Planta { get; set; }

        public DbSet<Proyectos> Proyectos { get; set; }
        public DbSet<Registrosanomalias> Registrosanomalias { get; set; }
        public DbSet<RegistroShe> Registroshe { get; set; }



        // PAMCO

        public DbContextSistema(DbContextOptions<DbContextSistema> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
     

            //////////////////////////////////////////// usuarios
            modelBuilder.ApplyConfiguration(new RolNetMap());
            modelBuilder.ApplyConfiguration(new UsuarioNetMap());
            modelBuilder.ApplyConfiguration(new usuario_rolMap());
            modelBuilder.ApplyConfiguration(new Usuario_areaMap());
            modelBuilder.ApplyConfiguration(new Responsable_areaMap());

            //Modelos WCM
            modelBuilder.ApplyConfiguration(new AreaMap());
            modelBuilder.ApplyConfiguration(new AnomaliaMap());
            modelBuilder.ApplyConfiguration(new TarjetaMap());
            modelBuilder.ApplyConfiguration(new FallaMap());
            modelBuilder.ApplyConfiguration(new CondicioneInseguraMap());
            modelBuilder.ApplyConfiguration(new SucesoMap());
            modelBuilder.ApplyConfiguration(new ComponenteMap());
            modelBuilder.ApplyConfiguration(new MaquinaMap());
            modelBuilder.ApplyConfiguration(new SectorMap());
            modelBuilder.ApplyConfiguration(new ProcesoMap());
            modelBuilder.ApplyConfiguration(new PlantaMap());

            modelBuilder.ApplyConfiguration(new ProyectoMap());
            modelBuilder.ApplyConfiguration(new RegistroAnomaliaMap());
            modelBuilder.ApplyConfiguration(new DetalleFotoAnomaliaMap());
            modelBuilder.ApplyConfiguration(new DetalleFotoAnomaliaThumbailMap());
            modelBuilder.ApplyConfiguration(new DetalleFotoAnomaliaSheMap());
            modelBuilder.ApplyConfiguration(new DetalleFotoAnomaliaThumbailSheMap());


            modelBuilder.ApplyConfiguration(new RegistroSheMap());

            ///////////////////////////////////////////////////////////// PAMCO
            modelBuilder.ApplyConfiguration(new CategoriaPerdidaMap());
            modelBuilder.ApplyConfiguration(new RegistroPamcoMap());

            ///////////////////////////////////////////////////////////// imagen

            // USUARIO ROL DETALLE


        }

    }
}
