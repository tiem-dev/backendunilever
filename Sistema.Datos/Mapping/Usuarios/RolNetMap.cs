﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Sistema.Entidades.Usuarios;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sistema.Datos.Mapping.Usuarios
{
    class RolNetMap : IEntityTypeConfiguration<RolNet>
    {
        public void Configure(EntityTypeBuilder<RolNet> builder)
        {
            builder.ToTable("unilever_roles")
               .HasKey(c => c.id);
            builder.Property(c => c.role_name)
                .HasMaxLength(50);
           
        }
    }
}
