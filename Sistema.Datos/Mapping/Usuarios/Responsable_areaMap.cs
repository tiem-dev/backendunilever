﻿using Microsoft.EntityFrameworkCore;
using Sistema.Entidades.Usuarios;

namespace Sistema.Datos.Mapping.Usuarios
{
    public class Responsable_areaMap : IEntityTypeConfiguration<responsable_area>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<responsable_area> builder)
        {
            builder.ToTable("tb_relacion_responsable_area")
                .HasKey(c => c.idrelacion);

            builder.HasOne(i => i.UsuarioNet)
              .WithMany(p => p.responsable_area)
              .HasForeignKey(i => i.idusuario);

            builder.HasOne(i => i.areas)
              .WithMany(p => p.responsable_area)
              .HasForeignKey(i => i.idarea);


        }
    }
}
