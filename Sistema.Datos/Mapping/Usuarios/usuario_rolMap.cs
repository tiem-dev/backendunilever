﻿using Microsoft.EntityFrameworkCore;
using Sistema.Entidades.Usuarios;

namespace Sistema.Datos.Mapping.Usuarios
{
    public class usuario_rolMap : IEntityTypeConfiguration<usuario_rol>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<usuario_rol> builder)
        {
            builder.ToTable("unilever_user_rl")
                .HasKey(c => c.id);

            builder.HasOne(i => i.UsuarioNet)
              .WithMany(p => p.detalle_rol)
              .HasForeignKey(i => i.user_id);

            builder.HasOne(i => i.RolNet)
              .WithMany(p => p.detalle_usuario)
              .HasForeignKey(i => i.role_id);
              
        }
    }
}
