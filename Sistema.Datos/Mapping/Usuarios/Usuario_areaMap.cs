﻿using Microsoft.EntityFrameworkCore;
using Sistema.Entidades.Usuarios;

namespace Sistema.Datos.Mapping.Usuarios
{
    public class Usuario_areaMap : IEntityTypeConfiguration<usuario_area>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<usuario_area> builder)
        {
            builder.ToTable("tb_relacion_usuario_area")
                .HasKey(c => c.idrelacionarea);

            builder.HasOne(i => i.UsuarioNet)
              .WithMany(p => p.detalle_area)
              .HasForeignKey(i => i.idusuario);
              
            builder.HasOne(i => i.areas)
              .WithMany(p => p.detalle_area)
              .HasForeignKey(i => i.idarea);
              
              
        }
    }
}
