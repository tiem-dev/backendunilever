﻿using Microsoft.EntityFrameworkCore;
using Sistema.Entidades.Wcm._1_N;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sistema.Datos.Mapping.Wcm
{
    public class PlantaMap : IEntityTypeConfiguration<Planta>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Planta> builder)
        {
            builder.ToTable("tb_planta")
                .HasKey(c => c.idplanta);
            builder.Property(c => c.nombre)
                .HasMaxLength(50);
            builder.Property(c => c.descripcion)
                .HasMaxLength(256);
        }
    }
}
