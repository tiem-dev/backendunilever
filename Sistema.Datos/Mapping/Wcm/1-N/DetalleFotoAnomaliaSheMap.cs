﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Sistema.Entidades.Wcm._1_N;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sistema.Datos.Mapping.Wcm._1_N
{
    class DetalleFotoAnomaliaSheMap : IEntityTypeConfiguration<DetalleFotoAnomaliaShe>
    {
        public void Configure(EntityTypeBuilder<DetalleFotoAnomaliaShe> builder)
        {
            builder.ToTable("tb_foto_she")
                 .HasKey(d => d.idfoto);
        }
    }
}
