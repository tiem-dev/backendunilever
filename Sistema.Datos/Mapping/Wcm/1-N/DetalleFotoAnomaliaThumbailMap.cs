﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Sistema.Entidades.Wcm._1_N;

namespace Sistema.Datos.Mapping.Wcm._1_N
{
    class DetalleFotoAnomaliaThumbailMap : IEntityTypeConfiguration<DetalleFotoAnomaliaThumbnail>
    {
        public void Configure(EntityTypeBuilder<DetalleFotoAnomaliaThumbnail> builder)
        {
            builder.ToTable("tb_foto_anomalia_thumbnail")
                 .HasKey(d => d.idfoto);
        }
    }
}
