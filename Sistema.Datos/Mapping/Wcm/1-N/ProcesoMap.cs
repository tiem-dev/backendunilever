﻿using Microsoft.EntityFrameworkCore;
using Sistema.Entidades.Wcm._1_N;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sistema.Datos.Mapping.Wcm._1_N
{
    public class ProcesoMap : IEntityTypeConfiguration<Proceso>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Proceso> builder)
        {
            builder.ToTable("tb_proceso")
              .HasKey(c => c.idproceso);
            builder.Property(c => c.nombre)
                .HasMaxLength(50);
            builder.Property(c => c.descripcion)
                .HasMaxLength(256);
        }
    }
}
