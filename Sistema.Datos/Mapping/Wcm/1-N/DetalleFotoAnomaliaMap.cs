﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Sistema.Entidades.Wcm._1_N;

namespace Sistema.Datos.Mapping.Wcm._1_N
{
    class DetalleFotoAnomaliaMap : IEntityTypeConfiguration<DetalleFotoAnomalia>
    {
        public void Configure(EntityTypeBuilder<DetalleFotoAnomalia> builder)
        {
            builder.ToTable("tb_foto_anomalia")
                .HasKey(d => d.idfoto);
        }
    }
}
