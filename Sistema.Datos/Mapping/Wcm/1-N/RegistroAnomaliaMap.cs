﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Sistema.Entidades.Wcm._1_N;

namespace Sistema.Datos.Mapping.Wcm._1_N
{
    class RegistroAnomaliaMap : IEntityTypeConfiguration<Registrosanomalias>
    {
        public void Configure(EntityTypeBuilder<Registrosanomalias> builder)
        {
            builder.ToTable("tb_registro_anomalia")
                  .HasKey(i => i.idregistroanomalia);


            builder.HasOne(i => i.usuario)
              .WithMany(p => p.registroAnomalia)
              .HasForeignKey(i => i.idusuario);


            builder.HasOne(i => i.area)
               .WithMany(p => p.registroAnomalia)
               .HasForeignKey(i => i.idarea);

            builder.HasOne(i => i.area)
                     .WithMany(p => p.registroAnomalia)
                     .HasForeignKey(i => i.idarea);

            builder.HasOne(i => i.maquina)
              .WithMany(p => p.registroAnomalia)
              .HasForeignKey(i => i.idmaquina);

            builder.HasOne(i => i.sector)
             .WithMany(p => p.registroAnomalia)
             .HasForeignKey(i => i.idsector);

            builder.HasOne(i => i.anomalia)
               .WithMany(p => p.registroAnomalia)
               .HasForeignKey(i => i.idanomalia);

            builder.HasOne(i => i.suceso)
              .WithMany(p => p.registroAnomalia)
              .HasForeignKey(i => i.idsuceso);


            builder.HasOne(i => i.tarjeta)
              .WithMany(p => p.registroAnomalia)
              .HasForeignKey(i => i.idtarjeta);

            builder.HasOne(i => i.proyecto)
             .WithMany(p => p.registroAnomalia)
             .HasForeignKey(i => i.idproyecto);


           builder.HasOne(u => u.usuariotecnico)
               .WithMany(u => u.registroAnomaliaTecnico)
               .HasForeignKey(u => u.idtecnico);

            builder.HasOne(u => u.usuariosupervisor)
              .WithMany(u => u.registroAnomaliaSupervisor)
              .HasForeignKey(u => u.idsupervisor);


            builder.HasOne(u => u.usuariolider)
              .WithMany(u => u.registroAnomaliaLider)
              .HasForeignKey(u => u.idlider);

        }

    }
}
