﻿using Microsoft.EntityFrameworkCore;
using Sistema.Entidades.Wcm._1_N;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sistema.Datos.Mapping.Wcm._1_N
{
    class RegistroSheMap : IEntityTypeConfiguration<RegistroShe>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<RegistroShe> builder)
        {
            builder.ToTable("tb_registro_she")
                .HasKey(i => i.idshe);

            builder.HasOne(i => i.usuario)
              .WithMany(p => p.registroAnomaliaShe)
              .HasForeignKey(i => i.idusuario);

            builder.HasOne(i => i.area)
               .WithMany(p => p.registroShe)
               .HasForeignKey(i => i.idarea);


            builder.HasOne(i => i.maquina)
              .WithMany(p => p.registroShe)
              .HasForeignKey(i => i.idmaquina);

            builder.HasOne(i => i.sector)
             .WithMany(p => p.registroShe)
             .HasForeignKey(i => i.idsector);

   
         builder.HasOne(i => i.tarjeta)
           .WithMany(p => p.registroShe)
           .HasForeignKey(i => i.idtarjeta);



         builder.HasOne(u => u.usuariotecnico)
             .WithMany(u => u.registroAnomaliaSheTecnico)
             .HasForeignKey(u => u.idtecnico);

         builder.HasOne(u => u.usuariosupervisor)
           .WithMany(u => u.registroAnomaliaSheSupervisor)
           .HasForeignKey(u => u.idsupervisor);
           

         builder.HasOne(u => u.usuariolider)
           .WithMany(u => u.registroAnomaliaSheLider)
           .HasForeignKey(u => u.idlider);

       builder.HasOne(i => i.condicionInsegura)
          .WithMany(p => p.registroShe)
          .HasForeignKey(i => i.idcondicion);

       builder.HasOne(i => i.Falla)
         .WithMany(p => p.registroShe)
         .HasForeignKey(i => i.idfalla);
        



        }
    }
}
