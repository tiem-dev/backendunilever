﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Sistema.Entidades.Wcm._1_N;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sistema.Datos.Mapping.Wcm._1_N
{
    class DetalleFotoAnomaliaThumbailSheMap : IEntityTypeConfiguration<DetalleFotoAnomaliaThumbailShe>
    {
        public void Configure(EntityTypeBuilder<DetalleFotoAnomaliaThumbailShe> builder)
        {
            builder.ToTable("tb_foto_anomalia_thumbnail_she")
                 .HasKey(d => d.idfoto);
        }
    }
}
