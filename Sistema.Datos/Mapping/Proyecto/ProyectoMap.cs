﻿using Microsoft.EntityFrameworkCore;
using System;
using Sistema.Entidades.Proyecto;

using System.Collections.Generic;
using System.Text;

namespace Sistema.Datos.Mapping.ProyectoMap
{
    class ProyectoMap : IEntityTypeConfiguration<Proyectos>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Proyectos> builder)
        {
            builder.ToTable("tb_proyecto")
               .HasKey(c => c.idproyecto);
            builder.Property(c => c.nombre)
                .HasMaxLength(50);
            builder.Property(c => c.descripcion)
                .HasMaxLength(256);
        }
    }
}
